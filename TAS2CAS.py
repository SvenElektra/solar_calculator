#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.spatial as spatial
from operator import add
import matplotlib as mpl
from matplotlib import cm
from matplotlib import rc
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from functions import MIN_POWER_HOVER
from functions import GET_BEST_EFF
from functions import INTERPOL_ALT_CLIMBRATE
from functions import INTERPOL_ALT_POWER
from functions import getStandardPressure
from functions import getStandardTemperature
from functions import getGeopotential
from functions import getEfficiencySolarcells
from functions import getSolarPowerDensity
from functions import getCosPsi
from functions import getAtmoTransmission
from functions import getDepression
from functions import make_format
from functions import getSolCellTemp
from functions import getSolCellTempCalibrated
from functions import GET_REYNOLDS
from functions import GET_MACH
from functions import GET_SUNVECTOR
from functions import GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo
from functions import GET_BAT_VOLTAGE_CURRENT_52V
from functions import GET_BAT_VOLTAGE_14S_20P_3500_LiIo
from functions import GET_BAT_VOLTAGE_52V
from functions import SOUNDSPEED
from functions import DYNPRESS
from functions import TAS2CAS_mps
from functions import TAS2CAS_kmh
from functions import GET_MAXRPM
from vectors import rotation_matrix
from vectors import GET_SUN_PROJ_SURF
from vectors import angle_between
from vectors import cell_surf_transmission

# print entire numpy arrays
np.set_printoptions(threshold=np.inf)



print("############################################");
print("###      Solar Mission Calculator        ###");
print("###         Sven Schmid                  ###");
print("############################################");


altitude = 7500.
TAS_kmh=42.33*3.6
print("In altitude = %8.2f m:" %(altitude))
print("TAS=%8.2fkm/h \nCAS=%8.2fkm/h" % (TAS_kmh,TAS2CAS_kmh(altitude,TAS_kmh)))
print("AND")
print("TAS=%8.2fm/s \nCAS=%8.2fm/s" % (TAS_kmh/3.6,TAS2CAS_kmh(altitude,TAS_kmh)/3.6))


