#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib.pyplot as plt
from operator import add
import math
from scipy import interpolate
from vectors import rotation_matrix
import cmath

################## GET BEST EFFICIENCY POINT FOR EACH CLIMBRATE ###############################################

def GET_BEST_EFF(mot_data_M_alt,CR,number_vx_vz_combi):
	


	index=[]
	best_eff=[]
	M=[]
	##### ZERO CLIMB #############################################
	if CR == 0.0:
		for i in range(0,number_vx_vz_combi):
			if(mot_data_M_alt[i][2] == CR):
				#print mot_data_M_alt[i][2]
				index.append(i)
				best_eff.append(mot_data_M_alt[i][10])
				M.append(mot_data_M_alt[i][:])
		return(M[np.argmin(best_eff)]);

	##### NON-ZERO CLIMB #############################################
	if CR > 0.0:
		for i in range(0,number_vx_vz_combi):
			if(mot_data_M_alt[i][2] == CR):
				#print mot_data_M_alt[i][2]
				index.append(i)
				best_eff.append(mot_data_M_alt[i][6])
				M.append(mot_data_M_alt[i][:])
		
		return(M[np.argmax(best_eff)]);
	del(index)
	del(best_eff)
	del(M)

################## INTERPOLATE ALTITUDE AND CLIMBRATE ###############################################

def INTERPOL_ALT_CLIMBRATE(mot_data_M_BestEff,act_altitude,act_climbrate):

	#print(mot_data_M_BestEff,act_altitude,act_climbrate)
	
	##### Interpolate all values for the actual altitude ######
	x=np.zeros(np.shape(mot_data_M_BestEff)[0])
	y=np.zeros(np.shape(mot_data_M_BestEff)[0])
	M_act_alt=np.zeros((np.shape(mot_data_M_BestEff)[1],np.shape(mot_data_M_BestEff)[2]))
	M_act_alt_act_Climbrate=np.zeros((np.shape(mot_data_M_BestEff)[2]))
	xint=act_altitude

	for k in range(1,np.shape(mot_data_M_BestEff)[2]): ## LOOP OVER VARIABLES
		for j in range(0,np.shape(mot_data_M_BestEff)[1]): ## LOOP OVER CLIMBRATES
			for i in range(0,np.shape(mot_data_M_BestEff)[0]): ## LOOP OVER ALTITUDES		
				x[i]=mot_data_M_BestEff[i][j][0]
				y[i]=mot_data_M_BestEff[i][j][k]

			tck = interpolate.splrep(x, y, s=0)
			yint = interpolate.splev(xint, tck, der=0)
			M_act_alt[j][k]=yint
			#if k==21:
				#print(k,xint,yint)
				
	for i in range(0,np.shape(mot_data_M_BestEff)[1]):
		M_act_alt[i][0]=xint
	
	##### Interpolate all values for the actual climbrate ######
	xx=np.zeros(np.shape(mot_data_M_BestEff)[1])
	yy=np.zeros(np.shape(mot_data_M_BestEff)[1])
	xxint=act_climbrate
		
	for k in range(0,np.shape(M_act_alt)[1]): ## LOOP OVER VARIABLES
		for j in range(0,np.shape(M_act_alt)[0]): ## LOOP OVER CLIMBRATES	
			xx[j]=M_act_alt[j][2]
			yy[j]=M_act_alt[j][k]
	
		tck = interpolate.splrep(xx, yy, s=0)
		yyint = interpolate.splev(xxint, tck, der=0)
		M_act_alt_act_Climbrate[k]=yyint
		#if k==21:
			#print(k,xxint,yyint)
			#print(xx,yy)

	return(M_act_alt_act_Climbrate)	
	
	

################## INTERPOLATE ALTITUDE AND POWER ###############################################

def INTERPOL_ALT_POWER(mot_data_M_BestEff,act_altitude,act_power,redsink_M):
	
	
	returnlist=[]
	##### Interpolate all values for the actual altitude ######
	x=np.zeros(np.shape(mot_data_M_BestEff)[0])
	y=np.zeros(np.shape(mot_data_M_BestEff)[0])
	M_act_alt=np.zeros((np.shape(mot_data_M_BestEff)[1],np.shape(mot_data_M_BestEff)[2]))
	M_act_alt_act_Power=np.zeros((np.shape(mot_data_M_BestEff)[2]))
	redsink_M_act_alt=np.zeros((np.shape(redsink_M)[1],np.shape(redsink_M)[2]))
	xint=act_altitude
	
	
	
	##### Interpolate Motorized Data for Climb ############
	for k in range(1,np.shape(mot_data_M_BestEff)[2]): ## LOOP OVER VARIABLES
		for j in range(0,np.shape(mot_data_M_BestEff)[1]): ## LOOP OVER CLIMBRATES
			for i in range(0,np.shape(mot_data_M_BestEff)[0]): ## LOOP OVER ALTITUDES		
				x[i]=mot_data_M_BestEff[i][j][0]
				y[i]=mot_data_M_BestEff[i][j][k]

			tck = interpolate.splrep(x, y, s=0)
			yint = interpolate.splev(xint, tck, der=0)
			M_act_alt[j][k]=yint
				
	for i in range(0,np.shape(mot_data_M_BestEff)[1]):
		M_act_alt[i][0]=xint
	
	
	#print redsink_M_act_alt
	
	##### Interpolate Motorized Data for Reduced Sink ############
	for k in range(1,np.shape(redsink_M)[2]): ## LOOP OVER VARIABLES
		for j in range(0,np.shape(redsink_M)[1]): ## LOOP OVER SINK-POWERS
			for i in range(0,np.shape(redsink_M)[0]): ## LOOP OVER ALTITUDES		
				x[i]=redsink_M[i][j][0]
				y[i]=redsink_M[i][j][k]

			tck = interpolate.splrep(x, y, s=0)
			yint = interpolate.splev(xint, tck, der=0)
			redsink_M_act_alt[j][k]=yint
				
	for i in range(0,np.shape(redsink_M)[1]):
		redsink_M_act_alt[i][0]=xint	
	
	#print(redsink_M_act_alt)
	#exit()
	
	### Check the HOVER power #########
	#print("Required Power: ",M_act_alt[0][10])
	#print("Available Power: ",act_power)
	#print("act_power: ",act_power,"redsink_M_act_alt[0][10]: ",redsink_M_act_alt[0][10])
	
	
	if (M_act_alt[0][10] <= act_power):
		#print("Available Power is greater than required power --> Climb",act_altitude,act_power)
		##### Interpolate all values for the actual power ######
		xx=np.zeros(np.shape(mot_data_M_BestEff)[1])
		yy=np.zeros(np.shape(mot_data_M_BestEff)[1])
		xxint=act_power

		for k in range(0,np.shape(M_act_alt)[1]): ## LOOP OVER VARIABLES
			for j in range(0,np.shape(M_act_alt)[0]): ## LOOP OVER POWER VALUES	
				xx[j]=M_act_alt[j][10]
				yy[j]=M_act_alt[j][k]

			tck = interpolate.splrep(xx, yy, s=0)
			yyint = interpolate.splev(xxint, tck, der=0)
			M_act_alt_act_Power[k]=yyint
	else:
		#print("Available Power is smaller than required power SINKFLIGHT",act_altitude,act_power)
			
		#print("SINKPOWERS: " , redsink_M_act_alt[0][10], redsink_M_act_alt[1][10], redsink_M_act_alt[2][10], redsink_M_act_alt[3][10], redsink_M_act_alt[4][10])
		#print("ACT POWER: ", act_power)
		##### Interpolate all values for the actual power ######
		xx=np.zeros(5)
		yy=np.zeros(5)
		xxint=act_power

		for k in range(0,np.shape(redsink_M_act_alt)[1]): ## LOOP OVER VARIABLES
			for j in range(0,5): ## LOOP OVER POWER VALUES	
				#print i
				xx[j]=redsink_M_act_alt[j][10]
				yy[j]=redsink_M_act_alt[j][k]
			#print(xx,yy)
			tck = interpolate.splrep(xx, yy, s=0)
			yyint = interpolate.splev(xxint, tck, der=0)
			M_act_alt_act_Power[k]=yyint
	
	
	if redsink_M_act_alt[0][10] > act_power:
		stopflag = 1
	else:
		stopflag = 0
	
	returnlist.append(M_act_alt_act_Power)
	returnlist.append(stopflag)
	
	return(returnlist)
	del(returnlist)


################## SOLAR CELLS TEMPERATURE ROUTINE  ###############################################

def getSolCellTempEnerBal(solcel_limtemp_alt,solcel_limtemp_mintemp,solcel_limtemp_maxtemp,solcell_nom_eff,eff_grad,lamturb,CosPsi,temp_atmo, RE, altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,ReRef):
	
	########## Solar Cells Temperature Calculation based on Energy Balance Consideration #############
	#print(solcell_nom_eff,eff_grad,lamturb,density,CosPsi,solcell_normals, temp_atmo,velocity, RE, altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,ReRef)
	
	### ARRAY THAT CONTAINS TEMPERATURES FROM -80DEG C to +30DEG C in 1C DELTAS (Celsius and Kelvin) ###
	temp_K=np.zeros(111)
	temp_C=np.zeros(111)
	RadPow=np.zeros(111)
	ConPow=np.zeros(111)
	SolPow=np.zeros(111)
	ElePow=np.zeros(111)
	Residual=np.zeros(111)
	
	for i in range(0,111):
		temp_K[i] = float(i-80+273.15)
		temp_C[i] = float(i-80)	
	
	#### RADIATED POWER ####
	StefanBoltzman=5.67E-8
	EmissEpsilon=0.8 # Assumed Emission Coefficient, black Surface
	for i in range(0,111):
		RadPow[i]=StefanBoltzman*EmissEpsilon*np.power(temp_K[i],4)
	
	#### CONVECTED POWER ####
	for i in range(0,111):
		DeltaT=temp_K[i]-temp_atmo
		cp=1004.
		Prandtl=0.73
		DynViscETA=5.309783*1E-8*temp_C[i] + 1.7359*1E-5
		
		if lamturb == 0: #laminar flow
			alpha=DynViscETA*cp/Prandtl/ReRef*0.332*np.power(RE,0.5)*np.power(Prandtl,0.33333333333) ###Skript ITLR, Uni Stuttgart, eigentlich noch Faktor 2 fuer Mittel...
			#alpha=DynViscETA*cp/Prandtl/ReRef*0.664*np.power(RE,0.5)*np.power(Prandtl,0.33333333333)
		if lamturb == 1: #turbulent flow
			alpha=DynViscETA*cp/Prandtl/ReRef*0.0296*np.power(RE,0.8)*np.power(Prandtl,0.43) ### Jochen Wilms, TU-Dresden
			#alpha=DynViscETA*cp/Prandtl/ReRef*0.037*np.power(RE,0.8)*np.power(Prandtl,0.43) ### Internet
		if lamturb !=0 and lamturb !=1:
			print(lamturb,"ERROR IN LAMINAR/TURBULENT FLAGS")
			exit()
		ConPow[i]=DeltaT*alpha

	#### SOLAR POWER ####
	for i in range(0,111):
		SolPow[i]=solPowDens*solcel_surf_transmission*CosPsi*atmoTransmiss
		actual_effi= solcell_nom_eff/100.*(1.+(temp_C[i]-25.)*eff_grad)
		ElePow[i]=SolPow[i]*actual_effi
		Residual[i]=SolPow[i]-ElePow[i]-ConPow[i]-RadPow[i]
		#print("RESIDUAL:",Residual[i],"i:",i,"j",j,"LAMTURB:",lamturb,"RADPOW:",RadPow[i],"CONPOW:",ConPow[i],"SOLPOW:",SolPow[i],"ELEPOW:",ElePow[i],"SOLCELTEMP:",temp_K[i],"ATMOTEMP:",temp_atmo,actual_effi,eff_grad)
	bestIndex = np.argmin(np.abs(Residual))
	
	#print("#### RESIDUAL:",Residual[bestIndex],"i:",bestIndex,"j",j,"LAMTURB:",lamturb,"RADPOW:",RadPow[bestIndex],"CONPOW:",ConPow[bestIndex],"SOLPOW:",SolPow[bestIndex],"ELEPOW:",ElePow[bestIndex],"SOLCELTEMP:",temp_K[bestIndex],"ATMOTEMP:",temp_atmo,actual_effi,eff_grad)
	sol_cells_temp=np.maximum(temp_K[bestIndex],temp_atmo)
	
	act_Ctemp_lim_upp=np.interp(altitude,solcel_limtemp_alt,solcel_limtemp_maxtemp)+273.15
	act_Ctemp_lim_low=np.interp(altitude,solcel_limtemp_alt,solcel_limtemp_mintemp)+273.15
	
	sol_cells_temp=np.maximum(sol_cells_temp,act_Ctemp_lim_low)
	sol_cells_temp=np.minimum(sol_cells_temp,act_Ctemp_lim_upp)
	
	return(sol_cells_temp);
	

################## SOLAR CELLS TEMPERATURE ROUTINE, CALIBRATED WITH 2 MEASUREMENTS IN 20KM ALTITUDE (BALLOON AND CHINESE UAS)  ###############################################

def getSolCellTempCalibrated(AtmoTemp,curr_alt,velocity,cosPsi):
	
	########## Solar Cells Temperature Calculation for High Altitudes based on velocity and current sun angle, Approach 2: corrected with Measurements from Balloon and Chinese UAS #############
	DeltaTemp=(curr_alt/20000.)*(76.5-1.1428*velocity)+(1-curr_alt/20000.)*10.
	
	if cosPsi > 0.:       ### Account for sun intensity via CosPSI
		AngFac=cosPsi
	else:
		AngFac=0.

	DeltaTemp=DeltaTemp*AngFac
	sol_cells_temp=AtmoTemp+DeltaTemp
	#print "Altitude: ",curr_alt,"\nVelocity: ",velocity,"\nAtmosphere Temperature: ",AtmoTemp-273.,"\nTemperature Delta: ",DeltaTemp,"\nSolar Cells Temperature: ",sol_cells_temp-273.,"\nAngle Factor: ",AngFac
	#exit()
	return(sol_cells_temp);


################## STANDARD PRESSURE ALTITUDE ROUTINE  ###############################################
def getStandardPressure(altitude):   ## Altitude in meters, Returns result in Pascals

        # Below 51 km: Practical Meteorology by Roland Stull, pg 12        
        # Validation data: https://www.avs.org/AVS/files/c7/c7edaedb-95b2-438f-adfb-36de54f87b9e.pdf
	# https://en.wikipedia.org/wiki/Barometric_formula
	
	altitude = altitude / 1000.0  # Convert m to km
	geopot_height = getGeopotential(altitude*1000.)
	
	t = getStandardTemperature(geopot_height)
	geopot_height=geopot_height/1000.

	if (geopot_height <= 11.):
		StdPress=101325 * math.pow(288.15 / t, -5.255877)
		#print "1: ",geopot_height, StdPress,t
	elif (geopot_height <= 20.):
		StdPress=22632.06 * math.exp(-0.1577 * (geopot_height - 11.))
		#print "2: ",geopot_height, StdPress,t
	elif (geopot_height <= 32.):
		StdPress= 5474.889 * math.pow(216.65 / t, 34.16319)

	#geopot_height = earth_radius * altitude / (earth_radius + altitude) /// All in km
	#Temperature is in kelvins = 273.15 + Celsius

	return(StdPress);
	
################## STANDARD TEMPERATURE ALTITUDE ROUTINE  ###############################################

def getStandardTemperature(geopot_height):
	#Standard atmospheric pressure
	#Below 51 km: Practical Meteorology by Roland Stull, pg 12
	# https://en.wikipedia.org/wiki/Barometric_formula
	
	geopot_height=geopot_height/1000.   # Input in meters
	
	if (geopot_height <= 11):          		# Troposphere
		stdTemp= 288.15 - (6.5 * geopot_height)
	elif (geopot_height <= 20):     		# Stratosphere starts
		stdTemp=  216.65
	elif (geopot_height <= 32):
		stdTemp=  196.65 + geopot_height
	# Thermosphere has high kinetic temperature (500 C to 2000 C) but temperature
	# as measured by a thermometer would be very low because of almost vacuum.
	return(stdTemp);
	
################## USER-DEFINED TEMPERATURE ALTITUDE ROUTINE  ###############################################

def getUserDefTemperature_CELSin_KELout(act_altitude,altitudes,temps_C):	
	temp_K_interpol=273.+ np.interp(float(act_altitude),altitudes,temps_C)
	
	return(temp_K_interpol);

################## GEO POTENTIAL ROUTINE  ###############################################   
 
def getGeopotential(altitude):
	altitude=altitude/1000.  # input in meters
	EARTH_RADIUS =  6356.766 # km
	geoPot= EARTH_RADIUS * altitude / (EARTH_RADIUS + altitude)
	
	geoPot = geoPot * 1000. # returns meters
	return(geoPot);

################## SOLARCELLS EFFICIENCY DEPENDING ON ALTITUDE ROUTINE  ###############################################   
def getEfficiencySolarcells(temperature,solcell_nom_eff,eff_grad):
	#Efficicney of cells at 25degC: 23.7percent.
	soleff=solcell_nom_eff*(1+eff_grad*(273.15+25. - temperature))
	return(soleff);

################## SOLAR POWER DENSITY ROUTINE  ###############################################   
def getSolarPowerDensity(time_of_year):
	solar_constant=1367 # W/square-meters
	solarEnergyDensity=solar_constant*(1.+0.033*np.cos(2*np.pi/365*(time_of_year-4)))
	return(solarEnergyDensity);

################## COSINUS OF LIGHT IMPACT ANGLE ROUTINE  ###############################################   
def getCosPsi(day_of_year,time_of_day,latitude):
	sunDeclination=-23.5*np.cos(2.*np.pi/365.*(day_of_year+10.))
	hourAngle=(time_of_day/12.-1.)*np.pi
	cosPsi=np.sin(latitude/180.*np.pi)*np.sin(sunDeclination*np.pi/180.) + \
				np.cos(latitude/180.*np.pi)*np.cos(sunDeclination*np.pi/180.)*np.cos(hourAngle)
	
	return(cosPsi);

################## ATHMOSPHERIC TRANSMISSION CALCULATION ROUTINE  ###############################################   
def getAtmoTransmission(altitude,incangle,depression):
	exponent=0.678+altitude/40000.
	base=np.sin((0.5*np.pi*(incangle+depression))/(0.5*np.pi*180/np.pi+depression))
	AtmoTransmission=np.exp(-0.357*np.exp(-altitude/7000.)/np.power(base,exponent))
	return(AtmoTransmission);

################## ATMOSPHERIC DEPRESSION CALCULATION ROUTINE  ###############################################   
def getDepression(altitude):
	AtmoDepression=0.57+np.arccos(6356766./(6356766.+altitude))*180/np.pi
	return(AtmoDepression);
	

################## GET HOVER CONDITION WITH MINIMUM POWER ###############################################

def MIN_POWER_HOVER(number_vx_vz_combi,mot_data_variables,mot_data_M_act_altitude):
	
	mot_data_M_hover=[]
	powers=[]

	for i in range(0,number_vx_vz_combi):
		if mot_data_M_act_altitude[i][2] == 0.0:
			mot_data_M_hover.append(mot_data_M_act_altitude[i][:])
	for i in range (0,len(mot_data_M_hover)):
		powers.append(mot_data_M_hover[i][10])
	
	#print powers
	bestindex=np.argmin(powers)
	return(mot_data_M_hover[bestindex][:]);



################## SHOW CURSOR VALUES IN PLOTS ###############################################

def make_format(current, other):
    # current and other are axes
    def format_coord(x, y):
        # x, y are data coordinates
        # convert to display coords
        display_coord = current.transData.transform((x,y))
        inv = other.transData.inverted()
        # convert back to data coords with respect to ax
        ax_coord = inv.transform(display_coord)
        coords = [ax_coord, (x, y)]
        return ('Left: {:<}    Right: {:<}'
                .format(*['({:.3f}, {:.3f})'.format(x, y) for x,y in coords]))
    return format_coord


################################################################################
def GET_REYNOLDS(act_alt,act_velocity,ref_len):
	altitude=[0,2500.,5000,7500.,10000,12500.,15000,17500.,20000,22000]
	nue=[1.479E-05, 1.8078E-5 ,2.235E-05, 2.8E-5,3.559E-05,4.986E-5 ,7.395E-05, 0.0001097,0.0001627, 0.000226459510358]
	soundspeed=[340., 330.5,320.7,310.,299.,295.,295.,295.,295.,296.4]
	nue_interpol=float(np.interp(act_alt,altitude,nue))
	reynolds=act_velocity*ref_len/nue_interpol
	return(reynolds)

################################################################################
def GET_MACH(act_alt,act_velocity):
	altitude=[0,2500.,5000,7500.,10000,12500.,15000,17500.,20000,22000]
	nue=[1.479E-05, 1.8078E-5 ,2.235E-05, 2.8E-5,3.559E-05,4.986E-5 ,7.395E-05, 0.0001097,0.0001627, 0.000226459510358]
	soundspeed=[340., 330.5,320.7,310.,299.,295.,295.,295.,295.,296.4]
	soundspeed_interpol=float(np.interp(act_alt,altitude,soundspeed))
	mach_interpol=act_velocity/soundspeed_interpol
	return(mach_interpol)


################################################################################
def GET_SUNVECTOR(day_of_year,curr_time_h,latitude):
	
	result=[]
	
	### 3 Steps ##################################
	# 1. calculate the incidence angle.
	# 2. calculate the azimuth angle. 
	# 3. Rotate a unit-vector to match the sun-ray direction. 
	
	### STEP #1 #################
	incidence_angle=90.-np.arccos(getCosPsi(float(day_of_year),float(curr_time_h),float(latitude)))*180./np.pi
	incidence_angle_rad=incidence_angle*np.pi/180.
	
	hour_angle=(curr_time_h/12.)*180.
	hour_angle_rad=hour_angle*np.pi/180.
	
	
	declination_angle=-23.44*np.cos(360./365.*(day_of_year+10.)*np.pi/180.)
	declination_angle_rad=declination_angle*np.pi/180.
	
	
	zenith_angle=90.-incidence_angle
	zenith_angle_rad=zenith_angle*np.pi/180.
				
	
	latitude_rad=float(latitude)*np.pi/180.				
	
	### STEP #2 #################
	if (((np.sin(declination_angle_rad)-np.cos(zenith_angle_rad)*np.sin(latitude_rad))/np.sin(zenith_angle_rad)/np.cos(latitude_rad)) <= -1.):
		azimuth_angle_rad=np.pi
	elif (((np.sin(declination_angle_rad)-np.cos(zenith_angle_rad)*np.sin(latitude_rad))/np.sin(zenith_angle_rad)/np.cos(latitude_rad)) >= 1.):
		azimuth_angle_rad=0.
	else:	
		azimuth_angle_rad=np.arccos((np.sin(declination_angle_rad)-np.cos(zenith_angle_rad)*np.sin(latitude_rad))/np.sin(zenith_angle_rad)/np.cos(latitude_rad))

	azimuth_angle=azimuth_angle_rad*180./np.pi
	
	if np.sin(hour_angle_rad) < 0.:
		azimuth_angle=360.-azimuth_angle
		
	### STEP #3 #################
	sun_vector=np.array([-1.,0.,0.]) # POINTING TO SOUTH INITIALLY
	sun_vector= np.dot(rotation_matrix(np.array([0.,1.,0.]),incidence_angle), sun_vector)  # ROTATION AROUND Y-AXIS TO ACCOUNT FOR INCLINATION
	sun_vector= np.dot(rotation_matrix(np.array([0.,0.,1.]),azimuth_angle), sun_vector)  # ROTATION AROUND Z-AXIS TO ACCOUNT FOR HOUR ANGLE

	result.append(incidence_angle)
	result.append(hour_angle)
	result.append(declination_angle)
	result.append(zenith_angle)
	result.append(azimuth_angle)
	result.append(sun_vector[0])
	result.append(sun_vector[1])
	result.append(sun_vector[2])

	return(result)

################################################################################
def GET_BAT_VOLTAGE_14S_20P_3500_LiIo(current,remaining_Bat_Energy,number_of_batteries,single_bat_cap):
	
	# THE FUNCTION RETURNS THE BAT VOLTAGE FOR A GIVEN CURRENT. NOT USED AS CURENT IS UNKNOWN (PART OF THE SOLUTION). ONLY POWER IS KNOWN!
	# current: total current of all motors together,
	# remaining_Bat_Energy: [kWh], for all batteries together. If all batteries are fully charged, then the following equation applies: remaining_Bat_Energy = number_of_batteries * single_bat_cap
	
	calib_factor = 1.0 # THIS FACTOR IS BASED ON COMPARISON WITH MEASUREMENT DATA FROM FLIGHT TEST: BATTERY VOLTAGE IS OVREPREDICTED OTHERWISE
	
	capacity = 70 * remaining_Bat_Energy / (number_of_batteries * single_bat_cap)
	current = current/number_of_batteries
	
	battery_voltage = ((42.-0.019*current)+(0.36-0.0008*current)*capacity-1E-5*(200.-current)*capacity*capacity)*calib_factor
	return(battery_voltage)
	
################################################################################
def GET_BAT_VOLTAGE_52V(current,remaining_Bat_Energy,number_of_batteries,single_bat_cap):
	return(52.)

################################################################################
def GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(power,remaining_Bat_Energy,number_of_batteries,single_bat_cap):
	
	# THE FUNCTION RETURNS THE BAT VOLTAGE AND THE TOTAL CURRENT OF ALL BATTERIES FOR A GIVEN CAPACITY AND POWER
	# power: total battery power drained [W]
	# current: total current of all motors together,
	# remaining_Bat_Energy: [kWh], for all batteries together. If all batteries are fully charged, then the following equation applies: remaining_Bat_Energy = number_of_batteries * single_bat_cap
	# CALCULATE ALL VALUES FOR A SINGLE BATTERY BLOCK ONLY. SOLVE QUADRATIC EQUATION (INPUT BY KARSTEN STRAUSS)
	
	calib_factor = 0.97 # THIS FACTOR IS BASED ON COMPARISON WITH MEASUREMENT DATA FROM FLIGHT TEST: BATTERY VOLTAGE IS OVREPREDICTED OTHERWISE
	calib_addition = 2.9 # REMOVE THIS AGAIN!
	
	result=[]
	power = power/number_of_batteries
	capacity = 70 * remaining_Bat_Energy / (number_of_batteries * single_bat_cap)
	a=(capacity*capacity - 80.*capacity - 1900.)/100000.
	b=42.+0.36*capacity-0.002*capacity*capacity
	c=-power
	delta = (b**2) - (4*a*c)
	I2 = (-b+cmath.sqrt(delta))/(2*a)
	
	
	if power/I2.real < 60. and power/I2.real > 39.:
		bat_voltage=power/I2.real
		bat_current_total=I2.real*number_of_batteries
	else:
		print("############ BATTERY OVER-DISCHARGED, SETTING U=39V ###################")
		bat_voltage = 39
		bat_current_total = power/bat_voltage*number_of_batteries

	result.append(bat_voltage*calib_factor+calib_addition)
	result.append(bat_current_total/calib_factor)
	
	return(result)


################################################################################
def GET_BAT_VOLTAGE_CURRENT_52V(power,remaining_Bat_Energy,number_of_batteries,single_bat_cap):
	
	result=[]
	bat_voltage=52.
	bat_current_total=power/bat_voltage

	result.append(bat_voltage)
	result.append(bat_current_total)

	return(result)


################################################################################
def SOUNDSPEED(act_altitude):
	altitude=[00,2500.,5000,7500.,10000,12500.,15000,17500.,20000,22000]
	soundspeed=[340., 330.5,320.7,310.,299.,295.,295.,295.,295.,296.4]
	act_soundspeed=float(np.interp(act_altitude,altitude,soundspeed))
	return(act_soundspeed)

################################################################################
def DYNPRESS(act_altitude,TAS_mps):
	kappa=1.4
	a=SOUNDSPEED(act_altitude)
	p_s=getStandardPressure(act_altitude)
	dyn_press=p_s*(((kappa-1)/2*(TAS_mps/a)**2+1)**(kappa/(kappa-1))-1)
	return(dyn_press)

################################################################################
def TAS2CAS_mps(act_altitude,TAS_mps):
	a0=340.
	p0=101325.
	kappa=1.4
	CAS_mps=a0*np.sqrt(2/(kappa-1)*((DYNPRESS(act_altitude,TAS_mps)/p0+1)**(0.4/1.4)-1))
	return(CAS_mps)

################################################################################
def TAS2CAS_kmh(act_altitude,TAS_kmh):
	TAS_mps=TAS_kmh/3.6
	a0=340.
	p0=101325.
	kappa=1.4
	CAS_mps=a0*np.sqrt(2/(kappa-1)*((DYNPRESS(act_altitude,TAS_mps)/p0+1)**(0.4/1.4)-1))
	return(CAS_mps*3.6)

################################################################################
def GET_MAXRPM(curr_pow,Batt_Volt):
	kv_0=48.   # RPM constant for no load conditions
	kv_nom=44. # RPM constant for nominal load conditions
	nompow=32000. # Nominal Power in W
	kv=kv_0-(kv_0-kv_nom)*curr_pow/nompow
	return(kv*Batt_Volt)
	

################################################################################
def SCALE_TAS(tempprofile_flag,act_altitude,ref_altitude,atmo_temp_altitude,atmo_temp_temp):

	if tempprofile_flag == 1:
		density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getStandardTemperature(getGeopotential(act_altitude))
		density_ref = 0.0289644*getStandardPressure(float(ref_altitude))/8.3144598/getStandardTemperature(getGeopotential(ref_altitude))
	else:
		density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
		density_ref = 0.0289644*getStandardPressure(float(ref_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(ref_altitude),atmo_temp_altitude,atmo_temp_temp)
	
	scalfac = np.sqrt(density/density_ref)
	#print(scalfac)
	return(scalfac)

################################################################################
def GET_CSVDATAPATH(datapath):
	csv_datapath=[]
	
	command = str('ls ') + str(datapath)  + str('/vel*/csv* > out_names.txt')
	os.system(command)
	readpath=open('out_names.txt','r')
	line=()
	while(line !=''):
		line = readpath.readline()
		if line != '':
			csv_datapath.append(str.strip(line))
	readpath.close()
	os.system('rm out_names.txt')
	
	return(csv_datapath)

################################################################################
def INTERPOL_CSV_FILES_VELO(csv_datapath,TAS_DATA,DBETA,count,mission_phase_type):
	
	# DO THIS ONLY FOR CLIMB AND HOVER, NOT FOR GLIDE! ################
	if(str(mission_phase_type) == str('CL_A1_CR1_A2_CR2') or str(mission_phase_type) == str('HOVER_ALTIT_TIME')):
	       velo=[]
	       for i in range(0,len(csv_datapath)):
	               f=open(csv_datapath[i])
	               f.readline()
	               velo.append(float(f.readline().strip().split(',')[1]))
	               alt=float(f.readline().strip().split(',')[0])
	               f.close()
	       #### PARSE THE DIFFERENT DBETA VALUES IN THE DATAFILE ########
	       f=open(csv_datapath[0])
	       line=f.readline()
	       dbeta_check=[]
	       while(line !=''):
	               line=f.readline()
	               if line !='':
	        	       dbeta_check.append(float(line.strip().split(',')[3]))
	        	       
	       if TAS_DATA < velo[0]:
	               print("\n########## ERROR ON REQUESTED TAS ##########")
	               print("INTERPOLATION ROUTINE FAILED FOR ALTITUDE:",alt,"m (REQUESTED TAS IS TOO LOW)\nACTUAL TAS=",TAS_DATA,"\nTAS DATA AVAILABLE:", velo)
	               print("\n--> CHANGE TAS1 OR/AND TAS2 IN MISSION PHASE  NR.",count+1,"\n")
	               exit()
	       if TAS_DATA > velo[len(velo)-1]:
	               print("\n########## ERROR ON REQUESTED TAS ##########")
	               print("INTERPOLATION ROUTINE FAILED FOR ALTITUDE:",alt,"m (REQUESTED TAS IS TOO HIGH)\nACTUAL TAS=",TAS_DATA,"\nTAS DATA AVAILABLE:", velo)
	               print("\n--> CHANGE TAS1 OR/AND TAS2 IN MISSION PHASE NR.",count+1,"\n")
	               exit()  
	
	       for i in range(1,len(velo)):
	               if velo[i-1] < TAS_DATA and TAS_DATA < velo[i]:
	        	       upper_index = i
	        	       lower_index = i-1
	        	       upper_fac= (TAS_DATA-velo[i-1])/(velo[i] - velo[i-1])
	        	       lower_fac= (velo[i]-TAS_DATA)/(velo[i] - velo[i-1])
	       for i in range(0,len(velo)):
	               if velo[i] == TAS_DATA:
	        	       upper_index = i
	        	       lower_index = i
	        	       lower_fac=1
	        	       upper_fac=0
	       lower_csv_file = csv_datapath[lower_index]
	       upper_csv_file = csv_datapath[upper_index]
	
	       ################################################################
	       f_lower=open(lower_csv_file,'r')
	       f_upper=open(upper_csv_file,'r')
	
	       datafile_lower = []
	
	       #### PARSE THE LOWER FILE ######################
	       line = f_lower.readline()       
	       while(line !=''):
	               line = f_lower.readline()
	               if line !='':
	        	       if float(line.strip().split(',')[3]) == float(DBETA):
	        		       datafile_lower.append(line.strip().split(','))

	       datafile_upper = []
	
	       #### PARSE THE UPPER FILE ######################
	       line = f_upper.readline()       

	       while(line !=''):
	               line = f_upper.readline()
	               if line !='':
	        	       if float(line.strip().split(',')[3]) == float(DBETA):
	        		       #print(line.strip().split(',')[3])      
	        		       datafile_upper.append(line.strip().split(','))
	
	       if( np.amin(dbeta_check)  > DBETA or DBETA > np.amax(dbeta_check) ):
	               print("\n#### ERROR IN REQUESTED DBETA ####\nALTITUDE:",alt)		       
	               print("DBETA REQUESTED",DBETA)
	               print("DBETA MIN DATA:",np.amin(dbeta_check),"DBETA MAX DATA:", np.amax(dbeta_check),"\n")
	               exit()
	               
	       #### INTERPOLATE UPPER AND LOWER LIST ######################
	       datafile_interpol=np.zeros((np.shape(datafile_upper)[0],np.shape(datafile_upper)[1]))
	
	       for i in range(0,np.shape(datafile_upper)[0]):
	               for j in range(0,np.shape(datafile_upper)[1]):
	        	       datafile_interpol[i][j] = list(np.float_(datafile_lower))[i][j]*lower_fac + list(np.float_(datafile_upper))[i][j]*upper_fac
	else:
		# THIS IS A DUMMY FOR THE GLIDE SECTION WHERE INTERPOLATED DATA IS NOT REQUIRED
		datafile_interpol=np.zeros((2,2))
				
	return(datafile_interpol)

################################################################################
def INTERPOL_MOT_DAT_OVER_ALT(MOT_DAT1,MOT_DAT2,act_altitude,act_climbrate):
	
	#### INTERPOLATE UPPER AND LOWER LIST FOR ALL CLIMBRATES ######################
	datafile_interpol=np.zeros((np.shape(MOT_DAT1)[0],np.shape(MOT_DAT1)[1]))
	
	if((MOT_DAT2[0][0] - MOT_DAT1[0][0]) > 0):
		upper_fac = (act_altitude - MOT_DAT1[0][0]) / (MOT_DAT2[0][0] - MOT_DAT1[0][0])
		lower_fac = (MOT_DAT2[0][0] - act_altitude) / (MOT_DAT2[0][0] - MOT_DAT1[0][0])
	else:
		upper_fac = 1.
		lower_fac = 0.
		
	
	for i in range(0,np.shape(MOT_DAT1)[0]):
		for j in range(0,np.shape(MOT_DAT1)[1]):
			datafile_interpol[i][j] = MOT_DAT1[i][j]*lower_fac + MOT_DAT2[i][j]*upper_fac
	
	#### NOW INTERPOL ALL VARIABLES FOR THE ACTUAAL CLIMBRATE ########################
	
	x=np.zeros(np.shape(datafile_interpol)[0])
	y=np.zeros(np.shape(datafile_interpol)[0])
	datafile_interpol_act_CR=np.zeros(np.shape(datafile_interpol)[1])
	xint=act_climbrate
	

	
	for i in range(0,np.shape(datafile_interpol)[1]): ## LOOP OVER VARIABLES
		for j in range(0,np.shape(datafile_interpol)[0]): ## LOOP OVER CLIMBRATES
			x[j]=datafile_interpol[j][2]
			y[j]=datafile_interpol[j][i]

		tck = interpolate.splrep(x, y, s=0)
		yint = interpolate.splev(xint, tck, der=0)
		datafile_interpol_act_CR[i]=yint


	return(datafile_interpol_act_CR)
