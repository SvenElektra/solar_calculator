#!/usr/bin/python
import datetime, time, os, os.path, string
import numpy as np
from numpy import dot
from numpy.linalg import norm
import math
from subprocess import call
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.spatial as spatial
from operator import add
import matplotlib as mpl
from matplotlib import cm
from matplotlib import rc
from matplotlib.ticker import LinearLocator, FormatStrFormatter


#########################################################################################
def angle_between(a,b):
  arccosInput = dot(a,b)/norm(a)/norm(b)
  arccosInput = 1.0 if arccosInput > 1.0 else arccosInput
  arccosInput = -1.0 if arccosInput < -1.0 else arccosInput
  return math.acos(arccosInput)*180./np.pi

#########################################################################################
def cell_surf_transmission(angle):
  # Values based on Gochermann-Diagram
  angles=[0.,40.,60.,80.,90.]
  transmissions=[0.9825,0.99,0.9825,0.97,0.]
  surf_trans = np.interp(angle,angles,transmissions)
  return surf_trans
  
#########################################################################################
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta in degrees.
    """
    theta=theta*np.pi/180.
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
#########################################################################################

def GET_SUN_PROJ_SURF(sun_vector,airc_pitch):
     
     theta_pitch=[]
     theta_dihedral=[]
     direction=[]
     direction_opt=[]
     solcell_count=[]
     azimuth=[]
     return_arg=[]

     ##### SOLAR CELLS ON RIGHT WING ######################################################
     
     # DIRECTION NUMBER 1
     theta_pitch.append(-18.7)
     theta_dihedral.append(-6.5)
     solcell_count.append(75)

     # DIRECTION NUMBER 2
     theta_pitch.append(-8.)
     theta_dihedral.append(-6.5)
     solcell_count.append(85)

     # DIRECTION NUMBER 3
     theta_pitch.append(-2.3)
     theta_dihedral.append(-6.5)
     solcell_count.append(95)

     # DIRECTION NUMBER 4
     theta_pitch.append(2.8)
     theta_dihedral.append(-6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 5
     theta_pitch.append(6.8)
     theta_dihedral.append(-6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 6
     theta_pitch.append(10.5)
     theta_dihedral.append(-6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 7
     theta_pitch.append(13.6)
     theta_dihedral.append(-6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 8
     theta_pitch.append(19.5)
     theta_dihedral.append(-6.5)
     solcell_count.append(105)

     # DIRECTION NUMBER 9
     theta_pitch.append(17.1)
     theta_dihedral.append(-6.5)
     solcell_count.append(105)

     ##### SOLAR CELLS ON LEFT WING ########################################################

     # DIRECTION NUMBER 10
     theta_pitch.append(-18.7)
     theta_dihedral.append(6.5)
     solcell_count.append(75)
     
     # DIRECTION NUMBER 11
     theta_pitch.append(-8.)
     theta_dihedral.append(6.5)
     solcell_count.append(85)
     
     # DIRECTION NUMBER 12
     theta_pitch.append(-2.3)
     theta_dihedral.append(6.5)
     solcell_count.append(95)

     # DIRECTION NUMBER 13
     theta_pitch.append(2.8)
     theta_dihedral.append(6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 14
     theta_pitch.append(6.8)
     theta_dihedral.append(6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 15
     theta_pitch.append(10.5)
     theta_dihedral.append(6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 16
     theta_pitch.append(13.6)
     theta_dihedral.append(6.5)
     solcell_count.append(106)

     # DIRECTION NUMBER 17
     theta_pitch.append(19.5)
     theta_dihedral.append(6.5)
     solcell_count.append(105)

     # DIRECTION NUMBER 18
     theta_pitch.append(17.1)
     theta_dihedral.append(6.5)
     solcell_count.append(105)

     ##### SOLAR CELLS ON HORIZONTAL STABILIZER ########################################################

     # DIRECTION NUMBER 19
     theta_pitch.append(0.0)
     theta_dihedral.append(0.0)
     solcell_count.append(88)
     
     ##### SOLAR CELLS ON FUSELAGE TOP ########################################################

     # DIRECTION NUMBER 20
     theta_pitch.append(0.0)
     theta_dihedral.append(0.0)
     solcell_count.append(88)
     
     ##### SOLAR CELLS WINGLETS LEFT (ORIENTATION UNKNOWN) ########################################################

     # DIRECTION NUMBER 21
     theta_pitch.append(0.0)
     theta_dihedral.append(-6.5)
     solcell_count.append(30)

     ##### SOLAR CELLS WINGLETS RIGHT (ORIENTATION UNKNOWN) ########################################################

     # DIRECTION NUMBER 22
     theta_pitch.append(0.0)
     theta_dihedral.append(6.5)
     solcell_count.append(30)
     
     ##### SOLAR CELLS ON FUSELAGE SIDE LEFT ########################################################

     # DIRECTION NUMBER 23
     theta_pitch.append(0.0)
     theta_dihedral.append(-90.)
     solcell_count.append(121)

     ##### SOLAR CELLS ON FUSELAGE SIDE RIGHT ########################################################

     # DIRECTION NUMBER 24
     theta_pitch.append(0.0)
     theta_dihedral.append(90.)
     solcell_count.append(121)   

     ##### NETTO SURFACE AREA PER SINGLE CELL ########################################################
     #cell_surf_area=0.0151589 #square-meters, OLD CELL TYPE
     cell_surf_area=0.015329 #square-meters, NEW CELL TYPE, A_new is by factor of 1.01504 bigger than A_old. Cell has efficiency value of 25.1%

     #########################################################################################
     
     normal = [0, 0, -cell_surf_area]

     ##### ROTATION IN AIRCRAFT COOS ###########
     rotaxis_pitch=np.array([0,1,0])
     rotaxis_dihedral=np.array([1,0,0])

     for i in range(0,len(theta_dihedral)):
	     v_rot1=(np.dot(rotation_matrix(rotaxis_pitch,theta_pitch[i]), normal))           ### Solar Cells Inclination around y-axis on Airfoil
	     v_rot2=(np.dot(rotation_matrix(rotaxis_dihedral,theta_dihedral[i]), v_rot1))     ### Solar Cells Tilt Angle due to Dihedral
	     v_rot3=(np.dot(rotation_matrix(rotaxis_pitch,airc_pitch), v_rot2))               ### Solar Cells Inclintaion around y-axis due to global pitch angle of aircraft
	     direction.append(np.array([v_rot3]))
	     direction_opt.append(np.array([0.,0.,0.]))

     for i in range(0,len(theta_dihedral)):
	     direction[i]=direction[i]*solcell_count[i]

     total_cells_count=0
     for i in range(0,len(theta_dihedral)):
	     total_cells_count=total_cells_count+solcell_count[i]
     
     ### PROBABLY NOT NECESSARY TO NORMALIZE BUT IN CASE OF ... ########
     sun_vector_normalized=sun_vector/np.sqrt(sun_vector[0]*sun_vector[0]+sun_vector[1]*sun_vector[1]+sun_vector[2]*sun_vector[2])

     sun_proj_surf=0.
     for i in range (0,len(direction)):	
	     if np.dot(direction[i],sun_vector_normalized) < 0:
		     sun_proj_surf=sun_proj_surf+np.abs(np.dot(direction[i],sun_vector_normalized))

     horizontal_proj_surf=0.
     for i in range (0,len(direction)):	
	     if np.dot(direction[i],[0.,0.,1.]) < 0:
		     horizontal_proj_surf=horizontal_proj_surf+np.dot(direction[i],[0.,0.,-1.])

     rotaxis=np.array([0.,0.,1.])
     s = (360,len(direction))
     sol_proj=np.zeros(s) 
     proj_sum_circ=np.zeros(360)
     proj_avg_dir=np.zeros(len(direction))    
     
     ########### CALCULATE PROJECTION OVER 360 DEGREES ##################################
     for i in range(0,360):
          azimuth.append(i)
          for j in range(0,len(direction)):
                    if np.dot(np.dot(rotation_matrix(rotaxis,float(i)),direction[j][0]),sun_vector_normalized) < 0.:
                              sol_proj[i,j]=np.abs(np.dot(np.dot(rotation_matrix(rotaxis,float(i)),direction[j][0]),sun_vector_normalized))
                    else:
                              sol_proj[i,j]=0.
  
     ### SUM UP ALL PROJECTIONS OF ALL DIRECTIONS OVER 360 DEGREES ##############
     for i in range (0,360):
               proj_sum_circ[i] = np.sum(sol_proj[i,:])

     ### AVERAGE THE PROJECTION FOR EACH DIRECTION OVER 360 DEGREES ################
     for j in range (0,len(direction)):
          proj_avg_dir[j] = np.average(sol_proj[:,j])

     ### ROTATE THE AIRCRAFT TO GET THE BEST ORIENATION ##############
     for j in range(0,len(direction)):
          direction_opt[j]=np.array([np.dot(rotation_matrix(rotaxis,float(np.argmax(proj_sum_circ))),direction[j][0])])

     return_arg.append(np.amax(proj_sum_circ))               	# SPS0
     return_arg.append(np.argmax(proj_sum_circ))             	# SPS1
     return_arg.append(np.average(proj_sum_circ))            	# SPS2
     return_arg.append(total_cells_count)                	# SPS3
     return_arg.append(total_cells_count*cell_surf_area) 	# SPS4
     return_arg.append(horizontal_proj_surf[0])          	# SPS5
     return_arg.append(sun_vector_normalized)            	# SPS6
     return_arg.append(np.amin(proj_sum_circ))                	# SPS7 
     return_arg.append(np.argmin(proj_sum_circ))              	# SPS8
     return_arg.append(len(direction))                   	# SPS9
     return_arg.append(direction)                        	# SPS10
     return_arg.append(direction_opt)                    	# SPS11
     return_arg.append(solcell_count)                    	# SPS12
     return_arg.append(proj_avg_dir)                         	# SPS13
     return_arg.append(sol_proj)                         	# SPS14
          
     del theta_pitch
     del theta_dihedral
     del direction
     del direction_opt
     del solcell_count
     del azimuth
          
     return return_arg
