#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.spatial as spatial
import sys
from collections import Counter
from operator import add
import matplotlib as mpl
from matplotlib import cm
from matplotlib import rc
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from functions import MIN_POWER_HOVER
from functions import GET_BEST_EFF
from functions import INTERPOL_ALT_CLIMBRATE
from functions import INTERPOL_ALT_POWER
from functions import getStandardPressure
from functions import getStandardTemperature
from functions import getUserDefTemperature_CELSin_KELout
from functions import getGeopotential
from functions import getEfficiencySolarcells
from functions import getSolarPowerDensity
from functions import getCosPsi
from functions import getAtmoTransmission
from functions import getDepression
from functions import make_format
from functions import getSolCellTempEnerBal
from functions import getSolCellTempCalibrated
from functions import GET_REYNOLDS
from functions import GET_MACH
from functions import GET_SUNVECTOR
from functions import GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo
from functions import GET_BAT_VOLTAGE_CURRENT_52V
from functions import GET_BAT_VOLTAGE_14S_20P_3500_LiIo
from functions import GET_BAT_VOLTAGE_52V
from functions import SOUNDSPEED
from functions import DYNPRESS
from functions import TAS2CAS_mps
from functions import TAS2CAS_kmh
from functions import GET_MAXRPM
from functions import SCALE_TAS
from functions import GET_CSVDATAPATH
from functions import INTERPOL_CSV_FILES_VELO
from functions import INTERPOL_MOT_DAT_OVER_ALT
from vectors import rotation_matrix
from vectors import GET_SUN_PROJ_SURF
from vectors import angle_between
from vectors import cell_surf_transmission

# print entire numpy arrays
np.set_printoptions(threshold=np.inf)



print("############################################");
print("###      Solar Mission Calculator        ###");
print("###         Sven Schmid                  ###");
print("############################################");


# Which max. altitude error is still acceptable between two consecutive phases?
altitude_mismatch_limit = 500.

# How many points should be plotted maximally in the ressult-diagrams?
max_plotted_points=200

altitude1=[]
altitude2=[]
climbrate1=[]
climbrate2=[]
dbeta1=[]
dbeta2=[]
tas1=[]
tas2=[]

CR=[]
redpower=[]
mission_phase_type=[]
phase_duration=[]
altitude_polar=[]
polarname=[]
subdirectories=[]
read_input=open("input_solar.inp",'r')
line=''
maxeff_filepath=[]
redsink_filepath=[]
polardiag_filepath=[]

mot_data_M_BestEff=[]
redsink_M=[]
mission_data_Efficiency_TOT=[]
mission_data_time=[]
mission_data_real_time_h=[]

mission_data_altitude=[]
mission_data_velocity=[]
mission_data_velocity_CAS=[]
mission_data_reynolds=[]
mission_data_mach=[]
mission_data_climbrate=[]
mission_data_DBeta=[]
mission_data_Efficiency_prop=[]
mission_data_Efficiency_EPU=[]
mission_data_Efficiency_MOT_MC=[]
mission_data_RPM=[]
mission_data_RPM_MAX=[]
mission_data_Pprop=[]
mission_data_Pshaft=[]
mission_data_Ptot=[]
mission_data_PBat_ex=[]
mission_data_PBat_int=[]
mission_data_Thrust=[]
mission_data_Q=[]
mission_data_Mot_Current=[]
mission_data_Bat_Current=[]
mission_data_Bat_Voltage=[]
mission_data_PL_Bat=[]
mission_data_PL_MC=[]
mission_data_PL_PROP=[]
mission_data_PL_MOT_COPPER=[]
mission_data_PL_MOT_BE_FE=[]
mission_data_PL_MOT_TOT=[]
mission_data_PL_MOT_TOT_MEAS=[]
mission_data_CURR_CONST_MEAS=[]
mission_data_PL_BATT_CHARGE_CHEM=[]
mission_data_PL_BATT_CHARGE_OHM=[]
mission_data_batt_charging_power=[]
mission_data_batt_charging_current=[]
mission_data_excess_solar_power=[]
mission_data_maxcurr=[]
mission_data_peakcurr=[]

mission_data_batt_heatup=[]
mission_data_batt_heatup_delta=[]

mission_data_sol_pow_avg=[]
mission_data_sol_pow_max=[]
mission_data_sol_pow=[]

mission_data_atmo_transmission=[]
mission_data_incangle=[]
mission_data_CL=[]
mission_data_alfa=[]
mission_data_density=[]


mission_data_cell_temp_individual=[]
mission_data_cell_temp_min=[]
mission_data_cell_temp_max=[]
mission_data_cell_temp_avg=[]

mission_data_cell_efficiency_individual=[]
mission_data_cell_efficiency_min=[]
mission_data_cell_efficiency_max=[]
mission_data_cell_efficiency_avg=[]

mission_data_best_heading=[]
mission_data_distance_east=[]
mission_data_distance_north=[]

mission_data_Energy_Spent=[]
mission_data_Potential_Energy=[]
mission_data_Energy_Remaining=[]
mission_data_harvested_Sol_Energy=[]
mission_data_hour_angle=[]
mission_data_declination_angle=[]
mission_data_zenith_angle=[]
mission_data_azimuth_angle=[]
mission_data_sunvector_x=[]
mission_data_sunvector_y=[]
mission_data_sunvector_z=[]

atmo_temp_altitude=[]
atmo_temp_temp=[]
solcel_limtemp_alt=[]
solcel_limtemp_mintemp=[]
solcel_limtemp_maxtemp=[]

altcurve=[]
altcurve_real=[]
tempcurve=[]
#tempcurve_cells=[]
presscurve=[]
densitycurve=[]
densitycurve_real=[]
#solefficiencycurve=[]
#solefficiencycurve_plus_add=[]
dayofyearcurve=[]
timeofdaycurve=[]
cospsicurve=[]
incanglecurve=[]
solarenergydensitycurve=[]
presscurve_proc=[]
densitycurve_proc=[]







##### VARIABLES INDICES ###############
#   0 - altitude[m]        
#   1 - velocity[m/s]
#   2 - climbrate[m/s]
#   3 - DBeta[deg]
#   4 - Efficiency_prop[%]
#   5 - Efficiency_EPU[%]
#   6 - Efficiency_TOT[%]
#   7 - RPM
#   8 - Pprop[W]
#   9 - Pshaft[W]
#   10 - Ptot[W]
#   11 - Thrust[N]
#   12 - Q[Nm]
#   13 - Mot_Current[A]
#   14 - Bat_Current[A]   
#   15 - PL_Bat[W]
#   16 - PL_MC[W]
#   17 - PL_PROP[W]
#   18 - PL_MOT_COPPER[W]
#   19 - PL_MOT_BE+FE[W]
#   20 - PL_MOT_TOT[W]

print("\n########## PARSING INPUT FILE ###################\n")

##### PARSE MAIN PATH OF MOTORIZED DATA #################################
while(line != '#### MAIN PATH OF MOTORIZED DATA ####'):
	line = str.strip(read_input.readline())

main_path_mot_dat = str.strip(read_input.readline())


##### SUBDIRECTORIES MOTORIZED DATA #################################
while(line != '#### SUBDIRECTORIES MOTORIZED DATA ####'):
	line = str.strip(read_input.readline())
	
while(line !='\n'):
	line = read_input.readline()
	if (line !='\n'):
		subdirectories.append(str.strip(line))

for i in range(0,len(subdirectories)-1):
	if float(str(subdirectories[i])[14:17]) - float(str(subdirectories[i+1])[14:17]) == 0.0:
		aircraft_mass=float(str(subdirectories[0])[14:17])
	else:
		print("\n###### Motorized Aircraft Mass inconsistent #######\n")
		exit()

##### PARSE MAIN PATH OF POLAR DIAGRAMS #################################
while(line != '#### MAIN PATH OF POLAR DIAGRAMS ####'):
	line = str.strip(read_input.readline())

main_path_pol_diag = str.strip(read_input.readline())


##### POLAR DIAGRAMS #################################
while(line != '#### POLAR DIAGRAMS ####'):
	line = str.strip(read_input.readline())
while(line !='\n'):
	line = read_input.readline()
	if (line !='\n'):
		altitude_polar.append(1000.*(float(str(line)[0:2])+float(str(line)[3:4])/10.))
		polarname.append(str.strip(line))

for i in range(0,len(polarname)-1):
	if float(str(polarname[i])[7:10]) - float(str(polarname[i+1])[7:10]) == 0.0:
		polar_mass=float(str(polarname[0])[7:10])
	else:
		print("\n###### Polar Diagramm Mass inconsistent #######\n")
		exit()

if polar_mass == aircraft_mass:
	print("Aircraft Mass [kg]= ",polar_mass)
else:
	print("\n######## Aircraft Mass and Polar Diagram Mass mismatch! #######\n")
	exit()



#### PARSE AIRCRAFT HEADING STRATEGY ####
while(line != '#### AIRCRAFT HEADING STRATEGY (1 - OPTIMIZED HEADING, 2 - AVERAGE OF 360 DEG) ####'):
	line = str.strip(read_input.readline())
heading_strategy = float(str.strip(read_input.readline()))


##### PARSE LATITUDE #################################
while(line != '#### LATITUDE ####'):
	line = str.strip(read_input.readline())
latitude = float(str.strip(read_input.readline()))

	
##### PARSE DAY OF YEAR #################################
while(line != '#### DAY OF YEAR ####'):
	line = str.strip(read_input.readline())
day_of_year = float(str.strip(read_input.readline()))


##### LOCAL TAKEOFF TIME #################################
while(line != '#### LOCAL TAKEOFF TIME [HH:MIN], 24HR-SYSTEM ####'):
	line = str.strip(read_input.readline())
line=read_input.readline()
takeoff_time_hr = float(line.strip().split(":")[0])
takeoff_time_min = float(line.strip().split(":")[1])
takeoff_time = takeoff_time_hr + takeoff_time_min/60.


##### LOCAL SUN CULMINATION TIME ##########################
while(line != '#### LOCAL SUN CULMINATION AT LOCAL TIME [HH:MIN], 24HR-SYSTEM ####'):
	line = str.strip(read_input.readline())
line=read_input.readline()
sunculm_time_hr = float(line.strip().split(":")[0])
sunculm_time_min = float(line.strip().split(":")[1])
sunculm_time = sunculm_time_hr + sunculm_time_min/60.

sunculm_delta_time_h = 12. - sunculm_time

##### PARSE CALCULATION TYPE #################################
while(line != '#### SIMULATION TYPE: OPTIMIZATION (OPT) OR RECALCULATION (RECALC) ####'):
	line = str.strip(read_input.readline())
calc_type = str(str.strip(read_input.readline()))


if(calc_type != "OPT" and calc_type != "RECALC" ):
	print("#### ERROR: WRONG CALCULATION TYPE SPECIFIED ###########")
	exit()

print("Calculation Type (Optimization/Recalculation):",calc_type)
print()
##### PARSE MISSION DEFINITION #################################
mission_counter=0
while(line.strip() !='#### MISSION PROFILE DEFINITION END ####'):
       line = read_input.readline()
       if (line.strip() == '#### MISSION SEGMENT ####'):
	       line = read_input.readline()
	       if(str.strip(line).split()[1] == 'CLIMB' ):
		       mission_counter+=1
		       print("Mission Phase",mission_counter," - CLIMB")
		       mission_phase_type.append('CL_A1_CR1_A2_CR2')
		       altitude1.append(float(read_input.readline().strip().split()[1]))
		       altitude2.append(float(read_input.readline().strip().split()[1]))
		       climbrate1.append(float(read_input.readline().strip().split()[1]))
		       climbrate2.append(float(read_input.readline().strip().split()[1])) 
		       dbeta1.append(float(read_input.readline().strip().split()[1]))
		       dbeta2.append(float(read_input.readline().strip().split()[1]))
		       tas1.append(float(read_input.readline().strip().split()[1]))
		       tas2.append(float(read_input.readline().strip().split()[1]))  
		       phase_duration.append(float(-999))
		       redpower.append(float(-999))
	       if(str.strip(line).split()[1] == 'HOVER' ):
		       mission_counter+=1
		       print("Mission Phase",mission_counter," - HOVER")
		       mission_phase_type.append('HOVER_ALTIT_TIME')
		       dummy=float(read_input.readline().strip().split()[1])
		       altitude1.append(dummy)
		       altitude2.append(dummy)
		       phase_duration.append(float(read_input.readline().strip().split()[1]))
		       dummy=float(read_input.readline().strip().split()[1])
		       dbeta1.append(dummy)
		       dbeta2.append(dummy)
		       climbrate1.append(float(0))
		       climbrate2.append(float(0))
		       dummy=float(read_input.readline().strip().split()[1])
		       tas1.append(dummy)
		       tas2.append(dummy)  
		       redpower.append(float(-999))

	       if(str.strip(line).split()[1] == 'GLIDE' ):
		       mission_counter+=1
		       print("Mission Phase",mission_counter," - GLIDE")
		       mission_phase_type.append('GLIDE_TO_ALTITUD')
		       dummy=float(read_input.readline().strip().split()[1])
		       altitude1.append(dummy)
		       altitude2.append(dummy)
		       climbrate1.append(float(-999))
		       climbrate2.append(float(-999))
		       phase_duration.append(float(-999))
		       redpower.append(float(-999))
		       tas1.append(float(-999))
		       tas2.append(float(-999))
		       dbeta1.append(float(-999))
		       dbeta2.append(float(-999))
	       
	       if(str.strip(line).split()[1] == 'SUNPOWCALC' ):
		       print("CALCULATE SONPOWER ONLY - NO MISSION SIMULATION!")
		       mission_phase_type.append('SUN_POW_ALT_VELO')
		       dummy=float(read_input.readline().strip().split()[1])
		       altitude1.append(dummy)
		       altitude2.append(dummy)
		       velocity=float(read_input.readline().strip().split()[1]) 
		       tas1.append(velocity)
		       tas2.append(velocity)  
	       
	       #if(str.strip(line).split()[0] == 'FLY_WIT_SUN_TIME:' ):
		       #print("FLY WITH SUN")
		       #mission_phase_type.append('FLY_WIT_SUN_TIME')
		       #altitude1.append(float(-999))
		       #altitude2.append(float(-999))
		       #climbrate1.append(float(-999))
		       #climbrate2.append(float(-999))
		       #phase_duration.append(str.strip(line).split()[1])
		       #redpower.append(float(-999))		



print("")

if(calc_type == "RECALC"):
	for i in range(0,len(altitude1)):
		diff = altitude2[i]-altitude1[i]
		if diff > 4000.:
			print("\n#### ERROR: ALTITUDE INCREMENT IN SEGMENT",i,"IS",diff," SHOULD NOT BE HIGHER THAN 4000 IN RECALC MODE! PLEASE REDUCE AND RETRY #########\n")
			exit()



##### Parse BATTERY CAPACITY, NUMBER OF BATTERIES AND RESISTANCE #################################
while(line != '#### NUMBER OF BATTERY PACKS, CAPACITY OF SINGLE BATTERY [Wh], INTERNAL BATTERY RESISTANCE (PER PACK) ####'):
	line = str.strip(read_input.readline())
line = str.strip(read_input.readline())
number_of_batteries = int(str.strip(line).split(",")[0])
single_bat_cap = float(str.strip(line).split(",")[1])
bat_cap=number_of_batteries*single_bat_cap
battery_int_res = float(str.strip(line).split(",")[2])

#### BATTERY HEATING CONSTANT ####
while(line != '#### BATTERY HEATING CONSTANT ####'):
	line = str.strip(read_input.readline())
bat_heat_const = str(str.strip(read_input.readline()))

#### BATTERY MODEL: "14S_20P_3500_LiIo" OR "52V" ####
while(line != '#### BATTERY MODEL: "14S_20P_3500_LiIo" OR "52V" ####'):
	line = str.strip(read_input.readline())
bat_type = str(str.strip(read_input.readline()))



#### BATTERY ENERGY CONTANT AT START [Wh] ####
while(line != '#### BATTERY ENERGY CONTANT AT START [Wh] ####'):
	line = str.strip(read_input.readline())
bat_cap_status_start = float(str.strip(read_input.readline()))


#### MPPT EFFICIENCY [%] ####
while(line != '#### MPPT EFFICIENCY [%] ####'):
	line = str.strip(read_input.readline())

mppt_eff = float(str.strip(read_input.readline()))

#### MPPT CUTOFF POWER PER SINGLE CELL [W] ####
while(line != '#### MPPT CUTOFF POWER PER SINGLE CELL [W] ####'):
	line = str.strip(read_input.readline())
mppt_cutoff_power_per_cell = float(str.strip(read_input.readline()))


#### SOLAR CELLS NOMINAL EFFICIENCY [%], EFFICIENCY INCREASE PER K ####
while(line != '#### SOLAR CELLS NOMINAL EFFICIENCY [%], EFFICIENCY INCREASE PER K ####'):
	line = str.strip(read_input.readline())
line = str.strip(read_input.readline())
solcell_nom_eff = float(str.strip(line).split(",")[0])
eff_grad = float(str.strip(line).split(",")[1])


#### CHEMICAL BATTERY CHARGING LOSSES [%], IN ADDITION TO OHM LOSSES ####
while(line != '#### CHEMICAL BATTERY CHARGING LOSSES [%], IN ADDITION TO OHM LOSSES ####'):
	line = str.strip(read_input.readline())
bat_chg_red_chem = float(str.strip(read_input.readline()))

#### CABLE LOSSES [%] ####
while(line != '#### CABLE LOSSES [%] ####'):
	line = str.strip(read_input.readline())
cable_loss = float(str.strip(read_input.readline()))

#### SYSTEMS AND PAYLOAD POWER DRAIN [W] ####
while(line != '#### SYSTEMS AND PAYLOAD POWER DRAIN [W] ####'):
	line = str.strip(read_input.readline())
sys_pwr_drn = float(str.strip(read_input.readline()))

#### AIRCRAFT REFERENCE AREA [sqm] ####
while(line != '#### AIRCRAFT REFERENCE AREA [sqm] ####'):
	line = str.strip(read_input.readline())
AircraftReferenceArea = float(str.strip(read_input.readline()))

#### REFERENCE LENGTH FOR REYNOLDS NUMBER [m] ####
while(line != '#### REFERENCE LENGTH FOR REYNOLDS NUMBER [m] ####'):
	line = str.strip(read_input.readline())
ReynoldsReferenceLength = float(str.strip(read_input.readline()))

#### TIMESTEP OF SIMULATION [s] ####
while(line != '#### TIMESTEP OF SIMULATION [s] ####'):
	line = str.strip(read_input.readline())
timestep = float(str.strip(read_input.readline()))

#### MAX. TOTAL CURRENT [A] FOR ALL MOTORS TOGETHER, ONLY FOR PLOTTING [s] ####
while(line != '#### MAX. TOTAL CURRENT [A] FOR ALL MOTORS TOGETHER, ONLY FOR PLOTTING [s] ####'):
	line = str.strip(read_input.readline())
max_current = float(str.strip(read_input.readline()))

#### PEAK CURRENT [A] FOR ALL MOTORS TOGETHER, ONLY FOR PLOTTING [s] ####
while(line != '#### PEAK CURRENT [A] FOR ALL MOTORS TOGETHER, ONLY FOR PLOTTING [s] ####'):
	line = str.strip(read_input.readline())
peak_current = float(str.strip(read_input.readline()))

#### SHOULD A CSV-TEXTFILE WITH ALL RESULTS BE CREATED? 1-YES, 0-NO ####
while(line != '#### SHOULD A CSV-TEXTFILE WITH ALL RESULTS BE CREATED? 1-YES, 0-NO ####'):
	line = str.strip(read_input.readline())
writedata = float(str.strip(read_input.readline()))

#### BATCH MODE [1] OR INTERACTIVE MODE [2] ####
while(line != '#### BATCH MODE [1] OR INTERACTIVE MODE [2] ####'):
	line = str.strip(read_input.readline())
batchmode = int(str.strip(read_input.readline()))

#####################################
#### WHICH DIAGRAMS SHOULD BE CREATED? 1-IF YES, ALL OTHER FOR NO ####
while(line != '#### ALTITUDE, VELOCITY AND CLIMBRATE ####'):
	line = str.strip(read_input.readline())
p1 = float(str.strip(read_input.readline()))

while(line != '#### EFFICIENCY AND POWER ####'):
	line = str.strip(read_input.readline())
p2 = float(str.strip(read_input.readline()))

while(line != '#### SOLAR CELLS EFFICIENCY AND POWER ####'):
	line = str.strip(read_input.readline())
p3 = float(str.strip(read_input.readline()))

while(line != '#### TOTAL POWER AND ENERGY ####'):
	line = str.strip(read_input.readline())
p4 = float(str.strip(read_input.readline()))

while(line != '#### TORQUE AND THRUST ####'):
	line = str.strip(read_input.readline())
p5 = float(str.strip(read_input.readline()))

while(line != '#### LIFT COEFFICIENT AND DBETA ####'):
	line = str.strip(read_input.readline())
p6 = float(str.strip(read_input.readline()))

while(line != '#### ATHOSPHERIC DATA ####'):
	line = str.strip(read_input.readline())
p7 = float(str.strip(read_input.readline()))

while(line != '#### AIRCRFT POLAR DIAGRAM ####'):
	line = str.strip(read_input.readline())
p8 = float(str.strip(read_input.readline()))

while(line != '#### POWER LOSSES DIAGRAM ####'):
	line = str.strip(read_input.readline())
p9 = float(str.strip(read_input.readline()))

while(line != '#### BATTERY CHARGE LOSSES DIAGRAM ####'):
	line = str.strip(read_input.readline())
p10 = float(str.strip(read_input.readline()))

while(line != '#### RPM AND CURRENT DIAGRAM ####'):
	line = str.strip(read_input.readline())
p11 = float(str.strip(read_input.readline()))

while(line != '#### RE AND MACH DIAGRAM ####'):
	line = str.strip(read_input.readline())
p12 = float(str.strip(read_input.readline()))

while(line != '#### FLIGHT PATH AND HEADING DIAGRAM ####'):
	line = str.strip(read_input.readline())
p13 = float(str.strip(read_input.readline()))

while(line != '#### BATTERY TEMPERATURE INCREASE ####'):
	line = str.strip(read_input.readline())
p14 = float(str.strip(read_input.readline()))

while(line != '#### DBETA TAS CAS CLIMBRATE RPM VS ALTITUDE ####'):
	line = str.strip(read_input.readline())
p15 = float(str.strip(read_input.readline()))

while(line != '#### TEMPERATURE PROFILE BASED ON STANDARD ATMOSPHERE (1) OR USER-DEFINED (2)? ####'):
	line = str.strip(read_input.readline())
tempprofile_flag = float(str.strip(read_input.readline()))

while(line != '#### ALTITUDE_AMSL_METERS TEMPERATURE_CELSIUS ####'):
	line = str.strip(read_input.readline())
while(line !='\n'):
	line = read_input.readline()
	if line !='\n':
		atmo_temp_altitude.append(float(str.split(str.strip(line))[0]))
		atmo_temp_temp.append(float(str.split(str.strip(line))[1]))

while(line != '#### SOLCEL TEMPERATURE LIMITS - ALTITUDE[m]    MIN TEMP[degC]   MAX TEMP[degC] ####'):
	line = str.strip(read_input.readline())
while(line !='\n'):
	line = read_input.readline()
	if line !='\n':
		solcel_limtemp_alt.append(float(str.split(str.strip(line))[0]))
		solcel_limtemp_mintemp.append(float(str.split(str.strip(line))[1]))
		solcel_limtemp_maxtemp.append(float(str.split(str.strip(line))[2]))


read_input.close()














####################################################################

number_mot_data=len(subdirectories)

if(number_mot_data < 4):
	print("#####################################################################################")
	print("### Number of Motorized Data is %d. This is lower than Minimum \"4\". Make sure that ###" %(number_mot_data))
	print("### Motorized Data is available for at least four different Altitude Levels.      ###")
	print("#####################################################################################\n\n")
	exit()
	
#print(number_mot_data)
number_polars=len(polarname)

for i in range (0,number_mot_data):
	maxeff_filepath.append(main_path_mot_dat+"/"+subdirectories[i]+"/Max_tot_Eff.csv")
for i in range (0,number_mot_data):
	redsink_filepath.append(main_path_mot_dat+"/"+subdirectories[i]+"/Reduced_Sink.csv")
for i in range (0,number_polars):
	polardiag_filepath.append(main_path_pol_diag+"/"+polarname[i])

####################### READ MOTORIZED DATA ##############################################
#### READ IN Max_tot_Eff.csv FILES ####
read_maxeff=open(maxeff_filepath[0],'r')
mot_data_header= read_maxeff.readline()
mot_data_variables=str.strip(mot_data_header).split(",")


#### DETERMINE THE NUMBER OF LINES IN THE Max_tot_Eff.csv FILES ####
number_vx_vz_combi=0
while(line != ''):
	line = read_maxeff.readline()
	number_vx_vz_combi=number_vx_vz_combi+1
number_vx_vz_combi=number_vx_vz_combi-1


#### ALLOCATE MATRIX ####
mot_data_M=np.zeros((number_mot_data,number_vx_vz_combi,len(mot_data_variables)))
redsink_M=np.zeros((number_mot_data,5,len(mot_data_variables)))
read_maxeff.close()

#### LOOP OVER ALL Max_tot_Eff.csv FILES AND STORING IN NUMPY ARRAY ####
for i in range (0,number_mot_data):
	read_maxeff=open(maxeff_filepath[i],'r')
	dummy=read_maxeff.readline()
	for j in range (0,number_vx_vz_combi):
		line=read_maxeff.readline()
		#print(len(mot_data_variables),i,j,line)
		#print(len(str.strip(line).split(",")))
		#print(line)
		mot_data_M[i][j][:]=str.strip(line).split(",")[:]
	

#### LOOP OVER ALL Reduced_Sink.csv FILES AND STORING IN NUMPY ARRAY ####
for i in range (0,number_mot_data):
	read_redsink=open(redsink_filepath[i],'r')
	dummy=read_redsink.readline()
	for j in range (0,5):
		redsink_M[i][j][:]=str.strip(read_redsink.readline()).split(",")[:]


#### DETERMINE THE NUMBER OF DIFFERENT CLIMB RATES ####
CR.append(mot_data_M[0][0][2])
for i in range(1,number_vx_vz_combi):
	if(mot_data_M[0][i][2] not in CR):
		CR.append(mot_data_M[0][i][2])


#### REDUCE MOTOR DATA TO BEST EFFICIENCY POINTS FOR ALL ALTITUDES AND CLIMBRATES ####
for i in range (0,number_mot_data):
	mot_data_M_BestEff_dummy=[]
	for j in range (0,len(CR)):
		mot_data_M_BestEff_dummy.append(GET_BEST_EFF(mot_data_M[i],CR[j],number_vx_vz_combi))
	mot_data_M_BestEff.append(mot_data_M_BestEff_dummy)
	del(mot_data_M_BestEff_dummy)



####################### READ POLAR DIAGRAM ##############################################
read_pol_diag=open(polardiag_filepath[0],'r')

#### DETERMINE THE NUMBER OF LINES IN THE POLAR DIAGRAM FILES ####
pol_diag_alphas=0
line="dummy"
while(line != 'alpha'):
	line = str.split(str.strip(read_pol_diag.readline())," ")[0]
while(line != ''):
	line = str.split(str.strip(read_pol_diag.readline())," ")[0]
	pol_diag_alphas=pol_diag_alphas+1
pol_diag_alphas=pol_diag_alphas-1


#### ALLOCATE MATRIX ####
pol_diag_alpha=np.zeros((len(polardiag_filepath),pol_diag_alphas))
pol_diag_CL=np.zeros((len(polardiag_filepath),pol_diag_alphas))
pol_diag_CD=np.zeros((len(polardiag_filepath),pol_diag_alphas))
pol_diag_QInf=np.zeros((len(polardiag_filepath),pol_diag_alphas))
pol_diag_v_sink=np.zeros((len(polardiag_filepath),pol_diag_alphas))
pol_diag_v_sink_min=np.zeros((len(polardiag_filepath)))
pol_diag_QInf_at_v_sink_min=np.zeros((len(polardiag_filepath)))
pol_diag_CD_at_v_sink_min=np.zeros((len(polardiag_filepath)))

read_pol_diag.close()


#### LOOP OVER ALL POLAR DIAGRAM FILES FILES AND STORING IN NUMPY ARRAY ####
for i in range (0,number_polars):
	read_pol_diag=open(polardiag_filepath[i],'r')
	line=""
	while(line != 'alpha'):
		line = str.split(str.strip(read_pol_diag.readline())," ")[0]
	for j in range (0,pol_diag_alphas):
		line=str.strip(read_pol_diag.readline())
		#print(float(str.split(line)[0]))
		pol_diag_alpha[i][j]=float(str.split(line)[0])
		pol_diag_CL[i][j]=float(str.split(line)[2])
		pol_diag_CD[i][j]=float(str.split(line)[5])
		pol_diag_QInf[i][j]=float(str.split(line)[11])

for i in range(0,number_polars):
	for j in range(0,pol_diag_alphas):
		pol_diag_v_sink[i][j]=pol_diag_QInf[i][j]/pol_diag_CL[i][j]*pol_diag_CD[i][j]

for i in range(0,number_polars):
	pol_diag_v_sink_min[i]=min(pol_diag_v_sink[i])
	pol_diag_QInf_at_v_sink_min[i]=pol_diag_QInf[i][np.argmin(pol_diag_v_sink[i])]
	pol_diag_CD_at_v_sink_min[i]=pol_diag_CD[i][np.argmin(pol_diag_v_sink[i])]


total_Energy_spent=0.0
delta_Solar_Energy=0.0
total_Solar_Energy=0.0
remaining_Bat_Energy=bat_cap_status_start

### PRINT INFO ABOUT SOLAR CELLS ON AIRCRAFT ##############
SPS=GET_SUN_PROJ_SURF([0,0,1],0.)
sol_cell_orientations=SPS[9]
print("Number of different Solar Cell Orientations on Aircraft:",sol_cell_orientations)
print("Number of Solar Cells in Total:",SPS[3])
num_sol_cell = SPS[3]

total_sol_surf_area = SPS[4]
print("Total Solar Cell Surface Area [sqm]:",total_sol_surf_area)
sol_area_horiz = SPS[5]
print("Horizontally Projected Solar Cell Area for Aircraft-AOA = 0 deg [sqm]:",sol_area_horiz)

### CREATE ARRAYS FOR TEMPERATURE AND EFFICIENCY THAT CONTAINS AS MANY ELEMENTS AS DIFFERENT SOLCEL ORIENTATIONS EXIST. TEMPERATURE/EFFICIENCY VALUES WILL BE STORED IN THIS ARRAY LATER ########################
solcel_temps_opt=np.zeros(sol_cell_orientations)
solcel_temps_min=np.zeros(sol_cell_orientations)
solcel_temps_avg=np.zeros(sol_cell_orientations)
solcel_efficiencies_opt=np.zeros(sol_cell_orientations)
solcel_efficiencies_min=np.zeros(sol_cell_orientations)
solcel_efficiencies_avg=np.zeros(sol_cell_orientations)
solcel_powers=np.zeros(sol_cell_orientations)
solcel_surf_transmission=np.zeros(sol_cell_orientations)



print("\n########## ALL DATA READ! ###################")


######################################################################################
######################################################################################
######################################################################################
######################################################################################
######################################################################################

ALT1LINDEX =[]
ALT1UINDEX =[]
TAS1L      =[]
TAS1U      =[]
ALT2LINDEX =[]
ALT2UINDEX =[]
TAS2L      =[]
TAS2U      =[]


#### FIND THE INDICES OF THE MOTORIZED DATA FOR BOTH MISSION POINTS #######################################
for i in range(0,len(altitude1)):
	if(altitude1[i] == 0.):
		ALT1LINDEX.append(0)
		ALT1UINDEX.append(0)
	elif(altitude1[i] > mot_data_M[number_mot_data-1][0][0]):
		print("#### ERROR: MISSION ALTITUDE VIOLATES MOTORIZED DATA RANGE ####")
		exit()
	else:	
		for j in range(1,number_mot_data):
			if( altitude1[i] < mot_data_M[j][0][0] and  altitude1[i] > mot_data_M[j-1][0][0]):
				ALT1LINDEX.append(j-1)
				ALT1UINDEX.append(j)
			if(altitude1[i] == mot_data_M[j][0][0]):
				ALT1LINDEX.append(j)
				ALT1UINDEX.append(j)

for i in range(0,len(altitude2)):
	if(altitude2[i] > mot_data_M[number_mot_data-1][0][0]):
		print("#### ERROR: MISSION ALTITUDE VIOLATES MOTORIZED DATA RANGE ####")
		exit()
	if(altitude2[i] == 0.):
		ALT2LINDEX.append(0)
		ALT2UINDEX.append(0)	
	
	for j in range(1,number_mot_data):
		if( altitude2[i] < mot_data_M[j][0][0] and  altitude2[i] > mot_data_M[j-1][0][0]):
			ALT2LINDEX.append(j-1)
			ALT2UINDEX.append(j)
		if( altitude2[i] == mot_data_M[j][0][0]):
			ALT2LINDEX.append(j)
			ALT2UINDEX.append(j)



#### CALCULATE TAS AT THE MOTORIZED-DATA-POINTS ####################
for i in range(0,len(altitude1)):
	TAS1L.append(tas1[i] * SCALE_TAS(tempprofile_flag,altitude1[i],mot_data_M[ALT1LINDEX[i]][0][0],atmo_temp_altitude,atmo_temp_temp))
	TAS1U.append(tas1[i] * SCALE_TAS(tempprofile_flag,altitude1[i],mot_data_M[ALT1UINDEX[i]][0][0],atmo_temp_altitude,atmo_temp_temp))
	TAS2L.append(tas2[i] * SCALE_TAS(tempprofile_flag,altitude2[i],mot_data_M[ALT2LINDEX[i]][0][0],atmo_temp_altitude,atmo_temp_temp))
	TAS2U.append(tas2[i] * SCALE_TAS(tempprofile_flag,altitude2[i],mot_data_M[ALT2UINDEX[i]][0][0],atmo_temp_altitude,atmo_temp_temp))

datapath1L=[]
datapath1U=[]
datapath2L=[]
datapath2U=[]

csv_datapath1L=[]
csv_datapath1U=[]
csv_datapath2L=[]
csv_datapath2U=[]

#### CALCULATE MOT_DAT1 and MOT_DAT2 AT MISSION POINT ALTITUDES BY MEANS OF INTERPOLATION FROM ORIGINAL ALTITUDE GRID ################
for i in range(0,len(altitude1)):
	datapath1L.append(str(main_path_mot_dat)+str("/")+str(subdirectories[ALT1LINDEX[i]]))
	datapath1U.append(str(main_path_mot_dat)+str("/")+str(subdirectories[ALT1UINDEX[i]]))
	datapath2L.append(str(main_path_mot_dat)+str("/")+str(subdirectories[ALT2LINDEX[i]]))
	datapath2U.append(str(main_path_mot_dat)+str("/")+str(subdirectories[ALT2UINDEX[i]]))

for i in range(0,len(altitude1)):
	csv_datapath1L.append(GET_CSVDATAPATH(datapath1L[i]))
	csv_datapath1U.append(GET_CSVDATAPATH(datapath1U[i]))
	csv_datapath2L.append(GET_CSVDATAPATH(datapath2L[i]))
	csv_datapath2U.append(GET_CSVDATAPATH(datapath2U[i]))



#### PARSE THE NUMBER OF CLIMBRATES AND THE NUMBER OF VCARIABLES IN THE CVS FILES ####################################################
f_dummy=open(csv_datapath1L[0][0])
line='re'
CR_check=[]
line = f_dummy.readline()
headline=line.strip()
number_variables= len(line.strip().split(','))
while(line !=''):
	line = f_dummy.readline()
	if line !='':
		CR_check.append(abs(float(line.strip().split(',')[2])))
number_climbrates = int(len(CR_check) / Counter(CR_check)[0])

#### ALLOCATE THE MOT_DAT FILES #####################################################################################################
MOT_DAT1=np.zeros((len(altitude1),number_climbrates,number_variables))
MOT_DAT2=np.zeros((len(altitude1),number_climbrates,number_variables))


if(calc_type == "RECALC"):
	max_loop_counter = len(altitude1)
else:
	max_loop_counter = 0

for count in range(0,max_loop_counter):
	#### INTERPOLATE BETWEEN VELOCITIES ON THE SAME ALTITUDE ##########################################	
	LOWERALT1=INTERPOL_CSV_FILES_VELO(csv_datapath1L[count],TAS1L[count],dbeta1[count],count,mission_phase_type[count])
	UPPERALT1=INTERPOL_CSV_FILES_VELO(csv_datapath1U[count],TAS1U[count],dbeta1[count],count,mission_phase_type[count])
	LOWERALT2=INTERPOL_CSV_FILES_VELO(csv_datapath2L[count],TAS2L[count],dbeta2[count],count,mission_phase_type[count])
	UPPERALT2=INTERPOL_CSV_FILES_VELO(csv_datapath2U[count],TAS2U[count],dbeta2[count],count,mission_phase_type[count])

	#print(np.shape(LOWERALT1)[0],np.shape(UPPERALT1)[0],np.shape(LOWERALT2)[0],np.shape(UPPERALT2)[0])
	num_CR1 = np.shape(LOWERALT1)[0]
	num_CR1 = np.shape(UPPERALT1)[0]
	num_CR1 = np.shape(LOWERALT2)[0]
	num_CR1 = np.shape(UPPERALT2)[0]
	
	if (num_CR1 == num_CR2 and num_CR2 == num_CR3 and num_CR3 == num_CR4):
		print("##### Number of Climbrates of all files consistent #####")
	else:
		print("#########################################################################################################################################")
		print("##### Number of climbrates in Motorized Files differ. Please make sure that forch each altitude, the same climbrates are calculated! ####")
		print("#########################################################################################################################################")
		exit()
	
	
	#### CALCULATE THE ALTITUDE INTERPOLATION FACTORS ########################
	if(UPPERALT1[0][0] == LOWERALT1[0][0]):
		altfac_lower1=1
		altfac_upper1=0

	else:
		altfac_upper1= (altitude1[count] - LOWERALT1[0][0]) / (UPPERALT1[0][0]-LOWERALT1[0][0])
		altfac_lower1= (UPPERALT1[0][0] - altitude1[count]) / (UPPERALT1[0][0]-LOWERALT1[0][0])
		
	#### CALCULATE THE ALTITUDE INTERPOLATION FACTORS ########################
	if(UPPERALT2[0][0] == LOWERALT2[0][0]):
		altfac_lower2=1
		altfac_upper2=0

	else:
		altfac_upper2= (altitude2[count] - LOWERALT2[0][0]) / (UPPERALT2[0][0]-LOWERALT2[0][0])
		altfac_lower2= (UPPERALT2[0][0] - altitude2[count]) / (UPPERALT2[0][0]-LOWERALT2[0][0])

	for i in range(0,np.shape(LOWERALT1)[0]):
		for j in range(0,np.shape(LOWERALT1)[1]):
			MOT_DAT1[count][i][j] = UPPERALT1[i][j]*altfac_upper1 + LOWERALT1[i][j]*altfac_lower1
	for i in range(0,np.shape(LOWERALT2)[0]):
		for j in range(0,np.shape(LOWERALT2)[1]):
			MOT_DAT2[count][i][j] = UPPERALT2[i][j]*altfac_upper2 + LOWERALT2[i][j]*altfac_lower2
			


######################################################################################
######################################################################################
######################################################################################
######################################################################################
######################################################################################
######################################################################################


if bat_cap < bat_cap_status_start:
	print("###########################################################################################")
	print("###########   CAUTION: BATTERY CAPACITY IS SMALLER THAN INITIAL BATTERY CONTENT  ##########")
	print("###########################################################################################")


###############################################################################################################
##################################### MISSION CALCULATION #####################################################
###############################################################################################################

mission_time=0.
number_mission_phases=len(mission_phase_type)
mission_point_count=0
mission_data_distance_east.append(0.0)
mission_data_distance_north.append(0.0)

header_temps=str("Time[h],Altitude[m],AtmoTemp[deg C],Max.CellTemp[degC],Avg.CellTemp[degC],Min.CellTemp[degC]")
header_effi=str("Time[h],Altitude[m],AtmoTemp[deg C],Max.CellEffi[%],Avg.CellEffi[%],Min.CellEffi[%]")
header_powers=str("Time[h],Altitude[m],AtmoTemp[degC],BestHeading[deg],SolarPower BestHeading[W], Average SolarPower[W], WorstHeading[deg], SolarPower at WorstHeading[W]")

header_temp_SPAVL = str("Heading[deg]")
header_effi_SPAVL = str("Heading[deg]")
header_power_SPAVL = str("Heading[deg]")
header_surf_transmission = str("Heading[deg]")
header_mppt_cutoff = str("Time[h]")

### CREATE HEADERS FOR OUTPUTFILES CONTAINING THE TIME AND THE TEMPERATURES/EFFICIENCIES FOR ALL SOLAR CELL GROUPS SEPARATELY ###################
for i in range(0,sol_cell_orientations):
	header_temps=header_temps + str(",TempBestHead_SCG_")+str(i+1)+str("[deg C]")
	header_effi=header_effi + str(",EfficBestHead_SCG_")+str(i+1)+str(" [%]")
	header_powers=header_powers + str(",PowerBestHead_SCG_")+str(i+1)+str(" [W]")
for i in range(0,sol_cell_orientations):	
	header_temps=header_temps + str(",TempAvgHead_SCG_")+str(i+1)+str("[deg C]")
	header_effi=header_effi + str(",EfficAvgHead_SCG_")+str(i+1)+str(" [%]")
	header_powers=header_powers + str(",PowerAvgHead_SCG_")+str(i+1)+str(" [W]")
for i in range(0,sol_cell_orientations):
	header_mppt_cutoff=header_mppt_cutoff + str(",MPPT_cutoff_")+str(i+1)
	header_surf_transmission=header_surf_transmission + str(",Surf_Transm_SCG_")+str(i+1)+str(" [%]")
	
for i in range(0,sol_cell_orientations):
	header_temp_SPAVL = header_temp_SPAVL + str(",Temp_SCG_")+str(i+1)+str("[deg C]")
	
for i in range(0,sol_cell_orientations):
	header_effi_SPAVL = header_effi_SPAVL + str(",Effip_SCG_")+str(i+1)+str("[%]")

for i in range(0,sol_cell_orientations):
	header_power_SPAVL = header_power_SPAVL + str(",Power_SCG_")+str(i+1)+str("[W]")
header_power_SPAVL = header_power_SPAVL + str(",Total Solar Power [W]")


### DECIDE WHETHER FILES ARE CREATED OR NOT ################################
if writedata == 1:
	write_cells_temp=open("sol_cells_temp.csv",'w')
	write_cells_temp.write(header_temps)
	write_cells_effi=open("sol_cells_effi.csv",'w')
	write_cells_effi.write(header_effi)
	write_cells_powers=open("sol_cells_powers.csv",'w')
	write_cells_powers.write(header_powers)	

	write_cells_temp_SPAVL=open("sol_cells_temp_spavl.csv",'w')
	write_cells_temp_SPAVL.write(header_temp_SPAVL)
	write_cells_effi_SPAVL=open("sol_cells_effi_spavl.csv",'w')
	write_cells_effi_SPAVL.write(header_effi_SPAVL)
	write_cells_powers_SPAVL=open("sol_cells_powers_spavl.csv",'w')
	write_cells_powers_SPAVL.write(header_power_SPAVL)

	write_cells_surf_transmission=open("sol_cells_surf_transmission.csv",'w')
	write_cells_surf_transmission.write(header_surf_transmission)	
	write_cells_mppt_cutoff=open("sol_cells_mppt_cutoff.csv",'w')
	write_cells_mppt_cutoff.write(header_mppt_cutoff)
		
act_altitude=altitude1[0]

altitude_error_sum=0.0
excess_solar_energy=0.0



for i in range (0,number_mission_phases):
	
	###########################################################################################################################################################
	###########################################################################################################################################################
	###                                                                                                                                                     ###
	###                                                      CLIMB PHASE                                                                                    ###
	###                                                                                                                                                     ###
	###########################################################################################################################################################
	###########################################################################################################################################################
	
	if(mission_phase_type[i] == 'CL_A1_CR1_A2_CR2'): 
		
		if  float(altitude1[i]) >  float(altitude2[i]):
			print("\n##### TARGET ALTITUDE IS LOWER THAN STARTING ALTITUDE FOR CLIMB PHASE # ",i,". --> CALCULATION ABORTED #####\n") 
			exit()
		
		act_climbrate=climbrate1[i]
		
		if(calc_type == "RECALC"):
			print("\n\n################## MISSION PHASE #",i+1, "- CLIMB ##################\n     DEFINED ALTITUDE AT START: ", float(altitude1[i]),"m, DEFINED ALTITUDE AT END: ", float(altitude2[i])," m\n     START CLIMBRATE: ",act_climbrate,"m/s, END CLIMBRATE: ",climbrate2[i]," m/s\n     START TAS: ",tas1[i],"m/s, END TAS: ",tas2[i]," m/s\n     START DBETA: ",dbeta1[i],"Deg, END DBETA: ",dbeta2[i]," Deg\n     TIME AT START OF CLIMB PHASE: =",takeoff_time+mission_time/3600.," hrs")
		else:
			print("\n\n################## MISSION PHASE #",i+1, "- CLIMB ##################\n     DEFINED ALTITUDE AT START: ", float(altitude1[i]),"m, DEFINED ALTITUDE AT END: ", float(altitude2[i])," m\n     START CLIMBRATE: ",act_climbrate,"m/s, END CLIMBRATE: ",climbrate2[i]," m/s\n     TIME AT START OF CLIMB PHASE: =",takeoff_time+mission_time/3600.," hrs")
	
		
		if (abs(act_altitude - altitude1[i]) < altitude_mismatch_limit):
			print("     ACTUAL ALTITUDE AT START: ",act_altitude,"m. --> ALTITUDE ERROR: ",act_altitude - altitude1[i],"m")
		else:
			print("\n\n      ##### CALCULATION ABORED DUE TO EXCESSIVE ALTITUDE MISMATCH BETWEEN CONSECUTIVE PHASES ########\nACTUAL ALTITUDE OF PRECEEDING PHASE: ",act_altitude,"m, DEFINED ALTITUDE FOR CURRENT PHASE: ",altitude1[i]," m. --> ALTITUDE ERROR: ",act_altitude - altitude1[i],"m")
			exit()
		altitude_error_sum=altitude_error_sum+(act_altitude - altitude1[i])
		check=1
		while(check==1): # turns to 0 if final altitude is reached
			
			print("     CURRENT ALTITUDE: %1.0fm - %1.0f%%" % (act_altitude,100*(act_altitude-altitude1[i])/(-altitude1[i]+altitude2[i])),end='\r')
			sys.stdout.flush()
			
			####### STORING MISSION VARIABLES ###################################################################
			mission_data_time.append(mission_time)
			curr_time_h=takeoff_time+mission_time/3600.
			mission_data_real_time_h.append(curr_time_h)
			mission_data_altitude.append(act_altitude)
			mission_data_Potential_Energy.append((act_altitude-altitude1[0])*aircraft_mass*9.81/3600.)		
			
			
			if(calc_type == "OPT"):
				mot_data_M_act_altitude = INTERPOL_ALT_CLIMBRATE(mot_data_M_BestEff,act_altitude,act_climbrate)	
			else:
				mot_data_M_act_altitude = INTERPOL_MOT_DAT_OVER_ALT(MOT_DAT1[i],MOT_DAT2[i],act_altitude,act_climbrate)
				

			
			velocity=float(mot_data_M_act_altitude[1])
			RE = GET_REYNOLDS(act_altitude,velocity,ReynoldsReferenceLength)
			mission_data_reynolds.append(RE)
			mission_data_mach.append(GET_MACH(act_altitude,velocity))			
			mission_data_velocity.append(velocity)		
			mission_data_climbrate.append(act_climbrate)
			mission_data_DBeta.append(float(round(mot_data_M_act_altitude[3],2)))
			Prop_Eff_TMP=float(mot_data_M_act_altitude[4])
			mission_data_Efficiency_prop.append(Prop_Eff_TMP)		
			mission_data_RPM.append(float(mot_data_M_act_altitude[7]))
			mission_data_Pprop.append(float(mot_data_M_act_altitude[8]))
			mission_data_Pshaft.append(float(mot_data_M_act_altitude[9]))
			mission_data_Thrust.append(float(mot_data_M_act_altitude[11]))
			mission_data_Q.append(float(mot_data_M_act_altitude[12]))

			if tempprofile_flag == 1:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getStandardTemperature(getGeopotential(act_altitude))
			else:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			mission_data_density.append(density)
			CL_tmp=2*aircraft_mass*9.81/density/velocity/velocity/AircraftReferenceArea
			mission_data_CL.append(CL_tmp)
			mission_data_alfa.append(np.interp(CL_tmp, pol_diag_CL[0], pol_diag_alpha[0]))			
			mission_data_Mot_Current.append(float(mot_data_M_act_altitude[13]))
			mission_data_maxcurr.append(max_current)
			mission_data_peakcurr.append(peak_current)
			mission_data_PL_MC.append(float(mot_data_M_act_altitude[16]))
			mission_data_PL_PROP.append(float(mot_data_M_act_altitude[17]))
			mission_data_PL_MOT_COPPER.append(float(mot_data_M_act_altitude[18]))
			mission_data_PL_MOT_BE_FE.append(float(mot_data_M_act_altitude[19]))
			mission_data_PL_MOT_TOT.append(float(mot_data_M_act_altitude[20]))
			
			mission_data_PL_MOT_TOT_MEAS.append(float(mot_data_M_act_altitude[21]))	
			mission_data_CURR_CONST_MEAS.append(float(mot_data_M_act_altitude[22]))			
	
	
						
			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################
			#############################            NEW SOLARCALC ROUTINE                      ##########################################
			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################

			############################################### Solar Energy Generation #################################################
			solPowDens=getSolarPowerDensity(day_of_year)
			OUT_GSV=GET_SUNVECTOR(day_of_year,curr_time_h+sunculm_delta_time_h,latitude)
			sun_vector=[OUT_GSV[5],OUT_GSV[6],OUT_GSV[7]]
					
			############# STORING RESULTS IN VARIABLES FOR OUTPUT IN CSV FILE #######################################################
			mission_data_incangle.append(OUT_GSV[0])
			mission_data_hour_angle.append(OUT_GSV[1])
			mission_data_declination_angle.append(OUT_GSV[2])
			mission_data_zenith_angle.append(OUT_GSV[3])
			mission_data_azimuth_angle.append(OUT_GSV[4])
			mission_data_sunvector_x.append(OUT_GSV[5])
			mission_data_sunvector_y.append(OUT_GSV[6])
			mission_data_sunvector_z.append(OUT_GSV[7])

			############# Additional Losses due to MPPT and ohm-losses in cables (1.5mmsq, at 7A peak current, 8m cable length).
			addLosses=mppt_eff/100.*(1.-cable_loss/100.)	
			
			############# CALCULATE ATMOSPHERIC TEMPERATURE ##########################################
			if tempprofile_flag == 1:
				temp_atmo = getStandardTemperature(getGeopotential(act_altitude))
			else:
				temp_atmo = getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			############# CSV OUTPUT ROUTINES ######################################################
			write_cells_temp.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_effi.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_powers.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
								
								


			############# CALCULATE ATMOSPHERIC TRANSMISSION AND  SOLAR CELL ORIENTATIONS ################################################
			if (act_altitude > -np.abs(getDepression(act_altitude))):
				atmoTransmiss = getAtmoTransmission(act_altitude,mission_data_incangle[mission_point_count],getDepression(act_altitude))
				SPS=GET_SUN_PROJ_SURF(sun_vector,mission_data_alfa[mission_point_count])
				solcell_normals=SPS[10]
				solcell_count=SPS[12]
				Reflength=SPS[17]
			else:
				atmoTransmiss = 0.0
				SPS = 0.0
				Reflength=0.0

			mission_data_atmo_transmission.append(atmoTransmiss)
			
			s=(int(360/5),sol_cell_orientations)
			CELL_GROUP_TEMP  = np.zeros(s)
			CELL_GROUP_EFF   = np.zeros(s)
			CELL_GROUP_POW   = np.zeros(s)
			
			
			#### DO THIS ONLY IF SUNLIGHT IS AVAILABLE #######
			if atmoTransmiss > 0:
				########### CALCULATE THE TEMPERATURE, THE EFFICIENCY AND THE SOLAR POWER FOR ALL ORIENTATIONS IN 5DEG-INCREMENTS OVER 360 DEGREES
				for count_i in range(0,360,5):
					for j in range(0,sol_cell_orientations):
						lamturb=SPS[15]
						rotated_solcel_normals = np.dot(rotation_matrix(np.array([0,0,-1]),-count_i), sum(solcell_normals[j]))	
						CosPsi = np.dot(sun_vector,rotated_solcel_normals/-np.linalg.norm(rotated_solcel_normals))
						if CosPsi >=0:
							CosPsi=CosPsi
						else:
							CosPsi=0.
						solcel_surf_transmission = cell_surf_transmission(angle_between(sun_vector,-rotated_solcel_normals))
						
						solcel_Temp = getSolCellTempEnerBal(solcel_limtemp_alt,solcel_limtemp_mintemp,solcel_limtemp_maxtemp,solcell_nom_eff,eff_grad,lamturb[j],CosPsi,temp_atmo, RE, act_altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,Reflength[j])
						CELL_GROUP_TEMP[int(count_i/5),j] = solcel_Temp-273.15
						solcel_effi = getEfficiencySolarcells(solcel_Temp,solcell_nom_eff,eff_grad)/100.
						CELL_GROUP_EFF[int(count_i/5),j] = solcel_effi
						sol_pow_inc = np.linalg.norm(rotated_solcel_normals)*CosPsi*solPowDens*atmoTransmiss*solcel_surf_transmission*addLosses*solcel_effi
						cutoff_power_per_direction = solcell_count[j] * mppt_cutoff_power_per_cell
						
						### CHECK IF THE SOLAR CELLS ON EACH STRING DELIVER ENOUGH POWER TO BE ABOVE THE MPPT THREASHOLD ##########
						if sol_pow_inc > cutoff_power_per_direction:
							CELL_GROUP_POW[int(count_i/5)][j] = sol_pow_inc
						else:
							CELL_GROUP_POW[int(count_i/5)][j] = 0.0
			else:
				CELL_GROUP_TEMP[:,:] =	temp_atmo-273.15
				CELL_GROUP_EFF[:,:] = getEfficiencySolarcells(temp_atmo,solcell_nom_eff,eff_grad)/100.
				CELL_GROUP_POW[:,:] = 0.0
				best_head = 0.0	

			#### ACCUMULATE OVER 360DEG ####################################
			solpow_total=[]
			for count_i in range(0,360,5):
				sum_solpow = 0.0
				for j in range(0,sol_cell_orientations):	
					sum_solpow = sum_solpow + CELL_GROUP_POW[int(count_i/5)][j]
				solpow_total.append(sum_solpow)
			
			#### MAXIMUM SOLAR POWER CONDITION ###################################	
			best_head = 5*np.argmax(solpow_total)
			best_solpow = np.max(solpow_total)
			
			#### AVERAGE SOLAR POWER CONDITION ###################################
			average_solpow = np.average(solpow_total)
			
			#### MINIMUM SOLAR POWER CONDITION ###################################
			min_solpow = np.min(solpow_total)
			min_head = 5*np.argmin(solpow_total)
			
			#### WRITE TO FILE THE BEST HEADING, THE BEST SOLAR POWER, THE AVERAGE SOLAR POWER, THE MINIMUM-SOL-POW HEADING AND THE MINIMUM SOLAR POWER ##################
			write_cells_powers.write(str(best_head)+","+str(best_solpow)+","+str(average_solpow)+","+str(min_head)+","+str(min_solpow)+",")
			
			#### CALCULATE EXTREMA FOR TEMPERATURES AND EFFICIENCIES OVER ALL ORIENTATIONS AND ALL CELL GROUPS TOGETHER ############################
			MinCelTemp = np.amin(CELL_GROUP_TEMP)
			MaxCelTemp = np.amax(CELL_GROUP_TEMP)
			AvgCelTemp = np.average(CELL_GROUP_TEMP)
			MinCelEffi = np.amin(CELL_GROUP_EFF)
			MaxCelEffi = np.amax(CELL_GROUP_EFF)
			AvgCelEffi = np.average(CELL_GROUP_EFF)
			
			mission_data_cell_temp_individual.append(CELL_GROUP_TEMP[np.argmax(solpow_total)][:])
			mission_data_cell_efficiency_individual.append(CELL_GROUP_EFF[np.argmax(solpow_total)][:])
			
			
			#### WRITE EXTREMA/AVERAGE TEMP AND EFFI TO FILE ########################################################
			write_cells_temp.write(str(MaxCelTemp)+","+str(AvgCelTemp)+","+str(MinCelTemp)+",")
			write_cells_effi.write(str(MaxCelEffi)+","+str(AvgCelEffi)+","+str(MinCelEffi)+",")
			
			#### WRITE SOLPOW/TEMP/EFFI DATA FOR BEST HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(CELL_GROUP_TEMP[int(best_head/5),j])+",")
				write_cells_effi.write(str(CELL_GROUP_EFF[int(best_head/5),j])+",")
				write_cells_powers.write(str(CELL_GROUP_POW[int(best_head/5),j])+",")

			#### WRITE SOLPOW/TEMP/EFFI DATA FOR AVERAGE HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(np.average(CELL_GROUP_TEMP[:,j]))+",")
				write_cells_effi.write(str(np.average(CELL_GROUP_EFF[:,j]))+",")
				write_cells_powers.write(str(np.average(CELL_GROUP_POW[:,j]))+",")
			
			mission_data_cell_temp_min.append(MinCelTemp)
			mission_data_cell_temp_max.append(MaxCelTemp)
			mission_data_cell_temp_avg.append(AvgCelTemp)
			mission_data_cell_efficiency_min.append(MinCelEffi)
			mission_data_cell_efficiency_max.append(MaxCelEffi)
			mission_data_cell_efficiency_avg.append(AvgCelEffi)

			del solpow_total			
			del CELL_GROUP_TEMP
			del CELL_GROUP_EFF
			del CELL_GROUP_POW

			mission_data_sol_pow_max.append(best_solpow)
			mission_data_sol_pow_avg.append(average_solpow)
			
			############# PATH CALCULATIONS ##########################################
			mission_data_best_heading.append(best_head)
			delta_east  = np.sin(best_head*np.pi/180.)*timestep*velocity/1000.
			delta_north = np.cos(best_head*np.pi/180.)*timestep*velocity/1000.
			mission_data_distance_east.append(mission_data_distance_east[mission_point_count]+delta_east)
			mission_data_distance_north.append(mission_data_distance_north[mission_point_count]+delta_north)
			
			if heading_strategy == 1:
				mission_data_sol_pow.append(best_solpow)
			if heading_strategy == 2:
				mission_data_sol_pow.append(average_solpow)
			if heading_strategy < 1 or heading_strategy > 2:
				print("################ NON EXISTANT HEADING STRATEGY SPECIFIED ###############")
				exit()

			##### Current Increment of Solar Energy #########################	
			if(mission_point_count>0):
				delta_Solar_Energy=(mission_data_sol_pow[mission_point_count-1]+mission_data_sol_pow[mission_point_count])/2.0*timestep/3600.
			else:
				delta_Solar_Energy=0.0
				
			##### Total Accumulated Solar Energy ############################
			total_Solar_Energy=total_Solar_Energy+delta_Solar_Energy
			
			##################### CALCULATE THE BATTERY POWER AND BATTERY CURRENT #######################################################
			P_Bat_ex = mission_data_Pshaft[mission_point_count] +  mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count] - mission_data_sol_pow[mission_point_count] + sys_pwr_drn
			mission_data_PBat_ex.append(P_Bat_ex)
			
			if mission_point_count == 0:
				current_capacity=bat_cap_status_start
			else:
				current_capacity= mission_data_Energy_Remaining[mission_point_count-1]
			
			#### BATTERY IS BEING DISCHARGED, CALCULATE LOSSES OF BATTERY #############################################
			if P_Bat_ex > 0: 
				if bat_type == "14S_20P_3500_LiIo":
					battery_voltage_current = GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(P_Bat_ex,current_capacity,number_of_batteries,single_bat_cap)
				if bat_type == "52V":
					battery_voltage_current = GET_BAT_VOLTAGE_CURRENT_52V(P_Bat_ex,current_capacity,number_of_batteries,single_bat_cap)
				
				Bat_Volt = battery_voltage_current[0]
				Bat_Curr = battery_voltage_current[1]
				PL_Bat = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				PBat_int = P_Bat_ex + (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				
				### No Charging Losses Apply ####
				P_chargloss_chem = 0.
				P_chargloss_ohm = 0.
				batt_charging_power = 0.0
				batt_charging_current = 0.0
			
			#### NO ENREGY IS FLOWING IN OR OUT OF THE BATTERY, LOSSES ARE ZERO ######################################
			if P_Bat_ex == 0.: 
				if bat_type == "14S_20P_3500_LiIo":
					Bat_Volt = GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(0.,mission_data_Energy_Remaining[mission_point_count-1],number_of_batteries,single_bat_cap)[0]
				if bat_type == "52V":
					Bat_Volt = GET_BAT_VOLTAGE_CURRENT_52V(0.,mission_data_Energy_Remaining[mission_point_count-1],number_of_batteries,single_bat_cap)[0]
				
				Bat_Curr = 0.
				PL_Bat = 0.0
				PBat_int = 0.0
				
				### No Charging Losses Apply ####
				P_chargloss_chem = 0.
				P_chargloss_ohm = 0.
				batt_charging_power = 0.0
				batt_charging_current = 0.0

			#### BATTERY IS BEING CHARGED, CALCULATE LOSSES OF BATTERY #############################################
			if P_Bat_ex < 0: 
				Bat_Volt = 42.0 + 15.4*current_capacity/(number_of_batteries*single_bat_cap)
				Bat_Curr = P_Bat_ex / Bat_Volt
				PL_Bat = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				PBat_int = P_Bat_ex - (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				
				### Charging Losses Apply ####
				P_chargloss_chem = ((-P_Bat_ex + (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries) * (bat_chg_red_chem/100.))
				P_chargloss_ohm = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				batt_charging_power = -P_Bat_ex
				batt_charging_current = -Bat_Curr				
			
			mission_data_PL_Bat.append(PL_Bat)
			mission_data_PBat_int.append(PBat_int)			
			mission_data_Bat_Current.append(Bat_Curr)
			mission_data_Bat_Voltage.append(Bat_Volt)			
			mission_data_PL_BATT_CHARGE_CHEM.append(P_chargloss_chem)
			mission_data_PL_BATT_CHARGE_OHM.append(P_chargloss_ohm)		
			mission_data_batt_charging_power.append(batt_charging_power)
			mission_data_batt_charging_current.append(batt_charging_current)					
			mission_data_Ptot.append(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count] + mission_data_PL_MC[mission_point_count])
	

			############## START INTEGRATE ##########################
			### Energy Consumption ###
			if(mission_point_count>0):
				delta_Energy_inc=(mission_data_Ptot[mission_point_count] + sys_pwr_drn + P_chargloss_chem + P_chargloss_ohm + mission_data_PL_Bat[mission_point_count])*timestep/3600. 
			else:
				delta_Energy_inc=0.
			
			total_Energy_spent=total_Energy_spent+delta_Energy_inc
			mission_data_Energy_Spent.append(total_Energy_spent)			
			
			
			### Overall Energy Status ###
			remaining_Bat_Energy = remaining_Bat_Energy - delta_Energy_inc + delta_Solar_Energy
			
			if remaining_Bat_Energy > bat_cap:
				mission_data_excess_solar_power.append((remaining_Bat_Energy-bat_cap)/timestep*3600.)
				remaining_Bat_Energy = bat_cap
				mission_data_PL_BATT_CHARGE_CHEM[mission_point_count] = 0.0
				mission_data_PL_BATT_CHARGE_OHM[mission_point_count] = 0.0
				mission_data_batt_charging_power[mission_point_count] = 0.0
				mission_data_batt_charging_current[mission_point_count] = 0.0
				
				print("##### CAUTION: BATTERIES ARE FULLY CHARGED AND INCOMING SOLAR ENERGY IS LOST #########, time= ",mission_time,"[s] OR ",takeoff_time+mission_time/3600.,"[h]")
			else:
				mission_data_excess_solar_power.append(0.0)

			mission_data_Energy_Remaining.append(remaining_Bat_Energy)	
			excess_solar_energy=excess_solar_energy+mission_data_excess_solar_power[mission_point_count]*timestep/3600.
			mission_data_harvested_Sol_Energy.append(total_Solar_Energy-excess_solar_energy)			
			
			EPU_Eff_TMP=mission_data_Pprop[mission_point_count] /(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count])
			mission_data_Efficiency_EPU.append(EPU_Eff_TMP)
			mission_data_Efficiency_TOT.append(mission_data_climbrate[mission_point_count] * 9.81 * aircraft_mass/(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count]+ mission_data_PL_MOT_TOT_MEAS[mission_point_count]))
			

			if Prop_Eff_TMP > 0.:
				mission_data_Efficiency_MOT_MC.append(EPU_Eff_TMP/Prop_Eff_TMP)
				#print("alt:",act_altitude,"epu-eff:",EPU_Eff_TMP,"prop-eff:",Prop_Eff_TMP, "MOT-MC-eff:",EPU_Eff_TMP/Prop_Eff_TMP)
				#print("Pprop:",mission_data_Pprop[mission_point_count],"Pshaft:",mission_data_Pshaft[mission_point_count],"PL_Mot_tot:",mission_data_PL_MOT_TOT[mission_point_count],"PL_MC:",mission_data_PL_MC[mission_point_count],"PL_MOT_TOT_MEAS:",mission_data_PL_MOT_TOT_MEAS[mission_point_count],"\n")
			else:
				mission_data_Efficiency_MOT_MC.append(0.0)

			if(act_altitude+timestep*act_climbrate < float(altitude2[i])):
				check=1
			else:
				check=0
			
			act_altitude=act_altitude+timestep*act_climbrate
			
			x=[]
			y=[]
			x.append(altitude1[i])
			x.append(altitude2[i])
			y.append(climbrate1[i])
			y.append(climbrate2[i])
			act_climbrate= np.interp(act_altitude,x,y)
			del(x)
			del(y)
			mission_time=mission_time+timestep
			mission_point_count=mission_point_count+1
	
		
		sys.stdout.flush()
		print("     MISSION PHASE CALCULATED SUCCESSFULLY          ")
		
	###########################################################################################################################################################
	###########################################################################################################################################################
	###                                                                                                                                                     ###
	###                                                      HOVER PHASE                                                                                    ###
	###                                                                                                                                                     ###
	###########################################################################################################################################################
	###########################################################################################################################################################

	if(mission_phase_type[i] == 'HOVER_ALTIT_TIME'): 
		
		act_climbrate=climbrate1[i]

		if(calc_type == "RECALC"):
			print("\n\n################## MISSION PHASE #",i+1, "- HOVER ##################\n     DEFINED HOVER ALTITUDE: ", altitude1[i],"m. \n     DBeta: ",dbeta1[i],"deg. \n     TAS: ",tas1[i],"m/s. \n     HOVER DURATION: ",phase_duration[i],"hrs. \n     TIME AT START OF PHASE: ",takeoff_time+mission_time/3600.,"hrs")	
		else:
			print("\n\n################## MISSION PHASE #",i+1, "- HOVER ##################\n     DEFINED HOVER ALTITUDE: ", altitude1[i],"m. \n     HOVER DURATION: ",phase_duration[i],"hrs. \n     TIME AT START OF PHASE: ",takeoff_time+mission_time/3600.,"hrs")	

		
		if (abs(act_altitude - altitude1[i]) < altitude_mismatch_limit):
			print("     ACTUAL ALTITUDE AT START: ",act_altitude,"m. --> ALTITUDE ERROR: ",act_altitude - altitude1[i],"m")
		else:
			print("\n\n     ##### CALCULATION ABORED DUE TO EXCESSIVE ALTITUDE MISMATCH ########\n     FINAL ALTITUDE OF PRECEDING PHASE: ",act_altitude,"m, TARGET ALTITUDE OF CURRENT PHASE: ",altitude1[i]," m. --> ALTITUDE ERROR BETWEEN PHASES: ",act_altitude - altitude1[i],"m")
			exit()		
		
		altitude_error_sum=altitude_error_sum+(act_altitude - altitude1[i])
		hover_time=0
		check=1
		
		if(calc_type == "OPT"):
			mot_data_M_act_altitude = INTERPOL_ALT_CLIMBRATE(mot_data_M_BestEff,act_altitude,act_climbrate)	
		else:
			mot_data_M_act_altitude = INTERPOL_MOT_DAT_OVER_ALT(MOT_DAT1[i],MOT_DAT2[i],act_altitude,act_climbrate)

		
		while(check==1): ################ turns to 0 if final time is reached
			
			print("     ELAPSED TIME: %1.2fh - %1.0f%%" % (hover_time/3600,100*hover_time/phase_duration[i]/3600.),end='\r')
			sys.stdout.flush()
			
			####### STORING MISSION VARIABLES ###################################################################
			mission_data_time.append(mission_time)
			curr_time_h=takeoff_time+mission_time/3600.
			mission_data_real_time_h.append(curr_time_h)
			mission_data_altitude.append(act_altitude)
			mission_data_Potential_Energy.append((act_altitude-altitude1[0])*aircraft_mass*9.81/3600.)
			velocity=float(mot_data_M_act_altitude[1])
			RE = GET_REYNOLDS(act_altitude,velocity,ReynoldsReferenceLength)
			mission_data_reynolds.append(RE)
			mission_data_mach.append(GET_MACH(act_altitude,velocity))
			mission_data_velocity.append(velocity)
			mission_data_climbrate.append(0.0)
			mission_data_DBeta.append(float(round(mot_data_M_act_altitude[3],2)))
			Prop_Eff_TMP=float(mot_data_M_act_altitude[4])
			mission_data_Efficiency_prop.append(Prop_Eff_TMP)			
			mission_data_RPM.append(float(mot_data_M_act_altitude[7]))
			mission_data_Pprop.append(float(mot_data_M_act_altitude[8]))
			mission_data_Pshaft.append(float(mot_data_M_act_altitude[9]))
			mission_data_Thrust.append(float(mot_data_M_act_altitude[11]))
			mission_data_Q.append(float(mot_data_M_act_altitude[12]))
			if tempprofile_flag == 1:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getStandardTemperature(getGeopotential(act_altitude))
			else:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			mission_data_density.append(density)
			CL_tmp=2*aircraft_mass*9.81/density/velocity/velocity/AircraftReferenceArea
			mission_data_CL.append(CL_tmp)
			mission_data_alfa.append(np.interp(CL_tmp, pol_diag_CL[0], pol_diag_alpha[0]))
			mission_data_Mot_Current.append(float(mot_data_M_act_altitude[13]))
			mission_data_maxcurr.append(max_current)
			mission_data_peakcurr.append(peak_current)			
			mission_data_PL_MC.append(float(mot_data_M_act_altitude[16]))
			mission_data_PL_PROP.append(float(mot_data_M_act_altitude[17]))
			mission_data_PL_MOT_COPPER.append(float(mot_data_M_act_altitude[18]))
			mission_data_PL_MOT_BE_FE.append(float(mot_data_M_act_altitude[19]))
			mission_data_PL_MOT_TOT.append(float(mot_data_M_act_altitude[20]))
			mission_data_PL_MOT_TOT_MEAS.append(float(mot_data_M_act_altitude[21]))	
			mission_data_CURR_CONST_MEAS.append(float(mot_data_M_act_altitude[22]))	



			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################
			#############################            NEW SOLARCALC ROUTINE                      ##########################################
			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################

			############################################### Solar Energy Generation #################################################
			solPowDens=getSolarPowerDensity(day_of_year)
			OUT_GSV=GET_SUNVECTOR(day_of_year,curr_time_h+sunculm_delta_time_h,latitude)
			sun_vector=[OUT_GSV[5],OUT_GSV[6],OUT_GSV[7]]
					
			############# STORING RESULTS IN VARIABLES FOR OUTPUT IN CSV FILE #######################################################
			mission_data_incangle.append(OUT_GSV[0])
			mission_data_hour_angle.append(OUT_GSV[1])
			mission_data_declination_angle.append(OUT_GSV[2])
			mission_data_zenith_angle.append(OUT_GSV[3])
			mission_data_azimuth_angle.append(OUT_GSV[4])
			mission_data_sunvector_x.append(OUT_GSV[5])
			mission_data_sunvector_y.append(OUT_GSV[6])
			mission_data_sunvector_z.append(OUT_GSV[7])

			############# Additional Losses due to MPPT and ohm-losses in cables (1.5mmsq, at 7A peak current, 8m cable length).
			addLosses=mppt_eff/100.*(1.-cable_loss/100.)	
			
			############# CALCULATE ATMOSPHERIC TEMPERATURE ##########################################
			if tempprofile_flag == 1:
				temp_atmo = getStandardTemperature(getGeopotential(act_altitude))
			else:
				temp_atmo = getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			############# CSV OUTPUT ROUTINES ######################################################
			write_cells_temp.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_effi.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_powers.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
								


			############# CALCULATE ATMOSPHERIC TRANSMISSION AND  SOLAR CELL ORIENTATIONS ################################################
			if (act_altitude > -np.abs(getDepression(act_altitude))):
				atmoTransmiss = getAtmoTransmission(act_altitude,mission_data_incangle[mission_point_count],getDepression(act_altitude))
				SPS=GET_SUN_PROJ_SURF(sun_vector,mission_data_alfa[mission_point_count])
				solcell_normals=SPS[10]
				solcell_count=SPS[12]
				Reflength=SPS[17]
			else:
				atmoTransmiss = 0.0
				SPS = 0.0
				Reflength=0.0

			mission_data_atmo_transmission.append(atmoTransmiss)
			
			s=(int(360/5),sol_cell_orientations)
			CELL_GROUP_TEMP = np.zeros(s)
			CELL_GROUP_EFF   = np.zeros(s)
			CELL_GROUP_POW   = np.zeros(s)
			
			
			#### DO THIS ONLY IF SUNLIGHT IS AVAILABLE #######
			if atmoTransmiss > 0:
				########### CALCULATE THE TEMPERATURE, THE EFFICIENCY AND THE SOLAR POWER FOR ALL ORIENTATIONS IN 5DEG-INCREMENTS OVER 360 DEGREES
				for count_i in range(0,360,5):
					for j in range(0,sol_cell_orientations):
						lamturb=SPS[15]
						rotated_solcel_normals = np.dot(rotation_matrix(np.array([0,0,-1]),-count_i), sum(solcell_normals[j]))	
						CosPsi = np.dot(sun_vector,rotated_solcel_normals/-np.linalg.norm(rotated_solcel_normals))
						if CosPsi >=0:
							CosPsi=CosPsi
						else:
							CosPsi=0.
						solcel_surf_transmission = cell_surf_transmission(angle_between(sun_vector,-rotated_solcel_normals))
						
						solcel_Temp = getSolCellTempEnerBal(solcel_limtemp_alt,solcel_limtemp_mintemp,solcel_limtemp_maxtemp,solcell_nom_eff,eff_grad,lamturb[j],CosPsi,temp_atmo, RE, act_altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,Reflength[j])
						CELL_GROUP_TEMP[int(count_i/5),j] = solcel_Temp-273.15
						solcel_effi = getEfficiencySolarcells(solcel_Temp,solcell_nom_eff,eff_grad)/100.
						CELL_GROUP_EFF[int(count_i/5),j] = solcel_effi
						sol_pow_inc = np.linalg.norm(rotated_solcel_normals)*CosPsi*solPowDens*atmoTransmiss*solcel_surf_transmission*addLosses*solcel_effi
						cutoff_power_per_direction = solcell_count[j] * mppt_cutoff_power_per_cell
						
						### CHECK IF THE SOLAR CELLS ON EACH STRING DELIVER ENOUGH POWER TO BE ABOVE THE MPPT THREASHOLD ##########
						if sol_pow_inc > cutoff_power_per_direction:
							CELL_GROUP_POW[int(count_i/5)][j] = sol_pow_inc
						else:
							CELL_GROUP_POW[int(count_i/5)][j] = 0.0
			else:
				CELL_GROUP_TEMP[:,:] =	temp_atmo-273.15
				CELL_GROUP_EFF[:,:] = getEfficiencySolarcells(temp_atmo,solcell_nom_eff,eff_grad)/100.
				CELL_GROUP_POW[:,:] = 0.0
				best_head = 0.0	

			#### ACCUMULATE OVER 360DEG ####################################
			solpow_total=[]
			for count_i in range(0,360,5):
				sum_solpow = 0.0
				for j in range(0,sol_cell_orientations):	
					sum_solpow = sum_solpow + CELL_GROUP_POW[int(count_i/5)][j]
				solpow_total.append(sum_solpow)
			
			#### MAXIMUM SOLAR POWER CONDITION ###################################	
			best_head = 5*np.argmax(solpow_total)
			best_solpow = np.max(solpow_total)
			
			#### AVERAGE SOLAR POWER CONDITION ###################################
			average_solpow = np.average(solpow_total)
			
			#### MINIMUM SOLAR POWER CONDITION ###################################
			min_solpow = np.min(solpow_total)
			min_head = 5*np.argmin(solpow_total)
			
			#### WRITE TO FILE THE BEST HEADING, THE BEST SOLAR POWER, THE AVERAGE SOLAR POWER, THE MINIMUM-SOL-POW HEADING AND THE MINIMUM SOLAR POWER ##################
			write_cells_powers.write(str(best_head)+","+str(best_solpow)+","+str(average_solpow)+","+str(min_head)+","+str(min_solpow)+",")
			
			#### CALCULATE EXTREMA FOR TEMPERATURES AND EFFICIENCIES OVER ALL ORIENTATIONS AND ALL CELL GROUPS TOGETHER ############################
			MinCelTemp = np.amin(CELL_GROUP_TEMP)
			MaxCelTemp = np.amax(CELL_GROUP_TEMP)
			AvgCelTemp = np.average(CELL_GROUP_TEMP)
			MinCelEffi = np.amin(CELL_GROUP_EFF)
			MaxCelEffi = np.amax(CELL_GROUP_EFF)
			AvgCelEffi = np.average(CELL_GROUP_EFF)
			
			
			mission_data_cell_temp_individual.append(CELL_GROUP_TEMP[np.argmax(solpow_total)][:])
			mission_data_cell_efficiency_individual.append(CELL_GROUP_EFF[np.argmax(solpow_total)][:])
			
			#### WRITE EXTREMA/AVERAGE TEMP AND EFFI TO FILE ########################################################
			write_cells_temp.write(str(MaxCelTemp)+","+str(AvgCelTemp)+","+str(MinCelTemp)+",")
			write_cells_effi.write(str(MaxCelEffi)+","+str(AvgCelEffi)+","+str(MinCelEffi)+",")
			
			#### WRITE SOLPOW/TEMP/EFFI DATA FOR BEST HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(CELL_GROUP_TEMP[int(best_head/5),j])+",")
				write_cells_effi.write(str(CELL_GROUP_EFF[int(best_head/5),j])+",")
				write_cells_powers.write(str(CELL_GROUP_POW[int(best_head/5),j])+",")

			#### WRITE SOLPOW/TEMP/EFFI DATA FOR AVERAGE HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(np.average(CELL_GROUP_TEMP[:,j]))+",")
				write_cells_effi.write(str(np.average(CELL_GROUP_EFF[:,j]))+",")
				write_cells_powers.write(str(np.average(CELL_GROUP_POW[:,j]))+",")
			
			mission_data_cell_temp_min.append(MinCelTemp)
			mission_data_cell_temp_max.append(MaxCelTemp)
			mission_data_cell_temp_avg.append(AvgCelTemp)
			mission_data_cell_efficiency_min.append(MinCelEffi)
			mission_data_cell_efficiency_max.append(MaxCelEffi)
			mission_data_cell_efficiency_avg.append(AvgCelEffi)

			del solpow_total			
			del CELL_GROUP_TEMP
			del CELL_GROUP_EFF
			del CELL_GROUP_POW

			mission_data_sol_pow_max.append(best_solpow)
			mission_data_sol_pow_avg.append(average_solpow)
			
			############# PATH CALCULATIONS ##########################################
			mission_data_best_heading.append(best_head)
			delta_east  = np.sin(best_head*np.pi/180.)*timestep*velocity/1000.
			delta_north = np.cos(best_head*np.pi/180.)*timestep*velocity/1000.
			mission_data_distance_east.append(mission_data_distance_east[mission_point_count]+delta_east)
			mission_data_distance_north.append(mission_data_distance_north[mission_point_count]+delta_north)
			
			if heading_strategy == 1:
				mission_data_sol_pow.append(best_solpow)
			if heading_strategy == 2:
				mission_data_sol_pow.append(average_solpow)
			if heading_strategy < 1 or heading_strategy > 2:
				print("################ NON EXISTANT HEADING STRATEGY SPECIFIED ###############")
				exit()

			##### Current Increment of Solar Energy #########################	
			if(mission_point_count>0):
				delta_Solar_Energy=(mission_data_sol_pow[mission_point_count-1]+mission_data_sol_pow[mission_point_count])/2.0*timestep/3600.
			else:
				delta_Solar_Energy=0.0
				
			##### Total Accumulated Solar Energy ############################
			total_Solar_Energy=total_Solar_Energy+delta_Solar_Energy
			

			
			
			
			
			
			
			
			
			##################### CALCULATE THE BATTERY POWER AND BATTERY CURRENT #######################################################
			
			P_Bat_ex = mission_data_Pshaft[mission_point_count] +  mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count] - mission_data_sol_pow[mission_point_count] + sys_pwr_drn
			mission_data_PBat_ex.append(P_Bat_ex)
			
			if mission_point_count == 0:
				current_capacity=bat_cap_status_start
			else:
				current_capacity= mission_data_Energy_Remaining[mission_point_count-1]
			
			#### BATTERY IS BEING DISCHARGED, CALCULATE LOSSES OF BATTERY #############################################
			if P_Bat_ex > 0: 
				if bat_type == "14S_20P_3500_LiIo":
					battery_voltage_current = GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(P_Bat_ex,current_capacity,number_of_batteries,single_bat_cap)
				if bat_type == "52V":
					battery_voltage_current = GET_BAT_VOLTAGE_CURRENT_52V(P_Bat_ex,current_capacity,number_of_batteries,single_bat_cap)
				
				Bat_Volt = battery_voltage_current[0]
				Bat_Curr = battery_voltage_current[1]
				PL_Bat = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				PBat_int = P_Bat_ex + (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				
				### No Charging Losses Apply ####
				P_chargloss_chem = 0.
				P_chargloss_ohm = 0.
				batt_charging_power = 0.0
				batt_charging_current = 0.0
			
			#### NO ENREGY IS FLOWING IN OR OUT OF THE BATTERY, LOSSES ARE ZERO ######################################
			if P_Bat_ex == 0.: 
				if bat_type == "14S_20P_3500_LiIo":
					Bat_Volt = GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(0.,mission_data_Energy_Remaining[mission_point_count-1],number_of_batteries,single_bat_cap)[0]
				if bat_type == "52V":
					Bat_Volt = GET_BAT_VOLTAGE_CURRENT_52V(0.,mission_data_Energy_Remaining[mission_point_count-1],number_of_batteries,single_bat_cap)[0]
				
				Bat_Curr = 0.
				PL_Bat = 0.0
				PBat_int = 0.0
				
				### No Charging Losses Apply ####
				P_chargloss_chem = 0.
				P_chargloss_ohm = 0.
				batt_charging_power = 0.0
				batt_charging_current = 0.0
			
			#### BATTERY IS BEING CHARGED, CALCULATE LOSSES OF BATTERY #############################################
			if P_Bat_ex < 0: 
				if bat_type == "14S_20P_3500_LiIo":
					Bat_Volt = GET_BAT_VOLTAGE_14S_20P_3500_LiIo(0.,current_capacity,number_of_batteries,single_bat_cap)
				if bat_type == "52V":
					Bat_Volt = GET_BAT_VOLTAGE_52V(0.,current_capacity,number_of_batteries,single_bat_cap)
				
				Bat_Curr = P_Bat_ex/Bat_Volt
				PL_Bat = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				PBat_int = P_Bat_ex - (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				
				### Charging Losses Apply ####
				P_chargloss_chem = ((-P_Bat_ex + (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries) * (bat_chg_red_chem/100.))
				P_chargloss_ohm = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				batt_charging_power = -P_Bat_ex
				batt_charging_current = -Bat_Curr				
			
			mission_data_PL_Bat.append(PL_Bat)
			mission_data_PBat_int.append(PBat_int)			
			mission_data_Bat_Current.append(Bat_Curr)
			mission_data_Bat_Voltage.append(Bat_Volt)			
			mission_data_PL_BATT_CHARGE_CHEM.append(P_chargloss_chem)
			mission_data_PL_BATT_CHARGE_OHM.append(P_chargloss_ohm)		
			mission_data_batt_charging_power.append(batt_charging_power)
			mission_data_batt_charging_current.append(batt_charging_current)					
			mission_data_Ptot.append(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count])
	
			############## START INTEGRATE ##########################
			### Energy Consumption ###
			if(mission_point_count>0):
				delta_Energy_inc=(mission_data_Ptot[mission_point_count] + sys_pwr_drn + P_chargloss_chem + P_chargloss_ohm + mission_data_PL_Bat[mission_point_count])*timestep/3600. 
			else:
				delta_Energy_inc=0.
			
			total_Energy_spent=total_Energy_spent+delta_Energy_inc
			mission_data_Energy_Spent.append(total_Energy_spent)			
			
			### Overall Energy Status ###
			remaining_Bat_Energy = remaining_Bat_Energy - delta_Energy_inc + delta_Solar_Energy
			
			if remaining_Bat_Energy > bat_cap:
				mission_data_excess_solar_power.append((remaining_Bat_Energy-bat_cap)/timestep*3600.)
				remaining_Bat_Energy = bat_cap
				mission_data_PL_BATT_CHARGE_CHEM[mission_point_count] = 0.0
				mission_data_PL_BATT_CHARGE_OHM[mission_point_count] = 0.0
				mission_data_batt_charging_power[mission_point_count] = 0.0
				mission_data_batt_charging_current[mission_point_count] = 0.0
				
				print("##### CAUTION: BATTERIES ARE FULLY CHARGED AND INCOMING SOLAR ENERGY IS LOST #########, time= ",mission_time,"[s] OR ",takeoff_time+mission_time/3600.,"[h]")
			else:
				mission_data_excess_solar_power.append(0.0)

			mission_data_Energy_Remaining.append(remaining_Bat_Energy)
			excess_solar_energy=excess_solar_energy+mission_data_excess_solar_power[mission_point_count]*timestep/3600.
			mission_data_harvested_Sol_Energy.append(total_Solar_Energy-excess_solar_energy)	
			
			EPU_Eff_TMP=mission_data_Pprop[mission_point_count] /(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count])
			mission_data_Efficiency_EPU.append(EPU_Eff_TMP)
			mission_data_Efficiency_TOT.append(0.0)
			
			if Prop_Eff_TMP > 0.:
				mission_data_Efficiency_MOT_MC.append(EPU_Eff_TMP/Prop_Eff_TMP)
			else:
				mission_data_Efficiency_MOT_MC.append(0.0)
		
			if hover_time > phase_duration[i]*3600.:
				check = 0
		
			mission_time=mission_time+timestep
			hover_time=hover_time+timestep	
			mission_point_count=mission_point_count+1			

		sys.stdout.flush()
		print("     MISSION PHASE CALCULATED SUCCESSFULLY          ")

	
	###########################################################################################################################################################
	###########################################################################################################################################################
	###                                                                                                                                                     ###
	###                                                      GLIDE PHASE                                                                                    ###
	###                                                                                                                                                     ###
	###########################################################################################################################################################
	###########################################################################################################################################################
	
	if(mission_phase_type[i] == 'GLIDE_TO_ALTITUD'): 
		if(i == 0):
			print("########## GLIDE is not possible as first step, insert a CLIMB Step before #########")
			exit()		
		
		print("\n\n################## MISSION PHASE #",i+1, "- GLIDE ##################\n     ACTUAL ALTITUDE AT START: ", act_altitude,"m. DEFINED ALTITUDE AT END: ",altitude2[i],"m.\n     TIME AT START OF GLIDE PHASE: ",takeoff_time+mission_time/3600.," hrs")
		check=1
		startalt = act_altitude
		while(check==1): # turns to 0 if final altitude is reached					
			
			print("     CURRENT ALTITUDE: %1.0fm - %1.0f%%" % (act_altitude,100*(startalt-act_altitude)/(startalt-altitude2[i])),end='\r')
			sys.stdout.flush()


			####### STORING MISSION VARIABLES ###################################################################
			mission_data_time.append(mission_time)
			curr_time_h=takeoff_time+mission_time/3600.
			mission_data_real_time_h.append(curr_time_h)
			mission_data_altitude.append(act_altitude)
			mission_data_Potential_Energy.append((act_altitude-altitude1[0])*aircraft_mass*9.81/3600.)
			velocity=np.interp(act_altitude,altitude_polar,pol_diag_QInf_at_v_sink_min)
			RE = GET_REYNOLDS(act_altitude,velocity,ReynoldsReferenceLength)
			mission_data_reynolds.append(RE)
			mission_data_mach.append(GET_MACH(act_altitude,velocity))
			mission_data_velocity.append(velocity)
			act_climbrate=(-1.)*np.interp(act_altitude,altitude_polar,pol_diag_v_sink_min)			
			mission_data_climbrate.append(act_climbrate)
			mission_data_DBeta.append(0.0)
			mission_data_Efficiency_prop.append(0.0)
			mission_data_RPM.append(0.0)
			mission_data_Pprop.append(0.0)
			mission_data_Pshaft.append(0.0)
			mission_data_Thrust.append(0.0)
			mission_data_Q.append(0.0)
			
			if tempprofile_flag == 1:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getStandardTemperature(getGeopotential(act_altitude))
			else:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			mission_data_density.append(density)
			CL_tmp=2*aircraft_mass*9.81/density/velocity/velocity/AircraftReferenceArea
			mission_data_CL.append(CL_tmp)
			mission_data_alfa.append(np.interp(CL_tmp, pol_diag_CL[0], pol_diag_alpha[0]))
			mission_data_Mot_Current.append(0.0)
			mission_data_maxcurr.append(max_current)
			mission_data_peakcurr.append(peak_current)	
			
			mission_data_PL_MC.append(0.0)
			mission_data_PL_PROP.append(0.0)
			mission_data_PL_MOT_COPPER.append(0.0)
			mission_data_PL_MOT_BE_FE.append(0.0)
			mission_data_PL_MOT_TOT.append(0.0)
			mission_data_PL_MOT_TOT_MEAS.append(0.0)
			mission_data_CURR_CONST_MEAS.append(0.0)
			

			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################
			#############################            NEW SOLARCALC ROUTINE                      ##########################################
			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################

			############################################### Solar Energy Generation #################################################
			solPowDens=getSolarPowerDensity(day_of_year)
			OUT_GSV=GET_SUNVECTOR(day_of_year,curr_time_h+sunculm_delta_time_h,latitude)
			sun_vector=[OUT_GSV[5],OUT_GSV[6],OUT_GSV[7]]
					
			############# STORING RESULTS IN VARIABLES FOR OUTPUT IN CSV FILE #######################################################
			mission_data_incangle.append(OUT_GSV[0])
			mission_data_hour_angle.append(OUT_GSV[1])
			mission_data_declination_angle.append(OUT_GSV[2])
			mission_data_zenith_angle.append(OUT_GSV[3])
			mission_data_azimuth_angle.append(OUT_GSV[4])
			mission_data_sunvector_x.append(OUT_GSV[5])
			mission_data_sunvector_y.append(OUT_GSV[6])
			mission_data_sunvector_z.append(OUT_GSV[7])

			############# Additional Losses due to MPPT and ohm-losses in cables (1.5mmsq, at 7A peak current, 8m cable length).
			addLosses=mppt_eff/100.*(1.-cable_loss/100.)	
			
			############# CALCULATE ATMOSPHERIC TEMPERATURE ##########################################
			if tempprofile_flag == 1:
				temp_atmo = getStandardTemperature(getGeopotential(act_altitude))
			else:
				temp_atmo = getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			############# CSV OUTPUT ROUTINES ######################################################
			write_cells_temp.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_effi.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_powers.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
								


			############# CALCULATE ATMOSPHERIC TRANSMISSION AND  SOLAR CELL ORIENTATIONS ################################################
			if (act_altitude > -np.abs(getDepression(act_altitude))):
				atmoTransmiss = getAtmoTransmission(act_altitude,mission_data_incangle[mission_point_count],getDepression(act_altitude))
				SPS=GET_SUN_PROJ_SURF(sun_vector,mission_data_alfa[mission_point_count])
				solcell_normals=SPS[10]
				solcell_count=SPS[12]
				Reflength=SPS[17]
			else:
				atmoTransmiss = 0.0
				SPS = 0.0
				Reflength=0.0

			mission_data_atmo_transmission.append(atmoTransmiss)
			
			s=(int(360/5),sol_cell_orientations)
			CELL_GROUP_TEMP = np.zeros(s)
			CELL_GROUP_EFF   = np.zeros(s)
			CELL_GROUP_POW   = np.zeros(s)
			
			
			#### DO THIS ONLY IF SUNLIGHT IS AVAILABLE #######
			if atmoTransmiss > 0:
				########### CALCULATE THE TEMPERATURE, THE EFFICIENCY AND THE SOLAR POWER FOR ALL ORIENTATIONS IN 5DEG-INCREMENTS OVER 360 DEGREES
				for count_i in range(0,360,5):
					for j in range(0,sol_cell_orientations):
						lamturb=SPS[15]
						rotated_solcel_normals = np.dot(rotation_matrix(np.array([0,0,-1]),-count_i), sum(solcell_normals[j]))	
						CosPsi = np.dot(sun_vector,rotated_solcel_normals/-np.linalg.norm(rotated_solcel_normals))
						if CosPsi >=0:
							CosPsi=CosPsi
						else:
							CosPsi=0.
						solcel_surf_transmission = cell_surf_transmission(angle_between(sun_vector,-rotated_solcel_normals))
						
						solcel_Temp = getSolCellTempEnerBal(solcel_limtemp_alt,solcel_limtemp_mintemp,solcel_limtemp_maxtemp,solcell_nom_eff,eff_grad,lamturb[j],CosPsi,temp_atmo, RE, act_altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,Reflength[j])
						CELL_GROUP_TEMP[int(count_i/5),j] = solcel_Temp-273.15
						solcel_effi = getEfficiencySolarcells(solcel_Temp,solcell_nom_eff,eff_grad)/100.
						CELL_GROUP_EFF[int(count_i/5),j] = solcel_effi
						sol_pow_inc = np.linalg.norm(rotated_solcel_normals)*CosPsi*solPowDens*atmoTransmiss*solcel_surf_transmission*addLosses*solcel_effi
						cutoff_power_per_direction = solcell_count[j] * mppt_cutoff_power_per_cell
						
						### CHECK IF THE SOLAR CELLS ON EACH STRING DELIVER ENOUGH POWER TO BE ABOVE THE MPPT THREASHOLD ##########
						if sol_pow_inc > cutoff_power_per_direction:
							CELL_GROUP_POW[int(count_i/5)][j] = sol_pow_inc
						else:
							CELL_GROUP_POW[int(count_i/5)][j] = 0.0
			else:
				CELL_GROUP_TEMP[:,:] =	temp_atmo-273.15
				CELL_GROUP_EFF[:,:] = getEfficiencySolarcells(temp_atmo,solcell_nom_eff,eff_grad)/100.
				CELL_GROUP_POW[:,:] = 0.0
				best_head = 0.0	

			#### ACCUMULATE OVER 360DEG ####################################
			solpow_total=[]
			for count_i in range(0,360,5):
				sum_solpow = 0.0
				for j in range(0,sol_cell_orientations):	
					sum_solpow = sum_solpow + CELL_GROUP_POW[int(count_i/5)][j]
				solpow_total.append(sum_solpow)
			
			#### MAXIMUM SOLAR POWER CONDITION ###################################	
			best_head = 5*np.argmax(solpow_total)
			best_solpow = np.max(solpow_total)
			
			#### AVERAGE SOLAR POWER CONDITION ###################################
			average_solpow = np.average(solpow_total)
			
			#### MINIMUM SOLAR POWER CONDITION ###################################
			min_solpow = np.min(solpow_total)
			min_head = 5*np.argmin(solpow_total)
			
			#### WRITE TO FILE THE BEST HEADING, THE BEST SOLAR POWER, THE AVERAGE SOLAR POWER, THE MINIMUM-SOL-POW HEADING AND THE MINIMUM SOLAR POWER ##################
			write_cells_powers.write(str(best_head)+","+str(best_solpow)+","+str(average_solpow)+","+str(min_head)+","+str(min_solpow)+",")
			
			#### CALCULATE EXTREMA FOR TEMPERATURES AND EFFICIENCIES OVER ALL ORIENTATIONS AND ALL CELL GROUPS TOGETHER ############################
			MinCelTemp = np.amin(CELL_GROUP_TEMP)
			MaxCelTemp = np.amax(CELL_GROUP_TEMP)
			AvgCelTemp = np.average(CELL_GROUP_TEMP)
			MinCelEffi = np.amin(CELL_GROUP_EFF)
			MaxCelEffi = np.amax(CELL_GROUP_EFF)
			AvgCelEffi = np.average(CELL_GROUP_EFF)
		
			mission_data_cell_temp_individual.append(CELL_GROUP_TEMP[np.argmax(solpow_total)][:])
			mission_data_cell_efficiency_individual.append(CELL_GROUP_EFF[np.argmax(solpow_total)][:])
			
			#### WRITE EXTREMA/AVERAGE TEMP AND EFFI TO FILE ########################################################
			write_cells_temp.write(str(MaxCelTemp)+","+str(AvgCelTemp)+","+str(MinCelTemp)+",")
			write_cells_effi.write(str(MaxCelEffi)+","+str(AvgCelEffi)+","+str(MinCelEffi)+",")
			
			#### WRITE SOLPOW/TEMP/EFFI DATA FOR BEST HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(CELL_GROUP_TEMP[int(best_head/5),j])+",")
				write_cells_effi.write(str(CELL_GROUP_EFF[int(best_head/5),j])+",")
				write_cells_powers.write(str(CELL_GROUP_POW[int(best_head/5),j])+",")

			#### WRITE SOLPOW/TEMP/EFFI DATA FOR AVERAGE HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(np.average(CELL_GROUP_TEMP[:,j]))+",")
				write_cells_effi.write(str(np.average(CELL_GROUP_EFF[:,j]))+",")
				write_cells_powers.write(str(np.average(CELL_GROUP_POW[:,j]))+",")
			
			mission_data_cell_temp_min.append(MinCelTemp)
			mission_data_cell_temp_max.append(MaxCelTemp)
			mission_data_cell_temp_avg.append(AvgCelTemp)
			mission_data_cell_efficiency_min.append(MinCelEffi)
			mission_data_cell_efficiency_max.append(MaxCelEffi)
			mission_data_cell_efficiency_avg.append(AvgCelEffi)

			del solpow_total			
			del CELL_GROUP_TEMP
			del CELL_GROUP_EFF
			del CELL_GROUP_POW

			mission_data_sol_pow_max.append(best_solpow)
			mission_data_sol_pow_avg.append(average_solpow)
			
			############# PATH CALCULATIONS ##########################################
			mission_data_best_heading.append(best_head)
			delta_east  = np.sin(best_head*np.pi/180.)*timestep*velocity/1000.
			delta_north = np.cos(best_head*np.pi/180.)*timestep*velocity/1000.
			mission_data_distance_east.append(mission_data_distance_east[mission_point_count]+delta_east)
			mission_data_distance_north.append(mission_data_distance_north[mission_point_count]+delta_north)
			
			if heading_strategy == 1:
				mission_data_sol_pow.append(best_solpow)
			if heading_strategy == 2:
				mission_data_sol_pow.append(average_solpow)
			if heading_strategy < 1 or heading_strategy > 2:
				print("################ NON EXISTANT HEADING STRATEGY SPECIFIED ###############")
				exit()

			##### Current Increment of Solar Energy #########################	
			if(mission_point_count>0):
				delta_Solar_Energy=(mission_data_sol_pow[mission_point_count-1]+mission_data_sol_pow[mission_point_count])/2.0*timestep/3600.
			else:
				delta_Solar_Energy=0.0
				
			##### Total Accumulated Solar Energy ############################
			total_Solar_Energy=total_Solar_Energy+delta_Solar_Energy
			
			

			##################### CALCULATE THE BATTERY POWER AND BATTERY CURRENT #######################################################
			
			P_Bat_ex =  - mission_data_sol_pow[mission_point_count] + sys_pwr_drn
			mission_data_PBat_ex.append(P_Bat_ex)
			
			if mission_point_count == 0:
				current_capacity=bat_cap_status_start
			else:
				current_capacity= mission_data_Energy_Remaining[mission_point_count-1]
			
			#### BATTERY IS BEING DISCHARGED, CALCULATE LOSSES OF BATTERY #############################################
			if P_Bat_ex > 0: 
				if bat_type == "14S_20P_3500_LiIo":
					battery_voltage_current = GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(P_Bat_ex,current_capacity,number_of_batteries,single_bat_cap)
				if bat_type == "52V":
					battery_voltage_current = GET_BAT_VOLTAGE_CURRENT_52V(P_Bat_ex,current_capacity,number_of_batteries,single_bat_cap)
				
				Bat_Volt = battery_voltage_current[0]
				Bat_Curr = battery_voltage_current[1]
				PL_Bat = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				PBat_int = P_Bat_ex + (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				
				### No Charging Losses Apply ####
				P_chargloss_chem = 0.
				P_chargloss_ohm = 0.
				batt_charging_power = 0.0
				batt_charging_current = 0.0
			
			#### NO ENREGY IS FLOWING IN OR OUT OF THE BATTERY, LOSSES ARE ZERO ######################################
			if P_Bat_ex == 0.: 
				if bat_type == "14S_20P_3500_LiIo":
					Bat_Volt = GET_BAT_VOLTAGE_CURRENT_14S_20P_3500_LiIo(0.,mission_data_Energy_Remaining[mission_point_count-1],number_of_batteries,single_bat_cap)[0]
				if bat_type == "52V":
					Bat_Volt = GET_BAT_VOLTAGE_CURRENT_52V(0.,mission_data_Energy_Remaining[mission_point_count-1],number_of_batteries,single_bat_cap)[0]
				
				Bat_Curr = 0.
				PL_Bat = 0.0
				PBat_int = 0.0
				
				### No Charging Losses Apply ####
				P_chargloss_chem = 0.
				P_chargloss_ohm = 0.
				batt_charging_power = 0.0
				batt_charging_current = 0.0
			
			#### BATTERY IS BEING CHARGED, CALCULATE LOSSES OF BATTERY #############################################
			if P_Bat_ex < 0: 
				if bat_type == "14S_20P_3500_LiIo":
					Bat_Volt = GET_BAT_VOLTAGE_14S_20P_3500_LiIo(0.,current_capacity,number_of_batteries,single_bat_cap)
				if bat_type == "52V":
					Bat_Volt = GET_BAT_VOLTAGE_52V(0.,current_capacity,number_of_batteries,single_bat_cap)
				
				Bat_Curr = P_Bat_ex/Bat_Volt
				PL_Bat = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				PBat_int = P_Bat_ex - (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				
				### Charging Losses Apply ####
				P_chargloss_chem = ((-P_Bat_ex + (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries) * (bat_chg_red_chem/100.))
				P_chargloss_ohm = (Bat_Curr/number_of_batteries)**2 * battery_int_res * number_of_batteries
				batt_charging_power = -P_Bat_ex
				batt_charging_current = -Bat_Curr				
			
			mission_data_PL_Bat.append(PL_Bat)
			mission_data_PBat_int.append(PBat_int)			
			mission_data_Bat_Current.append(Bat_Curr)
			mission_data_Bat_Voltage.append(Bat_Volt)			
			mission_data_PL_BATT_CHARGE_CHEM.append(P_chargloss_chem)
			mission_data_PL_BATT_CHARGE_OHM.append(P_chargloss_ohm)		
			mission_data_batt_charging_power.append(batt_charging_power)
			mission_data_batt_charging_current.append(batt_charging_current)					
			mission_data_Ptot.append(0.0)
	

			############## START INTEGRATE ##########################
			### Energy Consumption ###
			if(mission_point_count>0):
				delta_Energy_inc=( sys_pwr_drn + P_chargloss_chem + P_chargloss_ohm + mission_data_PL_Bat[mission_point_count])*timestep/3600. 
			else:
				delta_Energy_inc=0.
			
			total_Energy_spent=total_Energy_spent+delta_Energy_inc
			mission_data_Energy_Spent.append(total_Energy_spent)			
			
			
			### Overall Energy Status ###
			remaining_Bat_Energy = remaining_Bat_Energy - delta_Energy_inc + delta_Solar_Energy
			
			if remaining_Bat_Energy > bat_cap:
				mission_data_excess_solar_power.append((remaining_Bat_Energy-bat_cap)/timestep*3600.)
				remaining_Bat_Energy = bat_cap
				mission_data_PL_BATT_CHARGE_CHEM[mission_point_count] = 0.0
				mission_data_PL_BATT_CHARGE_OHM[mission_point_count] = 0.0
				mission_data_batt_charging_power[mission_point_count] = 0.0
				mission_data_batt_charging_current[mission_point_count] = 0.0
				
				print("##### CAUTION: BATTERIES ARE FULLY CHARGED AND INCOMING SOLAR ENERGY IS LOST #########, time= ",mission_time,"[s] OR ",takeoff_time+mission_time/3600.,"[h]")
			else:
				mission_data_excess_solar_power.append(0.0)

			mission_data_Energy_Remaining.append(remaining_Bat_Energy)				
			excess_solar_energy=excess_solar_energy+mission_data_excess_solar_power[mission_point_count]*timestep/3600.
			mission_data_harvested_Sol_Energy.append(total_Solar_Energy-excess_solar_energy)			
			mission_data_Efficiency_EPU.append(0.0)
			mission_data_Efficiency_TOT.append(0.0)
			mission_data_Efficiency_MOT_MC.append(0.0)
			
			
			
			if(act_altitude+timestep*act_climbrate > float(altitude2[i])):
				check=1
			else:
				check=0
			
			act_altitude=act_altitude+timestep*act_climbrate
			act_climbrate=(-1.)*np.interp(act_altitude,altitude_polar,pol_diag_v_sink_min)
			mission_time=mission_time+timestep
			mission_point_count=mission_point_count+1

		sys.stdout.flush()
		print("     MISSION PHASE CALCULATED SUCCESSFULLY          ")


	###########################################################################################################################################################
	###########################################################################################################################################################
	###                                                                                                                                                     ###
	###                                                      FLY WITH THE SUN PHASE                                                                         ###
	###                                                                                                                                                     ###
	###########################################################################################################################################################
	###########################################################################################################################################################

	if(mission_phase_type[i] == 'FLY_WIT_SUN_TIME'): 

		print("\n\n\n##### CHECK FIRST BEFORE USING - MODIFICATIONS DONE IN FLIY WITH THE SUN ROUTINE! \n##### Bei Verwendung von gemessenen EPU-Wirkungsgraddaten werden hier Leistungen abgefragt, die außerhalb des sinnvollen Bereiches liegen. Damit kommt die Interpolationsroutine nicht zurecht.\n\n\n################## MISSION PHASE #",i+1, "- FLY WITH SUN ##################\n     ACTUAL ALTITUDE AT START: ",act_altitude,"m\n     TIME AT START: ", takeoff_time+mission_time/3600.,"h, DEFINED PHASE DURATION: ",phase_duration[i],"hrs\n")
		check=1
		
		phase_start_time=mission_time
		phase_end_time=phase_start_time+float(phase_duration[i])*3600.
		

		while(check==1): # turns to 0 if period duration is reached
			
			####### STORING MISSION VARIABLES ###################################################################
			mission_data_time.append(mission_time)
			curr_time_h=takeoff_time+mission_time/3600.
			mission_data_real_time_h.append(curr_time_h)
			mission_data_altitude.append(act_altitude)
			mission_data_Potential_Energy.append((act_altitude-altitude1[0])*aircraft_mass*9.81/3600.)
			RE = GET_REYNOLDS(act_altitude,velocity,ReynoldsReferenceLength)
			mission_data_reynolds.append(RE)
			
			############## START INTEGRATE ##########################
			

			
			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################
			#############################            NEW SOLARCALC ROUTINE                      ##########################################
			##############################################################################################################################
			##############################################################################################################################
			##############################################################################################################################

			############################################### Solar Energy Generation #################################################
			solPowDens=getSolarPowerDensity(day_of_year)
			OUT_GSV=GET_SUNVECTOR(day_of_year,curr_time_h+sunculm_delta_time_h,latitude)
			sun_vector=[OUT_GSV[5],OUT_GSV[6],OUT_GSV[7]]
					
			############# STORING RESULTS IN VARIABLES FOR OUTPUT IN CSV FILE #######################################################
			mission_data_incangle.append(OUT_GSV[0])
			mission_data_hour_angle.append(OUT_GSV[1])
			mission_data_declination_angle.append(OUT_GSV[2])
			mission_data_zenith_angle.append(OUT_GSV[3])
			mission_data_azimuth_angle.append(OUT_GSV[4])
			mission_data_sunvector_x.append(OUT_GSV[5])
			mission_data_sunvector_y.append(OUT_GSV[6])
			mission_data_sunvector_z.append(OUT_GSV[7])

			############# Additional Losses due to MPPT and ohm-losses in cables (1.5mmsq, at 7A peak current, 8m cable length).
			addLosses=mppt_eff/100.*(1.-cable_loss/100.)	
			
			############# CALCULATE ATMOSPHERIC TEMPERATURE ##########################################
			if tempprofile_flag == 1:
				temp_atmo = getStandardTemperature(getGeopotential(act_altitude))
			else:
				temp_atmo = getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			############# CSV OUTPUT ROUTINES ######################################################
			write_cells_temp.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_effi.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
			write_cells_powers.write("\n" + str(curr_time_h)+","+ str(act_altitude)+","+ str(temp_atmo-273.15)+",")
								


			############# CALCULATE ATMOSPHERIC TRANSMISSION AND  SOLAR CELL ORIENTATIONS ################################################
			if (act_altitude > -np.abs(getDepression(act_altitude))):
				atmoTransmiss = getAtmoTransmission(act_altitude,mission_data_incangle[mission_point_count],getDepression(act_altitude))
				SPS=GET_SUN_PROJ_SURF(sun_vector,5.)
				solcell_normals=SPS[10]
				solcell_count=SPS[12]
				Reflength=SPS[17]
			else:
				atmoTransmiss = 0.0
				SPS = 0.0
				Reflength=0.0

			mission_data_atmo_transmission.append(atmoTransmiss)
			
			s=(int(360/5),sol_cell_orientations)
			CELL_GROUP_TEMP = np.zeros(s)
			CELL_GROUP_EFF   = np.zeros(s)
			CELL_GROUP_POW   = np.zeros(s)
			
			
			#### DO THIS ONLY IF SUNLIGHT IS AVAILABLE #######
			if atmoTransmiss > 0:
				########### CALCULATE THE TEMPERATURE, THE EFFICIENCY AND THE SOLAR POWER FOR ALL ORIENTATIONS IN 5DEG-INCREMENTS OVER 360 DEGREES
				for count_i in range(0,360,5):
					for j in range(0,sol_cell_orientations):
						lamturb=SPS[15]
						rotated_solcel_normals = np.dot(rotation_matrix(np.array([0,0,-1]),-count_i), sum(solcell_normals[j]))	
						CosPsi = np.dot(sun_vector,rotated_solcel_normals/-np.linalg.norm(rotated_solcel_normals))
						if CosPsi >=0:
							CosPsi=CosPsi
						else:
							CosPsi=0.
						solcel_surf_transmission = cell_surf_transmission(angle_between(sun_vector,-rotated_solcel_normals))
						
						solcel_Temp = getSolCellTempEnerBal(solcel_limtemp_alt,solcel_limtemp_mintemp,solcel_limtemp_maxtemp,solcell_nom_eff,eff_grad,lamturb[j],CosPsi,temp_atmo, RE, act_altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,Reflength[j])
						CELL_GROUP_TEMP[int(count_i/5),j] = solcel_Temp-273.15
						solcel_effi = getEfficiencySolarcells(solcel_Temp,solcell_nom_eff,eff_grad)/100.
						CELL_GROUP_EFF[int(count_i/5),j] = solcel_effi
						sol_pow_inc = np.linalg.norm(rotated_solcel_normals)*CosPsi*solPowDens*atmoTransmiss*solcel_surf_transmission*addLosses*solcel_effi
						cutoff_power_per_direction = solcell_count[j] * mppt_cutoff_power_per_cell
						
						### CHECK IF THE SOLAR CELLS ON EACH STRING DELIVER ENOUGH POWER TO BE ABOVE THE MPPT THREASHOLD ##########
						if sol_pow_inc > cutoff_power_per_direction:
							CELL_GROUP_POW[int(count_i/5)][j] = sol_pow_inc
						else:
							CELL_GROUP_POW[int(count_i/5)][j] = 0.0
			else:
				CELL_GROUP_TEMP[:,:] =	temp_atmo-273.15
				CELL_GROUP_EFF[:,:] = getEfficiencySolarcells(temp_atmo,solcell_nom_eff,eff_grad)/100.
				CELL_GROUP_POW[:,:] = 0.0
				best_head = 0.0	

			#### ACCUMULATE OVER 360DEG ####################################
			solpow_total=[]
			for count_i in range(0,360,5):
				sum_solpow = 0.0
				for j in range(0,sol_cell_orientations):	
					sum_solpow = sum_solpow + CELL_GROUP_POW[int(count_i/5)][j]
				solpow_total.append(sum_solpow)
			
			#### MAXIMUM SOLAR POWER CONDITION ###################################	
			best_head = 5*np.argmax(solpow_total)
			best_solpow = np.max(solpow_total)
			
			#### AVERAGE SOLAR POWER CONDITION ###################################
			average_solpow = np.average(solpow_total)
			
			#### MINIMUM SOLAR POWER CONDITION ###################################
			min_solpow = np.min(solpow_total)
			min_head = 5*np.argmin(solpow_total)
			
			#### WRITE TO FILE THE BEST HEADING, THE BEST SOLAR POWER, THE AVERAGE SOLAR POWER, THE MINIMUM-SOL-POW HEADING AND THE MINIMUM SOLAR POWER ##################
			write_cells_powers.write(str(best_head)+","+str(best_solpow)+","+str(average_solpow)+","+str(min_head)+","+str(min_solpow)+",")
			
			#### CALCULATE EXTREMA FOR TEMPERATURES AND EFFICIENCIES OVER ALL ORIENTATIONS AND ALL CELL GROUPS TOGETHER ############################
			MinCelTemp = np.amin(CELL_GROUP_TEMP)
			MaxCelTemp = np.amax(CELL_GROUP_TEMP)
			AvgCelTemp = np.average(CELL_GROUP_TEMP)
			MinCelEffi = np.amin(CELL_GROUP_EFF)
			MaxCelEffi = np.amax(CELL_GROUP_EFF)
			AvgCelEffi = np.average(CELL_GROUP_EFF)
			
			#### WRITE EXTREMA/AVERAGE TEMP AND EFFI TO FILE ########################################################
			write_cells_temp.write(str(MaxCelTemp)+","+str(AvgCelTemp)+","+str(MinCelTemp)+",")
			write_cells_effi.write(str(MaxCelEffi)+","+str(AvgCelEffi)+","+str(MinCelEffi)+",")
			
			#### WRITE SOLPOW/TEMP/EFFI DATA FOR BEST HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(CELL_GROUP_TEMP[int(best_head/5),j])+",")
				write_cells_effi.write(str(CELL_GROUP_EFF[int(best_head/5),j])+",")
				write_cells_powers.write(str(CELL_GROUP_POW[int(best_head/5),j])+",")

			#### WRITE SOLPOW/TEMP/EFFI DATA FOR AVERAGE HEADING ###########################
			for j in range(0,sol_cell_orientations):
				write_cells_temp.write(str(np.average(CELL_GROUP_TEMP[:,j]))+",")
				write_cells_effi.write(str(np.average(CELL_GROUP_EFF[:,j]))+",")
				write_cells_powers.write(str(np.average(CELL_GROUP_POW[:,j]))+",")
			
			mission_data_cell_temp_min.append(MinCelTemp)
			mission_data_cell_temp_max.append(MaxCelTemp)
			mission_data_cell_temp_avg.append(AvgCelTemp)
			mission_data_cell_efficiency_min.append(MinCelEffi)
			mission_data_cell_efficiency_max.append(MaxCelEffi)
			mission_data_cell_efficiency_avg.append(AvgCelEffi)

			del solpow_total			
			del CELL_GROUP_TEMP
			del CELL_GROUP_EFF
			del CELL_GROUP_POW

			mission_data_sol_pow_max.append(best_solpow)
			mission_data_sol_pow_avg.append(average_solpow)
			
			############# PATH CALCULATIONS ##########################################
			mission_data_best_heading.append(best_head)
			delta_east  = np.sin(best_head*np.pi/180.)*timestep*velocity/1000.
			delta_north = np.cos(best_head*np.pi/180.)*timestep*velocity/1000.
			mission_data_distance_east.append(mission_data_distance_east[mission_point_count]+delta_east)
			mission_data_distance_north.append(mission_data_distance_north[mission_point_count]+delta_north)
			
			if heading_strategy == 1:
				mission_data_sol_pow.append(best_solpow)
			if heading_strategy == 2:
				mission_data_sol_pow.append(average_solpow)
			if heading_strategy < 1 or heading_strategy > 2:
				print("################ NON EXISTANT HEADING STRATEGY SPECIFIED ###############")
				exit()

			##### Current Increment of Solar Energy #########################	
			if(mission_point_count>0):
				delta_Solar_Energy=(mission_data_sol_pow[mission_point_count-1]+mission_data_sol_pow[mission_point_count])/2.0*timestep/3600.
			else:
				delta_Solar_Energy=0.0
				
			##### Total Accumulated Solar Energy ############################
			total_Solar_Energy=total_Solar_Energy+delta_Solar_Energy
			
		
			
			
			mission_data_harvested_Sol_Energy.append(total_Solar_Energy)		

			### No Charging Losses Apply ####
			delta_energy_chargloss_chem = 0.
			mission_data_PL_BATT_CHARGE_CHEM.append(0.0)
			mission_data_PL_BATT_CHARGE_OHM.append(0.0)
			mission_data_batt_charging_power.append(0.0)
			mission_data_batt_charging_current.append(0.0)
			mission_data_excess_solar_power.append(0.0)
			mission_data_Energy_Remaining.append(remaining_Bat_Energy)			

			### Interpolate the motorized data using the current solar power minus the systems/payload drain power ######
			act_power = mission_data_sol_pow[mission_point_count] - sys_pwr_drn
						
						
						
			#print(mot_data_M_BestEff,act_altitude,act_power,redsink_M)
			#print(act_altitude,act_power)
			#exit()
			print("#######################")
			mot_data_M_act_altitude = INTERPOL_ALT_POWER(mot_data_M_BestEff,act_altitude,act_power,redsink_M)[0]		
			stopflag=INTERPOL_ALT_POWER(mot_data_M_BestEff,act_altitude,act_power,redsink_M)[1]
			
			####### STORING MISSION VARIABLES ###################################################################
			velocity=float(mot_data_M_act_altitude[1])

			mission_data_mach.append(GET_MACH(act_altitude,velocity))
			act_climbrate=float(mot_data_M_act_altitude[2])
			mission_data_velocity.append(velocity)		
			mission_data_climbrate.append(act_climbrate)
			mission_data_DBeta.append(float(round(mot_data_M_act_altitude[3],2)))
			Prop_Eff_TMP=float(mot_data_M_act_altitude[4])
			mission_data_Efficiency_prop.append(Prop_Eff_TMP)
			mission_data_RPM.append(float(mot_data_M_act_altitude[7]))
			mission_data_Pprop.append(float(mot_data_M_act_altitude[8]))
			mission_data_Pshaft.append(float(mot_data_M_act_altitude[9]))
			
			mission_data_Thrust.append(float(mot_data_M_act_altitude[11]))
			mission_data_Q.append(float(mot_data_M_act_altitude[12]))
			
			if tempprofile_flag == 1:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getStandardTemperature(getGeopotential(act_altitude))
			else:
				density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
			
			mission_data_density.append(density)
			CL_tmp=2*aircraft_mass*9.81/density/velocity/velocity/AircraftReferenceArea
			mission_data_CL.append(CL_tmp)
			mission_data_alfa.append(np.interp(CL_tmp, pol_diag_CL[0], pol_diag_alpha[0]))
			mission_data_Mot_Current.append(float(mot_data_M_act_altitude[13]))
			mission_data_maxcurr.append(max_current)
			mission_data_peakcurr.append(peak_current)

			mission_data_PL_MC.append(float(mot_data_M_act_altitude[16]))
			mission_data_PL_PROP.append(float(mot_data_M_act_altitude[17]))
			mission_data_PL_MOT_COPPER.append(float(mot_data_M_act_altitude[18]))
			mission_data_PL_MOT_BE_FE.append(float(mot_data_M_act_altitude[19]))
			mission_data_PL_MOT_TOT.append(float(mot_data_M_act_altitude[20]))
			mission_data_PL_MOT_TOT_MEAS.append(float(mot_data_M_act_altitude[21]))	
			mission_data_CURR_CONST_MEAS.append(float(mot_data_M_act_altitude[22]))

			
			
			mission_data_Ptot.append(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count])
			
			EPU_Eff_TMP=mission_data_Pprop[mission_point_count] /(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count])
			mission_data_Efficiency_EPU.append(EPU_Eff_TMP)
			
			if Prop_Eff_TMP > 0.:
				mission_data_Efficiency_MOT_MC.append(EPU_Eff_TMP/Prop_Eff_TMP)
			else:
				mission_data_Efficiency_MOT_MC.append(0.0)
				
			
			eff_tot_tmp=mission_data_climbrate[mission_point_count] * 9.81 * aircraft_mass/(mission_data_Pshaft[mission_point_count] + mission_data_PL_MOT_TOT[mission_point_count] + mission_data_PL_MC[mission_point_count] + mission_data_PL_MOT_TOT_MEAS[mission_point_count])
			
			if eff_tot_tmp > 0:
				mission_data_Efficiency_TOT.append(eff_tot_tmp)	
			else:
				mission_data_Efficiency_TOT.append(0.0)	
			
			mission_data_Bat_Current.append(0.0)
			mission_data_PL_Bat.append(0.0)	
			mission_data_PBat_int.append(0.0)
			mission_data_PBat_ex.append(0.0)		
			

			if bat_type == "14S_20P_3500_LiIo":
				Bat_Volt = GET_BAT_VOLTAGE_14S_20P_3500_LiIo(0.,current_capacity,number_of_batteries,single_bat_cap)
			if bat_type == "52V":
				Bat_Volt = GET_BAT_VOLTAGE_52V(0.,current_capacity,number_of_batteries,single_bat_cap)
				
				
			
			mission_data_Bat_Voltage.append(Bat_Volt)

			### Energy Consumption ###
			if(mission_point_count>0):
				delta_Energy_inc=mission_data_Ptot[mission_point_count]*timestep/3600. + sys_pwr_drn*timestep/3600.
			else:
				delta_Energy_inc=0.
			total_Energy_spent=total_Energy_spent+delta_Energy_inc
			mission_data_Energy_Spent.append(total_Energy_spent)
			
			if (stopflag == 1):
				print("FLY WITH SUN aborted at ",curr_time_h,"hrs in ",act_altitude," m MSL due to solar power dropping below 10% of minimum hover power")
				
			if(mission_time < phase_end_time and stopflag == 0):
				check=1
			else:
				check=0
			
			act_altitude=act_altitude+timestep*act_climbrate
			
			x=[]
			y=[]
			x.append(altitude1[i])
			x.append(altitude2[i])
			y.append(climbrate1[i])
			y.append(climbrate2[i])
			act_climbrate= np.interp(act_altitude,x,y)
			del(x)
			del(y)
			mission_time=mission_time+timestep
			mission_point_count=mission_point_count+1
		
		print("     #### FLY WITH SUN PHASE ENDED, ACTUAL TIME = ",takeoff_time+mission_time/3600.,"hrs, ACTUAL ALTITUDE= ",act_altitude,"m\n")




	###########################################################################################################################################################
	###########################################################################################################################################################
	###                                                                                                                                                     ###
	###                                                      PRINT CURRENT SUN POWER                                                                        ###
	###                                                                                                                                                     ###
	###########################################################################################################################################################
	###########################################################################################################################################################

	
	if(mission_phase_type[i] == 'SUN_POW_ALT_VELO'): ### SUN_POW_ALT_VELO #############################################################################################################################################################
		
		try:
			os.system("rm -rf OUTPUTFILES_PRINT_CURRENT_SUNPOWER")
		except:
			print()
		
		print("\n################## ONLY CALCULATE THE CURRENT SOLAR POWER, NO SIMULATION IS PERFORMED ################## \n")				
		act_altitude=altitude1[0]
		curr_time_h=takeoff_time
		if tempprofile_flag == 1:
			density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getStandardTemperature(getGeopotential(act_altitude))
		else:
			density = 0.0289644*getStandardPressure(float(act_altitude))/8.3144598/getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
		CL_tmp=2*aircraft_mass*9.81/density/velocity/velocity/AircraftReferenceArea
		alfa=np.interp(CL_tmp, pol_diag_CL[0], pol_diag_alpha[0])
		RE = GET_REYNOLDS(act_altitude,velocity,ReynoldsReferenceLength)	
				
		
		### Solar Energy Generation ###
		solPowDens=getSolarPowerDensity(day_of_year)
		OUT_GSV=GET_SUNVECTOR(day_of_year,curr_time_h+sunculm_delta_time_h,latitude)
		sun_vector=[OUT_GSV[5],OUT_GSV[6],OUT_GSV[7]]		

		############# STORING RESULTS IN VARIABLES FOR OUTPUT IN CSV FILE #######################################################
		incangle=OUT_GSV[0]			   
		hour_angle=OUT_GSV[1]
		declination_angle=OUT_GSV[2]
		zenith_angle=OUT_GSV[3]
		azimuth_angle=OUT_GSV[4]
		sunvector_x=OUT_GSV[5]
		sunvector_y=OUT_GSV[6]
		sunvector_z=OUT_GSV[7]
		
		
		############# Additional Losses due to MPPT and ohm-losses in cables (1.5mmsq, at 7A peak current, 8m cable length).
		addLosses=mppt_eff/100.*(1.-cable_loss/100.)	
		
		############# CALCULATE ATMOSPHERIC TEMPERATURE ##########################################
		if tempprofile_flag == 1:
			temp_atmo = getStandardTemperature(getGeopotential(act_altitude))
		else:
			temp_atmo = getUserDefTemperature_CELSin_KELout(float(act_altitude),atmo_temp_altitude,atmo_temp_temp)
		


		############# CALCULATE ATMOSPHERIC TRANSMISSION AND  SOLAR CELL ORIENTATIONS ################################################
		if (act_altitude > -np.abs(getDepression(act_altitude))):
			atmoTransmiss = getAtmoTransmission(act_altitude,incangle,getDepression(act_altitude))
			SPS=GET_SUN_PROJ_SURF(sun_vector,alfa)
			solcell_normals=SPS[10]
			solcell_count=SPS[12]
			Reflength=SPS[17]
		else:
			atmoTransmiss = 0.0
			SPS = 0.0
			Reflength=0.0

		
		s=(int(360/5),sol_cell_orientations)
		CELL_GROUP_TEMP = np.zeros(s)
		CELL_GROUP_EFF   = np.zeros(s)
		CELL_GROUP_POW   = np.zeros(s)
		
		write_cells_temp_SPAVL.write("\n")
		write_cells_effi_SPAVL.write("\n")
		write_cells_powers_SPAVL.write("\n")
		
		#### DO THIS ONLY IF SUNLIGHT IS AVAILABLE #######
		if atmoTransmiss > 0:
			########### CALCULATE THE TEMPERATURE, THE EFFICIENCY AND THE SOLAR POWER FOR ALL ORIENTATIONS IN 5DEG-INCREMENTS OVER 360 DEGREES
			for count_i in range(0,360,5):
				#### WRITE TO FILE ########################################################
				write_cells_temp_SPAVL.write(str(count_i)+",")
				write_cells_effi_SPAVL.write(str(count_i)+",")
				write_cells_powers_SPAVL.write(str(count_i)+",")				
				accumu_solpower = 0.0
				for j in range(0,sol_cell_orientations):
					lamturb=SPS[15]
					rotated_solcel_normals = np.dot(rotation_matrix(np.array([0,0,-1]),-count_i), sum(solcell_normals[j]))	
					CosPsi = np.dot(sun_vector,rotated_solcel_normals/-np.linalg.norm(rotated_solcel_normals))
					if CosPsi >=0:
						CosPsi=CosPsi
					else:
						CosPsi=0.
					solcel_surf_transmission = cell_surf_transmission(angle_between(sun_vector,-rotated_solcel_normals))
					
					solcel_Temp = getSolCellTempEnerBal(solcel_limtemp_alt,solcel_limtemp_mintemp,solcel_limtemp_maxtemp,solcell_nom_eff,eff_grad,lamturb[j],CosPsi,temp_atmo, RE, act_altitude, atmoTransmiss, solPowDens, solcel_surf_transmission,j,Reflength[j])
					CELL_GROUP_TEMP[int(count_i/5),j] = solcel_Temp-273.15
					solcel_effi = getEfficiencySolarcells(solcel_Temp,solcell_nom_eff,eff_grad)/100.
					CELL_GROUP_EFF[int(count_i/5),j] = solcel_effi
					sol_pow_inc = np.linalg.norm(rotated_solcel_normals)*CosPsi*solPowDens*atmoTransmiss*solcel_surf_transmission*addLosses*solcel_effi
					cutoff_power_per_direction = solcell_count[j] * mppt_cutoff_power_per_cell
					
					### CHECK IF THE SOLAR CELLS ON EACH STRING DELIVER ENOUGH POWER TO BE ABOVE THE MPPT THREASHOLD ##########
					if sol_pow_inc > cutoff_power_per_direction:
						CELL_GROUP_POW[int(count_i/5)][j] = sol_pow_inc
						accumu_solpower = accumu_solpower + sol_pow_inc
					else:
						CELL_GROUP_POW[int(count_i/5)][j] = 0.0
						accumu_solpower = accumu_solpower + 0.0
							
					#### WRITE TO FILE ########################################################
					write_cells_temp_SPAVL.write(str(solcel_Temp-273.15)+",")
					write_cells_effi_SPAVL.write(str(solcel_effi)+",")
					write_cells_powers_SPAVL.write(str(sol_pow_inc)+",")
				
				write_cells_powers_SPAVL.write(str(accumu_solpower)+",")
				write_cells_temp_SPAVL.write("\n")
				write_cells_effi_SPAVL.write("\n")
				write_cells_powers_SPAVL.write("\n")
		
		
		else:
			CELL_GROUP_TEMP[:,:] =	temp_atmo-273.15
			CELL_GROUP_EFF[:,:] = getEfficiencySolarcells(temp_atmo,solcell_nom_eff,eff_grad)/100.
			CELL_GROUP_POW[:,:] = 0.0
			best_head = 0.0	

		#### ACCUMULATE OVER 360DEG ####################################
		solpow_total=[]
		for count_i in range(0,360,5):
			sum_solpow = 0.0
			for j in range(0,sol_cell_orientations):	
				sum_solpow = sum_solpow + CELL_GROUP_POW[int(count_i/5)][j]
			solpow_total.append(sum_solpow)
		
		#### MAXIMUM SOLAR POWER CONDITION ###################################	
		best_head = 5*np.argmax(solpow_total)
		best_solpow = np.max(solpow_total)
		
		#### AVERAGE SOLAR POWER CONDITION ###################################
		average_solpow = np.average(solpow_total)
		
		#### MINIMUM SOLAR POWER CONDITION ###################################
		min_solpow = np.min(solpow_total)
		min_head = 5*np.argmin(solpow_total)
		

		
		#### CALCULATE EXTREMA FOR TEMPERATURES AND EFFICIENCIES OVER ALL ORIENTATIONS AND ALL CELL GROUPS TOGETHER ############################
		MinCelTemp = np.amin(CELL_GROUP_TEMP)
		MaxCelTemp = np.amax(CELL_GROUP_TEMP)
		AvgCelTemp = np.average(CELL_GROUP_TEMP)
		MinCelEffi = np.amin(CELL_GROUP_EFF)
		MaxCelEffi = np.amax(CELL_GROUP_EFF)
		AvgCelEffi = np.average(CELL_GROUP_EFF)
		

		
		#### WRITE SOLPOW/TEMP/EFFI DATA FOR BEST HEADING ###########################
		for j in range(0,sol_cell_orientations):
			write_cells_temp.write(str(CELL_GROUP_TEMP[int(best_head/5),j])+",")
			write_cells_effi.write(str(CELL_GROUP_EFF[int(best_head/5),j])+",")
			write_cells_powers.write(str(CELL_GROUP_POW[int(best_head/5),j])+",")

		#### WRITE SOLPOW/TEMP/EFFI DATA FOR AVERAGE HEADING ###########################
		for j in range(0,sol_cell_orientations):
			write_cells_temp.write(str(np.average(CELL_GROUP_TEMP[:,j]))+",")
			write_cells_effi.write(str(np.average(CELL_GROUP_EFF[:,j]))+",")
			write_cells_powers.write(str(np.average(CELL_GROUP_POW[:,j]))+",")


		##### Current Increment of Solar Energy #########################	
		if(mission_point_count>0):
			delta_Solar_Energy=(mission_data_sol_pow[mission_point_count-1]+mission_data_sol_pow[mission_point_count])/2.0*timestep/3600.
		else:
			delta_Solar_Energy=0.0
			
		##### Total Accumulated Solar Energy ############################
		total_Solar_Energy=total_Solar_Energy+delta_Solar_Energy		
		
		print("\nSun Incidence Angle [deg]:__________",incangle)		   
		print("Hour Angle [deg]:___________________", hour_angle)
		print("Declination Angle [deg]:____________", declination_angle)
		print("Zenith Angle [deg]:_________________", zenith_angle)
		print("Azimuth Angle [deg]:________________", azimuth_angle)
		print("Sunvector:__________________________", sunvector_x,sunvector_y,sunvector_z)
		print("Atmospheric Transmission:___________", atmoTransmiss)
		print("Extraterr. Sol. Pow. Dens. [W/sqm]:_", solPowDens)
		print("Altitude [m]:_______________________", act_altitude)
		print("Velocity [m/s]:_____________________", velocity)
		print("Day of Year:________________________", day_of_year)
		print("Latitude [deg]:_____________________", latitude)
		print("MPPT Efficiency [perc]:_____________", mppt_eff)
		print("MPPT Cutoff Pow. per Cell [W]:______", mppt_cutoff_power_per_cell)
		print("Solcell Eff, Grad. [Perc,Perc/deg]:_", solcell_nom_eff, eff_grad)
		print("Current Time [h]:___________________", curr_time_h)
		print("Cable Loss. [Perc.]:________________", cable_loss)
		print("AOA [deg]:__________________________", alfa)
		print("CL [deg]:___________________________", CL_tmp)
		print("Solar Power at best Heading [W]:____", best_solpow)
		print("Best Heading [deg]:_________________", best_head)
		print("Average Solar Power [W]:____________", average_solpow)
		print("Solar Power at worst Heading [W]:___", min_solpow)
		print("Worst Heading [deg]:________________", min_head)
		print("Minimum Solarcell Temp [degC]_______", MinCelTemp)
		print("Maximum Solarcell Temp [degC]_______", MaxCelTemp)
		print("Average Solarcell Temp [degC]_______", AvgCelTemp)
		print("Minimum Solarcell Efficiency [%]____", MinCelEffi*100)
		print("Maximum Solarcell Efficiency [%]____", MaxCelEffi*100)
		print("Average Solarcell Efficiency [%]____", AvgCelEffi*100)
		
		output_dir_solpow = str("OUT_SOLPOWER_Alt=")+str(act_altitude)+str("_Vel=")+str(velocity)+str("_DOY=")+str(day_of_year)+str("_Time=")+str(curr_time_h)+str("_Lat=")+str(latitude)
		command_output_dir = str("mkdir ")+output_dir_solpow
		os.system(command_output_dir)
		mv_command = str("mv sol_cells_surf_transmission.csv sol_cells_mppt_cutoff.csv sol_cells_effi_spavl.csv sol_cells_powers_spavl.csv sol_cells_temp_spavl.csv ")+str(output_dir_solpow)
		cp_command = str("cp /home/sven/Python/Solar_Calculator/vectors.py /home/sven/Python/Solar_Calculator/functions.py /home/sven/Python/Solar_Calculator/solarcalc.py input_solar.inp solsyst.def ")+str(output_dir_solpow)
		os.system(mv_command)
		os.system(cp_command)
		os.system("rm sol_cells_temp.csv sol_cells_effi.csv sol_cells_powers.csv")
		
		del solpow_total			
		del CELL_GROUP_TEMP
		del CELL_GROUP_EFF
		del CELL_GROUP_POW	
		
		write_cells_temp.close()
		write_cells_powers.close()
		write_cells_effi.close()
		exit()


### Calculate the CAS based on TAS and Altitude #############################
for i in range(0,len(mission_data_velocity)):
	mission_data_velocity_CAS.append(TAS2CAS_mps(mission_data_altitude[i],mission_data_velocity[i]))
	mission_data_RPM_MAX.append(GET_MAXRPM(mission_data_Pshaft[i],mission_data_Bat_Voltage[i]))


if tempprofile_flag == 1:
	tempprof_string = str(", Std.-Atmo. Temp.")

elif tempprofile_flag == 2:
	tempprof_string = str(", User-Def. Temp.")
else:
	print("\n#### ERROR: wrong temperature profile Type Specified! ####\n")
	exit()
	

end_time=takeoff_time+mission_data_time[len(mission_data_time)-1]/3600.

figtitles="Weight: " + str(aircraft_mass) + "kg, Latitude: " + str(latitude) + "deg, Day of year: " + str(int(day_of_year)) +\
 ", Local Takeoff Time: " + str(takeoff_time)+ ", End Time: " + str(round(end_time,2)) + ", Duration: " + str(round((mission_data_time[len(mission_data_time)-1]-mission_data_time[0])/3600.,3))+ " h, Heading Strategy (1-opt., 2-arb.): " + str(int(heading_strategy)) +\
  ", Horiz. Proj. Sol. Area at zero AOA: " + str(round(sol_area_horiz,2))+" sqm, \nTot. Sol. Cell Surf. Area: " + str(round(total_sol_surf_area,2)) + " sqm, Tot. Number of Sol. Cells: " + str(num_sol_cell) + ", Sol. Cell Eff. @ 25 deg C: " +str(solcell_nom_eff) + "%, tot. Batt. Cap.: " + str(bat_cap) + "Wh, MPPT Eff.: " + str(mppt_eff) + "%, MPPT Cutoff Power per Cell.: " + str(mppt_cutoff_power_per_cell) + "W, Chem. Batt. Charg. Loss: "\
  +str(bat_chg_red_chem)+ "%\nInt. Batt. Res.: " + str(battery_int_res)+ " Ohm, Number of Batt.: " + str(number_of_batteries) + ", Cable Loss: " + str(cable_loss) + "%, Sys. & Payload Pwr.: " + str(sys_pwr_drn) + "W, Sol. Cell Eff. Incr.: " + str(eff_grad) + "per K, Ref. Length: " + str(ReynoldsReferenceLength) + "m, Bat. Heating const: " + str(bat_heat_const)+", Timestep: " + str(timestep) + " [s]"\
 + ", Calc. Type: " + str(calc_type) + tempprof_string

### Calculation of battery heating ########################################

### STEP 1 - calculate deltas 
for i in range(0,len(mission_data_real_time_h)):
	mission_data_batt_heatup_delta.append((float(mission_data_Bat_Current[i])/float(number_of_batteries))**2 * float(timestep) * float(bat_heat_const))
	mission_data_batt_heatup.append(0.0)

### STEP 2 - calculate absolute 
for i in range(1,len(mission_data_real_time_h)):
	mission_data_batt_heatup[i] = mission_data_batt_heatup[i-1] + mission_data_batt_heatup_delta[i]
	

if tempprofile_flag == 2:
	densitycurve_real.append(0.0289644*getStandardPressure(atmo_temp_altitude[0])/8.3144598/getUserDefTemperature_CELSin_KELout(atmo_temp_altitude[0],atmo_temp_altitude,atmo_temp_temp))
	altcurve_real.append(atmo_temp_altitude[0])
	
### Calculation of atmospheric data and efficiency curve ########################################
for i in range(0,21):
	
	altcurve.append(float(i*1000))
	presscurve.append(getStandardPressure(float(i*1000))/100.)
	presscurve_proc.append(getStandardPressure(float(i*1000))/getStandardPressure(0)*100.)
	tempcurve.append(getStandardTemperature(float(i*1000))-273.15)
	
	densitycurve.append(0.0289644*getStandardPressure(float(i*1000))/8.3144598/getStandardTemperature(float(i*1000)))
	if tempprofile_flag == 2:
		if(float(i*1000) > np.min(atmo_temp_altitude)):
			densitycurve_real.append(0.0289644*getStandardPressure(float(i*1000))/8.3144598/getUserDefTemperature_CELSin_KELout(float(i*1000),atmo_temp_altitude,atmo_temp_temp))
			altcurve_real.append(float(i*1000))
	densitycurve_proc.append(0.0289644*getStandardPressure(float(i*1000))/8.3144598/getStandardTemperature(float(i*1000))/1.225*100.0)
	#print(float(i*1000),getUserDefTemperature_CELSin_KELout(float(i*1000),atmo_temp_altitude,atmo_temp_temp))

plot_skip = int(len(mission_data_real_time_h)/max_plotted_points)
if plot_skip < 1:
	plot_skip = 1

print("\n#### Plot-Skip: ",plot_skip,", Number of plotted Points:",int(len(mission_data_real_time_h)/plot_skip)," ####\n")


#############################################################
#############################################################
##### FIRST FIGURE ##########################################
#############################################################
#############################################################
if p1==1:		
	plot_1 = plt.figure(1, figsize=(20,15))
	plot_1.canvas.set_window_title('PLOT 1 - TAS/CAS, CLIMBRATE')
	plot_1.suptitle(figtitles, fontsize=12)	

	##### VELOCITY AND ALTITUDE OVER TIME #########################################
	ax018=plot_1.add_subplot(2, 1, 1)
	ax018.plot(mission_data_real_time_h[::plot_skip],mission_data_velocity[::plot_skip],'-',label="Velocity (TAS)",color="red")
	ax018.plot(mission_data_real_time_h[::plot_skip],mission_data_velocity_CAS[::plot_skip],'-',label="Velocity (CAS)",color="green")
	ax019 = ax018.twinx()
	ax019.format_coord = make_format(ax019, ax018)
	ax019.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax018.set_xlabel('Time [h]',fontsize=10)
	ax018.set_ylabel('Velocity [m/s]',fontsize=10,color="red")
	ax019.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax018.grid(b=True,which='both',color='r',linestyle='--')
	ax019.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Velocity and Altitude over Time')
	ax018.tick_params(axis='y', colors='red')
	ax019.tick_params(axis='y', colors='blue')
	lines, labels = ax018.get_legend_handles_labels()
	lines2, labels2 = ax019.get_legend_handles_labels()
	ax019.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### CLIMBRATE AND ALTITUDE OVER TIME #########################################
	ax0180=plot_1.add_subplot(2, 1, 2)
	plt.margins(x=0.1, y=0.1)
	ax0180.plot(mission_data_real_time_h[::plot_skip],mission_data_climbrate[::plot_skip],'-',label="Climbrate",color="red")
	ax0190 = ax0180.twinx()
	ax0190.format_coord = make_format(ax0190, ax0180)
	ax0190.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax0180.set_xlabel('Time [h]',fontsize=10)
	ax0180.set_ylabel('Climbrate [m/s]',fontsize=10,color="red")
	ax0190.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax0180.grid(b=True,which='both',color='r',linestyle='--')
	ax0190.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Climbrate and Altitude over Time')
	ax0180.tick_params(axis='y', colors='red')
	ax0190.tick_params(axis='y', colors='blue')
	lines, labels = ax0180.get_legend_handles_labels()
	lines2, labels2 = ax0190.get_legend_handles_labels()
	ax0190.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	plot_1.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)

	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot1-Velo-Climbrate.png')


#############################################################
#############################################################
##### SECOND FIGURE #########################################
#############################################################
#############################################################
if p2==1:
	plot_2 = plt.figure(2, figsize=(20,15))
	plot_2.canvas.set_window_title('PLOT 2 - EFFICIENCY, POWER')
	plot_2.suptitle(figtitles, fontsize=12)

	##### EFFICIENCY OVER TIME #########################################
	ax0181=plot_2.add_subplot(2, 1, 1)
	ax0181.plot(mission_data_real_time_h[::plot_skip],mission_data_Efficiency_TOT[::plot_skip],'-',label="Total Climb Efficiency",color="red")
	ax0181.plot(mission_data_real_time_h[::plot_skip],mission_data_Efficiency_EPU[::plot_skip],'-',label="EPU Efficiency",color="orange")
	ax0181.plot(mission_data_real_time_h[::plot_skip],mission_data_Efficiency_prop[::plot_skip],'-',label="Prop. Efficiency",color="green")
	ax0181.plot(mission_data_real_time_h[::plot_skip],mission_data_Efficiency_MOT_MC[::plot_skip],'-',label="Mot. & MC Efficiency",color="magenta")

	plt.margins(x=0.1, y=0.1)
	ax0191 = ax0181.twinx()
	ax0191.format_coord = make_format(ax0191, ax0181)
	ax0191.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax0181.set_xlabel('Time [h]',fontsize=10)
	ax0181.set_ylabel('Efficiency [%]',fontsize=10,color="red")
	ax0191.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax0181.grid(b=True,which='both',color='r',linestyle='--')
	ax0191.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Efficiency and Altitude over Time')
	ax0181.tick_params(axis='y', colors='red')
	ax0191.tick_params(axis='y', colors='blue')
	lines, labels = ax0181.get_legend_handles_labels()
	lines2, labels2 = ax0191.get_legend_handles_labels()
	ax0191.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### POWER OVER TIME #########################################
	ax0182=plot_2.add_subplot(2, 1, 2)
	ax0182.plot(mission_data_real_time_h[::plot_skip],mission_data_Ptot[::plot_skip],'-',label="Ptot",color="red")
	ax0182.plot(mission_data_real_time_h[::plot_skip],mission_data_Pshaft[::plot_skip],'-',label="Pshaft",color="orange")
	ax0182.plot(mission_data_real_time_h[::plot_skip],mission_data_Pprop[::plot_skip],'-',label="Pprop",color="green")
	ax0182.plot(mission_data_real_time_h[::plot_skip],mission_data_PBat_ex[::plot_skip],'-',label="PBat_ex",color="purple")
	plt.margins(x=0.1, y=0.1)
	ax0192 = ax0182.twinx()
	ax0192.format_coord = make_format(ax0192, ax0182)
	ax0192.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax0182.set_xlabel('Time [h]',fontsize=10)
	ax0182.set_ylabel('Power [W]',fontsize=10,color="red")
	ax0192.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax0182.grid(b=True,which='both',color='r',linestyle='--')
	ax0192.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Power and Altitude over Time')
	ax0182.tick_params(axis='y', colors='red')
	ax0192.tick_params(axis='y', colors='blue')
	lines, labels = ax0182.get_legend_handles_labels()
	lines2, labels2 = ax0192.get_legend_handles_labels()
	ax0192.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	plot_2.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot2-Efficiency-Power.png')


#############################################################
#############################################################
##### THIRD FIGURE ##########################################
#############################################################
#############################################################
if p3==1:
	for i in range(0,365):
		dayofyearcurve.append(i)
		solarenergydensitycurve.append(getSolarPowerDensity(i))
	
	for i in range((int(takeoff_time)-4)*100,(int(end_time)+4)*100):
		timeofdaycurve.append(float(i)/100)
		cospsicurve.append(getCosPsi(float(day_of_year),float(i)/100,float(latitude)))
		incanglecurve.append(90.-np.arccos(getCosPsi(float(day_of_year),float(i)/100,float(latitude)))*180./np.pi)

	plot_3 = plt.figure(3, figsize=(20,15))
	plot_3.canvas.set_window_title('PLOT 3 - SOLCEL EFFICIENCY/POWER')
	plot_3.suptitle(figtitles, fontsize=12)
	
	##### SOLAR CELLS EFFICIENCY AND ALTITUDE OVER TIME  #########################################
	ax13=plot_3.add_subplot(2, 2, 1)
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_efficiency_max[::plot_skip],'-',label="Max. Effi. Solar Cells",color="red")
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_efficiency_min[::plot_skip],'-',mfc='none',label="Min. Effi. Solar Cells",color="green")
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_efficiency_avg[::plot_skip],'-',mfc='none',label="Avg. Effi. Solar Cells",color="orange")
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_efficiency_individual[::plot_skip][:],'.',mfc='none',color="grey",linewidth=0.1)
	ax133 = ax13.twinx()
	ax133.format_coord = make_format(ax133, ax13)
	ax133.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax13.tick_params(axis='y', colors='red')
	ax133.tick_params(axis='y', colors='blue')
	ax13.set_xlabel('Time [h]', color='k',fontsize=10)
	ax13.set_ylabel('Solar Cells Efficiency [%]', color='red',fontsize=10)
	ax133.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax13.grid(b=True,which='both',color='r',linestyle='--')
	ax133.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Solar Cells Efficiency and Altitude over Time')
	lines, labels = ax13.get_legend_handles_labels()
	lines2, labels2 = ax133.get_legend_handles_labels()
	ax133.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	
	##### SOLAR CELLS TEMPERATURE AND ALTITUDE OVER TIME  #########################################
	ax3=plot_3.add_subplot(2, 2, 3)
	ax3.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_temp_max[::plot_skip],'-',label="Max. T. Solar Cells",color="red")
	ax3.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_temp_min[::plot_skip],'-',mfc='none',label="Min. T. Solar Cells",color="green")
	ax3.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_temp_avg[::plot_skip],'-',mfc='none',label="Avg. T. Solar Cells",color="orange")
	ax3.plot(mission_data_real_time_h[::plot_skip],mission_data_cell_temp_individual[::plot_skip][:],'.',mfc='none',color="grey",linewidth=0.1)
	ax33 = ax3.twinx()
	ax33.format_coord = make_format(ax33, ax3)
	ax33.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax3.tick_params(axis='y', colors='red')
	ax33.tick_params(axis='y', colors='blue')
	ax3.set_xlabel('Time [h]', color='k',fontsize=10)
	ax3.set_ylabel('Solar Cells Temperature [deg C]', color='red',fontsize=10)
	ax33.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax3.grid(b=True,which='both',color='r',linestyle='--')
	ax33.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Solar Cells Temperature and Altitude over Time')
	lines, labels = ax3.get_legend_handles_labels()
	lines2, labels2 = ax33.get_legend_handles_labels()
	ax33.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	
	
	##### SOLAR INTENSITY OVER TIME OF YEAR  #########################################
	ax10=plot_3.add_subplot(3, 2, 2)
	ax10.plot(dayofyearcurve,solarenergydensitycurve,'-',label="Solar Intensity",color="red")
	ax10.plot([0,365],[1367.0,1367.0],'-',label="Sol. Const. 1367 W/sqm",color="blue")
	ax10.plot([day_of_year,day_of_year],[1320.,1420.],'--',label="Actual Day of Year",color="red",linewidth=2.0)
	ax10.set_xlabel('Day of Year [-]',fontsize=10)
	ax10.set_ylabel('Solar Intensity [W/square meter]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Solar Intensity during the Year')
	lines,labels = ax10.get_legend_handles_labels()
	ax10.legend(lines, labels, loc=1, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### COSPSI AND INCIDENCEANGLE OVER TIME OF DAY  #########################################
	ax11=plot_3.add_subplot(3, 2, 4)
	ax11.plot(timeofdaycurve,cospsicurve,'-',label="cos(PSI)",color="green")
	plt.ylim([0.,1.0])
	ax12 = ax11.twinx()
	ax12.format_coord = make_format(ax12, ax11)
	ax12.plot(timeofdaycurve,incanglecurve,'-',label="Incidence Angle",color="blue")
	ax11.plot([takeoff_time,takeoff_time],[-1,1],'--',label="Takeoff Time",color="red",linewidth=2.0)
	ax11.plot([end_time,end_time],[-1,1],'--',label="End Time",color="red",linewidth=2.0)
	ax11.set_xlabel('Time of Day [h]',fontsize=10)
	ax11.set_ylabel('Cos(PSI)',fontsize=10,color="green")
	ax12.set_ylabel('Incidence Angle (deg)',fontsize=10,color="blue")
	plt.ylim([0.,90.])
	ax11.grid(b=True,which='both',color='g',linestyle='--')
	ax12.grid(b=True,which='both',color='b',linestyle='--')
	ax11.tick_params(axis='y', colors='green')
	ax12.tick_params(axis='y', colors='blue')
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Incidence Angle and cos(PSI) during the Day')
	lines, labels = ax11.get_legend_handles_labels()
	lines2, labels2 = ax12.get_legend_handles_labels()
	ax12.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	
	##### ATMOSPHERIC TRANSMISSIBILITY OVER TIME #########################################
	ax016=plot_3.add_subplot(3, 2, 6)
	ax016.plot(mission_data_real_time_h[1::plot_skip],mission_data_atmo_transmission[1::plot_skip],'-',label="Atmospheric Transmissibility",color="red")
	plt.ylim([0.,1.])
	ax017 = ax016.twinx()
	plt.margins(x=0.1, y=0.1)
	ax017.format_coord = make_format(ax017,ax016)
	ax017.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax016.set_xlabel('Time [h]',fontsize=10)
	ax016.set_ylabel('Atmospheric Transmissibility [%]',fontsize=10,color="red")
	ax017.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax016.grid(b=True,which='both',color='r',linestyle='--')
	ax017.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Atmospheric Transmissibility and Altitude over Time')
	ax016.tick_params(axis='y', colors='red')
	ax017.tick_params(axis='y', colors='blue')
	lines, labels = ax016.get_legend_handles_labels()
	lines2, labels2 = ax017.get_legend_handles_labels()
	ax017.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	plot_3.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot3-SolCel-Effi-Power.png')

#############################################################
#############################################################
##### FOURTH FIGURE #########################################
#############################################################
#############################################################
if p4==1:
	plot_4 = plt.figure(4, figsize=(20,15))
	plot_4.canvas.set_window_title('PLOT 4 - POWER/ENERGY')
	plot_4.suptitle(figtitles, fontsize=12)

	##### ACCUMULATED ENERGY OVER TIME  #########################################
	ax13=plot_4.add_subplot(2, 2, 3)
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_Energy_Spent[::plot_skip],'-',label="Consumed Energy",color="red")
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_harvested_Sol_Energy[::plot_skip],'-',label="Harvested Solar Energy",color="orange")
	ax133 = ax13.twinx()
	ax133.format_coord = make_format(ax133, ax13)
	ax133.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax13.tick_params(axis='y', colors='red')
	ax133.tick_params(axis='y', colors='blue')
	ax13.set_xlabel('Time [h]', color='k',fontsize=10)
	ax13.set_ylabel('Energy [Wh]', color='red',fontsize=10)
	ax133.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax13.grid(b=True,which='both',color='r',linestyle='--')
	ax133.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Energy and Altitude over Time')
	lines, labels = ax13.get_legend_handles_labels()
	lines2, labels2 = ax133.get_legend_handles_labels()
	ax133.legend(lines + lines2, labels + labels2, loc=2, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	
	
	##### ENERGY, POWER AND ALTITUDE OVER TIME  #########################################
	ax13=plot_4.add_subplot(2, 1, 1)
	#ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_Potential_Energy[::plot_skip],'--',label="Potential Energy",color="grey",linewidth=2.0)
	##########ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_Energy_Remaining[::plot_skip],'-',label="El. Energy in Batteries",color="green")
	ax13.plot(mission_data_real_time_h[::plot_skip],mission_data_Energy_Remaining[::plot_skip],'-',label="El. Energy in Batteries",color="green")
	plt.margins(x=0.1, y=0.1)
	ax133 = ax13.twinx()
	ax133.format_coord = make_format(ax133, ax13)
	ax133.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude",color="blue")
	
	if heading_strategy ==1:
		ax133.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_max[1::plot_skip],'-',label="Used Sol. Pow. (losses cons.), max.",color="orange",linewidth=2.0)
		ax133.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_avg[1::plot_skip],'--',label="Reference Sol. Pow. (losses cons.), avg.",color="orange",linewidth=2.0)
	else:
		ax133.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_avg[1::plot_skip],'-',label="Used Sol. Pow. (losses cons.), avg.",color="orange",linewidth=2.0)
		ax133.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_max[1::plot_skip],'--',label="Reference Sol. Pow. (losses cons.), max.",color="orange",linewidth=2.0)
	
	
	
	Ptot_plus_payload_system=[]
	for i in range (0,len(mission_data_Ptot)):
		Ptot_plus_payload_system.append(mission_data_Ptot[i] + sys_pwr_drn)	
	
	
	ax133.plot(mission_data_real_time_h[::plot_skip],mission_data_Ptot[::plot_skip],'-',label="Tot. Pow., w/o Payload Pow.",color="black",linewidth=1.0)
	ax133.plot(mission_data_real_time_h[::plot_skip],Ptot_plus_payload_system[::plot_skip],'--',label="Tot. Pow., w Payload Pow.",color="black",linewidth=1.0)
	ax13.tick_params(axis='y', colors='green')
	ax133.tick_params(axis='y', colors='black')
	ax13.set_xlabel('Time [h]', color='k',fontsize=10)
	ax13.set_ylabel('Energy [Wh]', color='green',fontsize=10)
	ax133.set_ylabel('Altitude [m], Power [W], Solar Power [W]',fontsize=10,color="black")
	ax13.grid(b=True,which='both',color='green',linestyle='--')
	ax133.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Energy, Solar Power and Altitude over Time')
	lines, labels = ax13.get_legend_handles_labels()
	lines2, labels2 = ax133.get_legend_handles_labels()
	ax133.legend(lines + lines2, labels + labels2, loc=1, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### POWER OVER TIME  #########################################
	ax15=plot_4.add_subplot(2, 2, 4)
	
	if heading_strategy ==1:
		ax15.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_max[1::plot_skip],'-',label="Used  Sol. Pow. (losses cons.), max.",color="orange",linewidth=2.0)
		ax15.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_avg[1::plot_skip],'--',label="Reference Sol. Pow. (losses cons.), avg.",color="orange",linewidth=2.0)
	else:
		ax15.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_avg[1::plot_skip],'-',label="Used Sol. Pow. (losses cons.), avg.",color="orange",linewidth=2.0)
		ax15.plot(mission_data_real_time_h[1::plot_skip],mission_data_sol_pow_max[1::plot_skip],'--',label="Reference Sol. Pow. (losses cons.), max.",color="orange",linewidth=2.0)
	
	ax15.set_xlabel('Time [h]', color='k',fontsize=10)
	ax15.set_ylabel('Solar Power [W]', color='k',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Total Solar Power over Time (Losses considered)')
	lines, labels = ax15.get_legend_handles_labels()
	ax15.legend(lines, labels, loc=2, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	plot_4.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot4-Power-Energy.png')

#############################################################
#############################################################
##### FIFTH FIGURE ##########################################
#############################################################
#############################################################
if p5==1:	
	plot_5 = plt.figure(5, figsize=(20,15))
	plot_5.canvas.set_window_title('PLOT 5 - TORQUE/THRUST')
	plot_5.suptitle(figtitles, fontsize=12)


	##### TORQUE AND ALTITUDE OVER TIME [h] #########################################
	ax22=plot_5.add_subplot(2, 1, 1)
	ax22.plot(mission_data_real_time_h[::plot_skip],mission_data_Q[::plot_skip],'-',label="Q [Nm]",color="red")
	plt.margins(x=0.1, y=0.1)
	ax23 = ax22.twinx()
	ax23.format_coord = make_format(ax23, ax22)
	ax23.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax22.set_xlabel('Time [h]',fontsize=10)
	ax22.set_ylabel('Torque [Nm]',fontsize=10,color="red")
	ax23.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax22.grid(b=True,which='both',color='r',linestyle='--')
	ax23.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Torque and Altitude over Time')
	ax22.tick_params(axis='y', colors='red')
	ax23.tick_params(axis='y', colors='blue')
	lines, labels = ax22.get_legend_handles_labels()
	lines2, labels2 = ax23.get_legend_handles_labels()
	ax23.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	plot_5.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()


	##### THRUST AND ALTITUDE OVER TIME [h] #########################################
	ax022=plot_5.add_subplot(2, 1, 2)
	ax022.plot(mission_data_real_time_h[::plot_skip],mission_data_Thrust[::plot_skip],'-',label="Thrust [N]",color="red")
	ax023 = ax022.twinx()
	ax023.format_coord = make_format(ax023, ax022)
	ax023.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax022.set_xlabel('Time [h]',fontsize=10)
	ax022.set_ylabel('Thrust [N]',fontsize=10,color="red")
	ax023.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax022.grid(b=True,which='both',color='r',linestyle='--')
	ax023.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Thrust and Altitude over Time')
	ax022.tick_params(axis='y', colors='red')
	ax023.tick_params(axis='y', colors='blue')
	lines, labels = ax022.get_legend_handles_labels()
	lines2, labels2 = ax023.get_legend_handles_labels()
	ax023.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	plot_5.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot5-Torque-Thrust.png')


#############################################################
#############################################################
##### SIXTH FIGURE ##########################################
#############################################################
#############################################################
if p6==1:		
	plot_6 = plt.figure(6, figsize=(20,15))
	plot_6.canvas.set_window_title('PLOT 6 - DBETA, CL, ALPHA')
	plot_6.suptitle(figtitles, fontsize=12)

	##### LIFT COEFFICIENT AND ALTITUDE OVER TIME  #########################################
	ax27=plot_6.add_subplot(2, 1, 1)
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_CL[::plot_skip],'-',label="CL [-]",color="red")
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_alfa[::plot_skip],'-',label="AOA [deg]",color="green")
	#plt.ylim([0.,1.2])
	ax28 = ax27.twinx()
	ax28.format_coord = make_format(ax28, ax27)
	ax28.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax27.set_xlabel('Time [h]',fontsize=10)
	ax27.set_ylabel('CL, AOA [-/deg]',fontsize=10,color="red")
	ax28.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax27.grid(b=True,which='both',color='r',linestyle='--')
	ax28.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('CL, AOA and Altitude over Time')
	ax27.tick_params(axis='y', colors='red')
	ax28.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax27.get_legend_handles_labels()
	lines2, labels2 = ax28.get_legend_handles_labels()
	ax28.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	##### DBETA AND ALTITUDE OVER TIME  #########################################
	ax29=plot_6.add_subplot(2, 1, 2)
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_DBeta[::plot_skip],'-',label="DBeta [deg]",color="red")
	ax30 = ax29.twinx()
	ax30.format_coord = make_format(ax30, ax29)
	ax30.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax29.set_xlabel('Time [h]',fontsize=10)
	ax29.set_ylabel('DBeta [deg]',fontsize=10,color="red")
	ax30.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax29.grid(b=True,which='both',color='r',linestyle='--')
	ax30.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('DBeta and Altitude over Time')
	ax29.tick_params(axis='y', colors='red')
	ax30.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax29.get_legend_handles_labels()
	lines2, labels2 = ax30.get_legend_handles_labels()
	ax30.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plot_6.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot6-DBeta-CL.png')


#############################################################
#############################################################
##### SEVENTH FIGURE #########################################
#############################################################
#############################################################

if p7==1:
	plot_7 = plt.figure(7, figsize=(20,15))
	plot_7.canvas.set_window_title('PLOT 7 - ATMOSPHERIC DATA')
	ctitle = "ATMOSPHERIC DATA, US Standard Atmosphere Version 1976"
	plot_7.suptitle(ctitle, fontsize=12)

	##### PRESSURE OVER ALTITUDE  #########################################
	ax4=plot_7.add_subplot(2, 2, 1)
	ax4.plot(altcurve,presscurve,'-',label="Pressure Std. Atmo.",color="red")
	ax4.set_xlabel('Altitude [m]',fontsize=10)
	ax4.set_ylabel('Pressure [hPa]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Pressure over Altitude')
	lines,labels = ax4.get_legend_handles_labels()
	ax4.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
		
		
	##### TEMPERATURE OVER ALTITUDE  #########################################
	ax7=plot_7.add_subplot(2, 2, 2)
	ax7.plot(altcurve,tempcurve,'-',label="Air Temperature Std. Atmo.",color="red",linewidth=2.0)
	if tempprofile_flag == 2:
		ax7.plot(atmo_temp_altitude,atmo_temp_temp,'--',label="Air Temperature User-Defined",color="red",linewidth=1.0)
	#ax7.plot(altcurve,tempcurve_cells,'--',label="Solar Cells Temperature [deg C]",color="red",linewidth=2.0)
	ax7.plot(mission_data_altitude[::plot_skip],mission_data_cell_temp_max[::plot_skip],'.',label="Max. Solcel Temperature",color="black",linewidth=3.0)
	ax7.plot(mission_data_altitude[::plot_skip],mission_data_cell_temp_min[::plot_skip],'o',mfc='none',label="Min. Solcel Temperature",color="blue",linewidth=1.0)
	
	ax7.plot(solcel_limtemp_alt,solcel_limtemp_mintemp,'--',label="User-Defined Solcel Min. Temp",color="grey",linewidth=1.0)
	ax7.plot(solcel_limtemp_alt,solcel_limtemp_maxtemp,'--',label="User-Defined Solcel Max. Temp.",color="grey",linewidth=1.0)
	
	ax7.set_xlabel('Altitude [m]', color='k',fontsize=10)
	ax7.set_ylabel('Temperature [deg C]', color='k',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Temperature over Altitude')
	plt.xlim([0.,20000.])
	lines, labels = ax7.get_legend_handles_labels()
	ax7.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)

	##### DENSITY OVER ALTITUDE  #########################################
	ax6=plot_7.add_subplot(2, 2, 3)
	ax6.plot(altcurve,densitycurve,'-',label="Density Std. Atmo.",color="red")
	if tempprofile_flag == 2:
		ax6.plot(altcurve_real,densitycurve_real,'--',label="Density User-Defined",color="red",linewidth=1.0)
	ax6.set_xlabel('Altitude [m]',fontsize=10)
	ax6.set_ylabel('Density [kg/cubic meters]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Density over Altitude')
	lines,labels = ax6.get_legend_handles_labels()
	ax6.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)


	##### ALL VARIABLES DIMENSIONLESS OVER ALTITUDE  #########################################
	ax1=plot_7.add_subplot(2, 2, 4)
	ax1.plot(altcurve,presscurve_proc,'-',label="Pressure Std. Atmo.",color="red")
	ax1.plot(altcurve,densitycurve_proc,'-',label="Density Std. Atmo.",color="blue")
	ax1.set_xlabel('Altitude [m]', color='k',fontsize=10)
	ax1.set_ylabel('[%]', color='k',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('Dimensionless Pressure and Density over Altitude')
	lines,labels = ax1.get_legend_handles_labels()
	ax1.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plot_7.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot7-Atmosphere-Data.png')


#############################################################
#############################################################
##### EIGHT FIGURE #########################################
#############################################################
#############################################################

if p8==1:
	plot_8 = plt.figure(8, figsize=(20,15))
	plot_8.canvas.set_window_title('PLOT 8 - AC POLAR DIAGRAM')
	ctitle = "Aircraft Polar Diagram"
	plot_8.suptitle(ctitle, fontsize=12)

	##### V_Sink OVER CL  #########################################
	ax4=plot_8.add_subplot(2, 2, 1)
	color_PD=iter(cm.rainbow(np.linspace(0,1,number_polars)))
	for i in range(0,number_polars):
		lab="alt= " + str(altitude_polar[i]) + " km"
		c=next(color_PD)
		ax4.plot(pol_diag_CL[i][:],pol_diag_v_sink[i][:],'-',label=lab,color=c)
	ax4.set_xlabel('CL [-]',fontsize=10)
	ax4.set_ylabel('V_sink [m/s]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('V_sink over CL')
	lines,labels = ax4.get_legend_handles_labels()
	ax4.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### CD OVER CL  #########################################
	ax4=plot_8.add_subplot(2, 2, 2)
	color_PD=iter(cm.rainbow(np.linspace(0,1,number_polars)))
	for i in range(0,number_polars):
		lab="alt= " + str(altitude_polar[i]) + " km"
		c=next(color_PD)
		ax4.plot(pol_diag_CL[i][:],pol_diag_CD[i][:],'-',label=lab,color=c)
	ax4.set_xlabel('CL [-]',fontsize=10)
	ax4.set_ylabel('CD [-]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('CD over CL')
	lines,labels = ax4.get_legend_handles_labels()
	ax4.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### CL/CD OVER QInf  #########################################
	ax4=plot_8.add_subplot(2, 2, 3)
	color_PD=iter(cm.rainbow(np.linspace(0,1,number_polars)))
	for i in range(0,number_polars):
		lab="alt= " + str(altitude_polar[i]) + " km"
		c=next(color_PD)
		ax4.plot(pol_diag_QInf[i][:],pol_diag_CL[i][:]/pol_diag_CD[i][:],'-',label=lab,color=c)
	ax4.set_xlabel('QInf [m/s]',fontsize=10)
	ax4.set_ylabel('CL/CD [-]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('CL/CD over QInf')
	lines,labels = ax4.get_legend_handles_labels()
	ax4.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	##### V_sink OVER QInf  #########################################
	ax4=plot_8.add_subplot(2, 2, 4)
	color_PD=iter(cm.rainbow(np.linspace(0,1,number_polars)))
	for i in range(0,number_polars):
		lab="alt= " + str(altitude_polar[i]) + " km"
		c=next(color_PD)
		ax4.plot(pol_diag_QInf[i][:],pol_diag_v_sink[i][:],'-',label=lab,color=c)
	ax4.set_xlabel('QInf [m/s]',fontsize=10)
	ax4.set_ylabel('V_sink [m/s]',fontsize=10)
	plt.grid(b=True,which='major',color='k',linestyle='--')
	plt.title('V_sink over QInf')
	lines,labels = ax4.get_legend_handles_labels()
	ax4.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)

	### Plot lowest sink values as black circles #################
	for i in range(0,number_polars):
		ax4.plot(pol_diag_QInf_at_v_sink_min,pol_diag_v_sink_min,'.',label=lab,color="black") 

	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot8-AC-Polar-Diagram.png')
	
	
#############################################################
#############################################################
##### NINETH FIGURE #########################################
#############################################################
#############################################################

if p9==1:
	plot_9 = plt.figure(9, figsize=(20,15))
	plot_9.canvas.set_window_title('PLOT 9 - POWER LOSSES')
	plot_9.suptitle(figtitles, fontsize=12)

	##### POWER LOSSES BATT, MC & PROPELLER AND ALTITUDE OVER TIME  #########################################
	ax27=plot_9.add_subplot(2, 1, 1)
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_Bat[::plot_skip],'-',label="PL_Bat",color="red")
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_PROP[::plot_skip],'-',label="PL_Prop",color="brown")
	plt.margins(x=0.1, y=0.1)
	ax28 = ax27.twinx()
	ax28.format_coord = make_format(ax28, ax27)
	ax28.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax27.set_xlabel('Time [h]',fontsize=10)
	ax27.set_ylabel('Power Losses [W]',fontsize=10,color="red")
	ax28.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax27.grid(b=True,which='both',color='r',linestyle='--')
	ax28.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Power Losses and Altitude over Time')
	ax27.tick_params(axis='y', colors='red')
	ax28.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax27.get_legend_handles_labels()
	lines2, labels2 = ax28.get_legend_handles_labels()
	ax28.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	##### POWER LOSSES MOT AND ALTITUDE OVER TIME #########################################
	ax29=plot_9.add_subplot(2, 1, 2)
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_MOT_COPPER[::plot_skip],'-',label="PL_Mot_Copper",color="red")
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_MOT_BE_FE[::plot_skip],'-',label="PL_Mot_Be_Fe",color="orange")
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_MOT_TOT[::plot_skip],'-',label="PL_Mot_Tot_analyt",color="green")
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_MC[::plot_skip],'-',label="PL_MC",color="black")
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_MOT_TOT_MEAS[::plot_skip],'-',label="PL_Mot_MC_Meas",color="magenta")
	ax30 = ax29.twinx()
	ax30.format_coord = make_format(ax30, ax29)
	ax30.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax29.set_xlabel('Time [h]',fontsize=10)
	ax29.set_ylabel('Power Loss [W]',fontsize=10,color="red")
	ax30.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax29.grid(b=True,which='both',color='r',linestyle='--')
	ax30.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Power Losses and Altitude over Time')
	ax29.tick_params(axis='y', colors='red')
	ax30.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax29.get_legend_handles_labels()
	lines2, labels2 = ax30.get_legend_handles_labels()
	ax30.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plot_9.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot9-EPU-Power-Losses.png')



#############################################################
#############################################################
###### TENTH FIGURE #########################################
#############################################################
#############################################################

if p10==1:
	plot_10 = plt.figure(10, figsize=(20,15))
	plot_10.canvas.set_window_title('PLOT 10 - BATT CHARG LOSSES')
	plot_10.suptitle(figtitles, fontsize=12)
	
	##### BATTERY CHARGING POWER AND ALTITUDE OVER TIME  #########################################
	ax027=plot_10.add_subplot(2,2,1)
	ax027.plot(mission_data_real_time_h[::plot_skip],mission_data_batt_charging_power[::plot_skip],'-',label="Battery Charging Power [W]",color="green")
	plt.margins(x=0.1, y=0.1)
	ax028 = ax027.twinx()
	ax028.format_coord = make_format(ax028, ax027)
	ax028.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax027.set_xlabel('Time [h]',fontsize=10)
	ax027.set_ylabel('Battery Charging Power [W]',fontsize=10,color="red")
	ax028.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax027.grid(b=True,which='both',color='r',linestyle='--')
	ax028.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Battery Charging Power and Altitude over Time')
	ax027.tick_params(axis='y', colors='red')
	ax028.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax027.get_legend_handles_labels()
	lines2, labels2 = ax028.get_legend_handles_labels()
	ax028.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	##### CHARGING LOSSES BATTERY AND ALTITUDE OVER TIME  #########################################
	ax27=plot_10.add_subplot(2,2,2)
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_BATT_CHARGE_CHEM[::plot_skip],'-',label="CHEM Bat Charge Loss [W]",color="orange")
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_PL_BATT_CHARGE_OHM[::plot_skip],'-',label="OHM Bat Charge Loss [W]",color="black")
	plt.margins(x=0.1, y=0.1)
	ax28 = ax27.twinx()
	ax28.format_coord = make_format(ax28, ax27)
	ax28.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax27.set_xlabel('Time [h]',fontsize=10)
	ax27.set_ylabel('Battery Charging Loss [W]',fontsize=10,color="red")
	ax28.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax27.grid(b=True,which='both',color='r',linestyle='--')
	ax28.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Battery Charging Losses and Altitude over Time')
	ax27.tick_params(axis='y', colors='red')
	ax28.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax27.get_legend_handles_labels()
	lines2, labels2 = ax28.get_legend_handles_labels()
	ax28.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	##### BATTERY CHARGING CURRENT AND ALTITUDE OVER TIME  #########################################
	ax1027=plot_10.add_subplot(2,2,3)
	ax1027.plot(mission_data_real_time_h[::plot_skip],mission_data_batt_charging_current[::plot_skip],'-',label="Battery Charging Current [A]",color="red")
	plt.margins(x=0.1, y=0.1)
	ax1028 = ax1027.twinx()
	ax1028.format_coord = make_format(ax1028, ax1027)
	ax1028.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax1027.set_xlabel('Time [h]',fontsize=10)
	ax1027.set_ylabel('Battery Charging Current [A]',fontsize=10,color="red")
	ax1028.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax1027.grid(b=True,which='both',color='r',linestyle='--')
	ax1028.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Battery Charging Current and Altitude over Time')
	ax1027.tick_params(axis='y', colors='red')
	ax1028.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax1027.get_legend_handles_labels()
	lines2, labels2 = ax1028.get_legend_handles_labels()
	ax1028.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	##### EXCESSIVE SOLAR POWER AND ALTITUDE OVER TIME  #########################################
	ax7=plot_10.add_subplot(2,2,4)
	ax7.plot(mission_data_real_time_h[::plot_skip],mission_data_excess_solar_power[::plot_skip],'-',label="Excessive Solar Power",color="red")
	plt.margins(x=0.1, y=0.1)
	ax8 = ax7.twinx()
	ax8.format_coord = make_format(ax8, ax7)
	ax8.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax7.set_xlabel('Time [h]',fontsize=10)
	ax7.set_ylabel('Excessive Solar Power [W]',fontsize=10,color="red")
	ax8.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax7.grid(b=True,which='both',color='r',linestyle='--')
	ax8.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Excessive Solar Power and Altitude over Time')
	ax7.tick_params(axis='y', colors='red')
	ax8.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax7.get_legend_handles_labels()
	lines2, labels2 = ax8.get_legend_handles_labels()
	ax8.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	plot_10.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot10-Batt-Charg-Losses.png')
	
#############################################################
#############################################################
###### ELEVENTH FIGURE ######################################
#############################################################
#############################################################

if p11==1:
	plot_11 = plt.figure(11, figsize=(20,15))
	plot_11.canvas.set_window_title('PLOT 11 - RPM, VOLTAGE, CURRENT')
	plot_11.suptitle(figtitles, fontsize=12)

	##### MOTOR AND BATTERY CURRENT OVER TIME [s]  #########################################
	ax20=plot_11.add_subplot(2, 2, 1)
	ax20.plot(mission_data_real_time_h[::plot_skip],mission_data_Mot_Current[::plot_skip],'-',label="Motor Current",color="red")
	ax20.plot(mission_data_real_time_h[::plot_skip],mission_data_Bat_Current[::plot_skip],'-',label="Battery Current",color="orange")
	ax20.plot(mission_data_real_time_h[::plot_skip],mission_data_maxcurr[::plot_skip],'-.',label="Motor max. Current",color="red",linewidth=3.0)
	ax20.plot(mission_data_real_time_h[::plot_skip],mission_data_peakcurr[::plot_skip],'-',label="Motor peak Current",color="red",linewidth=3.0)
	ax21 = ax20.twinx()
	ax21.format_coord = make_format(ax21, ax20)
	ax21.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax20.set_xlabel('Time [h]',fontsize=10)
	ax20.set_ylabel('Current [A]',fontsize=10,color="red")
	ax21.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax20.grid(b=True,which='both',color='r',linestyle='--')
	ax21.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Current and Altitude over Time')
	ax20.tick_params(axis='y', colors='red')
	ax21.tick_params(axis='y', colors='blue')
	lines, labels = ax20.get_legend_handles_labels()
	lines2, labels2 = ax21.get_legend_handles_labels()
	ax21.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	
	##### BATTERY VOLTAGE OVER TIME [s]  #########################################
	ax201=plot_11.add_subplot(2, 2, 2)
	ax201.plot(mission_data_real_time_h[::plot_skip],mission_data_Bat_Voltage[::plot_skip],'-',label="Battery Voltage",color="red")
	ax211 = ax201.twinx()
	ax211.format_coord = make_format(ax211, ax201)
	ax211.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax201.set_xlabel('Time [h]',fontsize=10)
	ax201.set_ylabel('Voltage [V]',fontsize=10,color="red")
	ax211.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax201.grid(b=True,which='both',color='r',linestyle='--')
	ax211.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Battery Voltage and Altitude over Time')
	ax201.tick_params(axis='y', colors='red')
	ax211.tick_params(axis='y', colors='blue')
	lines, labels = ax201.get_legend_handles_labels()
	lines2, labels2 = ax211.get_legend_handles_labels()
	ax211.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	

	##### RPM AND ALTITUDE OVER TIME #########################################
	ax18=plot_11.add_subplot(2, 1, 2)
	ax18.plot(mission_data_real_time_h[::plot_skip],mission_data_RPM[::plot_skip],'-',label="RPM",color="red")
	ax18.plot(mission_data_real_time_h[::plot_skip],mission_data_RPM_MAX[::plot_skip],'--',label="RPM-max",color="red")
	ax19 = ax18.twinx()
	ax19.format_coord = make_format(ax19, ax18)
	ax19.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax18.set_xlabel('Time [h]',fontsize=10)
	ax18.set_ylabel('RPM',fontsize=10,color="red")
	ax19.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax18.grid(b=True,which='both',color='r',linestyle='--')
	ax19.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('RPM and Altitude over Time')
	ax18.tick_params(axis='y', colors='red')
	ax19.tick_params(axis='y', colors='blue')
	lines, labels = ax18.get_legend_handles_labels()
	lines2, labels2 = ax19.get_legend_handles_labels()
	ax19.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plt.margins(x=0.1, y=0.1)
	
	plot_11.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot11-RPM-Voltage-Current.png')

#############################################################
#############################################################
##### TWELVETH FIGURE #######################################
#############################################################
#############################################################
if p12==1:		
	plot_12 = plt.figure(12, figsize=(20,15))
	plot_12.canvas.set_window_title('PLOT 12 - RE, MACH')
	plot_12.suptitle(figtitles, fontsize=12)

	##### REYNOLDS AND ALTITUDE OVER TIME  #########################################
	ax27=plot_12.add_subplot(2, 1, 1)
	ax27.plot(mission_data_real_time_h[::plot_skip],mission_data_reynolds[::plot_skip],'-',label="Re [-]",color="red")
	#plt.ylim([0.,1.2])
	ax28 = ax27.twinx()
	ax28.format_coord = make_format(ax28, ax27)
	ax28.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax27.set_xlabel('Time [h]',fontsize=10)
	ax27.set_ylabel('Re [-]',fontsize=10,color="red")
	ax28.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax27.grid(b=True,which='both',color='r',linestyle='--')
	ax28.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Re and Altitude over Time')
	ax27.tick_params(axis='y', colors='red')
	ax28.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax27.get_legend_handles_labels()
	lines2, labels2 = ax28.get_legend_handles_labels()
	ax28.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)

	##### MACH AND ALTITUDE OVER TIME  #########################################
	ax29=plot_12.add_subplot(2, 1, 2)
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_mach[::plot_skip],'-',label="Mach [-]",color="red")
	ax30 = ax29.twinx()
	ax30.format_coord = make_format(ax30, ax29)
	ax30.plot(mission_data_real_time_h[::plot_skip],mission_data_altitude[::plot_skip],'-',label="Altitude [m]",color="blue")
	ax29.set_xlabel('Time [h]',fontsize=10)
	ax29.set_ylabel('Mach [-]',fontsize=10,color="red")
	ax30.set_ylabel('Altitude [m]',fontsize=10,color="blue")
	ax29.grid(b=True,which='both',color='r',linestyle='--')
	ax30.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Mach and Altitude over Time')
	ax29.tick_params(axis='y', colors='red')
	ax30.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax29.get_legend_handles_labels()
	lines2, labels2 = ax30.get_legend_handles_labels()
	ax30.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	plot_6.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot12-Re-Ma.png')	
	
#############################################################
#############################################################
##### THIRTEENTH FIGURE #####################################
#############################################################
#############################################################
if p13==1:		
	plot_13 = plt.figure(13, figsize=(20,15))
	plot_13.canvas.set_window_title('PLOT 13 - FLIGHT PATH AND HEADING')
	plot_13.suptitle(figtitles, fontsize=12)

	##### FLIGHT PATH  #########################################
	ax27=plot_13.add_subplot(2, 2, 1)
	ax27.plot(mission_data_distance_east,mission_data_distance_north,'-',label="Flight Path",color="red")
	ax27.set_xlabel('East [km]',fontsize=10)
	ax27.set_ylabel('North [km]',fontsize=10,color="black")
	ax27.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('FLIGHT PATH')
	ax27.tick_params(axis='y', colors='black')
	plt.margins(x=0.1, y=0.1)
	plt.axis('equal')
	lines, labels = ax27.get_legend_handles_labels()
	ax27.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)

	##### SUN ANGLES OVER TIME  #########################################
	ax271=plot_13.add_subplot(2, 2, 2)
	ax271.plot(mission_data_real_time_h[::plot_skip],mission_data_azimuth_angle[::plot_skip],'-',label="Sun Azimuth [deg]",color="red")
	ax271.plot(mission_data_real_time_h[::plot_skip],mission_data_zenith_angle[::plot_skip],'-',label="Sun Zenith [deg]",color="blue")
	ax271.plot(mission_data_real_time_h[::plot_skip],mission_data_declination_angle[::plot_skip],'-',label="Sun Declination [deg]",color="green")
	ax271.plot(mission_data_real_time_h[::plot_skip],mission_data_incangle[::plot_skip],'-',label="Sun Incidence Angle[deg]",color="orange")
	ax271.plot(mission_data_real_time_h[::plot_skip],mission_data_hour_angle[::plot_skip],'-',label="Sun Hour Angle [deg]",color="magenta")
	ax271.set_xlabel('Time [h]',fontsize=10)
	ax271.set_ylabel('Angle [deg]',fontsize=10,color="black")
	ax271.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('SUN ANGLES')
	ax271.tick_params(axis='y', colors='black')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax271.get_legend_handles_labels()
	ax271.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)

	##### BEST HEADING OVER TIME  #########################################
	ax29=plot_13.add_subplot(2, 1, 2)
	ax29.plot(mission_data_real_time_h[::plot_skip],mission_data_best_heading[::plot_skip],'-',label="Best Heading [deg]",color="red")
	ax29.set_xlabel('Time [h]',fontsize=10)
	ax29.set_ylabel('Best Heading [deg]',fontsize=10,color="black")
	ax29.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Best Heading over Time')
	ax29.tick_params(axis='y', colors='black')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax29.get_legend_handles_labels()
	#plot_6.subplots_adjust(left=0.05, bottom=0.05, right=0.93, top=0.87,wspace=0.25, hspace=0.4)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot13-Path-Heading.png')
	

#############################################################
#############################################################
##### FOURTEENTH FIGURE #####################################
#############################################################
#############################################################
if p14==1:		
	plot_14 = plt.figure(14, figsize=(20,15))
	plot_14.canvas.set_window_title('PLOT 14 - BATTERY HEATUP')
	plot_14.suptitle(figtitles, fontsize=12)

	##### BATTERY HEATUP  #########################################
	ax271=plot_14.add_subplot(2, 2, 1)
	ax271.plot(mission_data_real_time_h[::plot_skip],mission_data_batt_heatup[::plot_skip],'-',label="Battery Heatup",color="red")
	ax271.set_xlabel('Time [h]',fontsize=10)
	ax271.set_ylabel('Temperature Increase [K]',fontsize=10,color="black")
	ax271.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('BATTERY TEMPERATURE INCREASE')
	ax271.tick_params(axis='y', colors='black')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax271.get_legend_handles_labels()
	ax271.legend(lines, labels, loc=0, prop={'size': 10}, framealpha=1.)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()	
	plt.savefig('plot14-Batt-Heatup.png')


#############################################################
#############################################################
##### FIFTEENTH FIGURE ######################################
#############################################################
#############################################################
p15=1
if p15==1:		
	plot_15 = plt.figure(15, figsize=(20,15))
	plot_15.canvas.set_window_title('PLOT 15 - RPM/DBETA/CR/TAS/CAS VS ALT')
	plot_15.suptitle(figtitles, fontsize=12)

	##### DBETA AND TAS  #########################################
	ax27=plot_15.add_subplot(2, 1, 1)
	ax27.plot(mission_data_altitude[::plot_skip],mission_data_DBeta[::plot_skip],'-',label="DBeta [Deg]",color="red")
	ax28 = ax27.twinx()
	ax28.format_coord = make_format(ax28, ax27)
	ax28.plot(mission_data_altitude[::plot_skip],mission_data_velocity[::plot_skip],'-',label="TAS [m/s]",color="blue")
	ax28.plot(mission_data_altitude[::plot_skip],mission_data_velocity_CAS[::plot_skip],'--',label="CAS [m/s]",color="blue")
	ax27.set_xlabel('Altitude [m]',fontsize=10)
	ax27.set_ylabel('Dbeta [deg]',fontsize=10,color="red")
	ax28.set_ylabel('TAS/CAS [m/s]',fontsize=10,color="blue")
	ax27.grid(b=True,which='both',color='r',linestyle='--')
	ax28.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('DBETA, CAS and TAS over Altitude')
	ax27.tick_params(axis='y', colors='red')
	ax28.tick_params(axis='y', colors='blue')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax27.get_legend_handles_labels()
	lines2, labels2 = ax28.get_legend_handles_labels()
	ax28.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)



	##### CLIMBRATE    #########################################
	ax29=plot_15.add_subplot(2, 1, 2)
	ax29.plot(mission_data_altitude[::plot_skip],mission_data_RPM[::plot_skip],'-',label="RPM [-]",color="blue")
	ax30 = ax29.twinx()
	ax30.format_coord = make_format(ax30, ax29)
	ax30.plot(mission_data_altitude[::plot_skip],mission_data_climbrate[::plot_skip],'-',label="Climbrate [m/s]",color="red")
	ax29.set_xlabel('Altitude [m]',fontsize=10)
	ax29.set_ylabel('RPM [-]',fontsize=10,color="blue")
	ax30.set_ylabel('Climbrate [m/s]',fontsize=10,color="red")
	ax29.grid(b=True,which='both',color='r',linestyle='--')
	ax30.grid(b=True,which='both',color='b',linestyle='--')
	plt.title('Climbrate and RPM over Altitude')
	ax29.tick_params(axis='y', colors='blue')
	ax30.tick_params(axis='y', colors='red')
	plt.margins(x=0.1, y=0.1)
	lines, labels = ax29.get_legend_handles_labels()
	lines2, labels2 = ax30.get_legend_handles_labels()
	ax30.legend(lines + lines2, labels + labels2, loc=0, prop={'size': 10}, framealpha=1.)
	figManager = plt.get_current_fig_manager()
	figManager.window.showMaximized()
	plt.savefig('plot15-DBETA-TAS-CR-RPM-ALT.png')	
		

###################################################################################################

if writedata == 1:
	write_all=open("mission.csv",'w')
	write_all.write("Time[s],Real_Time[h],Altitude[m],TAS[m/s],CAS[km/h],Re[-],Ma[-],CL[-],AOA[deg],Climbrate[m/s],DBeta[deg],Efficiency_Prop[%],Efficiency_EPU[%],Efficiency_Mot_and_MC[%],Efficiency_TOT[%],RPM,RPM_max,Pprop[W],Pshaft[W],Ptot[W],Ptot_plus_payl+syst.[W],PBat_ex[W],PBat_int[W],Thrust[N],Q[Nm],Mot_Current[A],Bat_Current[A],Bat_Voltage[V],PowLoss_Bat_Discharge[W],PowLoss_MC[W],PowLoss_PROP[W],PowLoss_MOT_COPPER[W],PowLoss_MOT_BE+FE[W],PowLoss_MOT_TOT[W],PowLoss_MOT_MC_MEAS[W],Curr_Const_Meas[Nm/A],Excess_Solar_Pow[W],PowLoss_BATT_CHARGE_CHEM[W],PowLoss_BATT_CHARGE_OHM[W],BATT_Charging_Pow[W],BATT_Charging_Current[A],BATT_Heatup[K],Solar_Pow_avg[W],Solar_Pow_max[W],Atmosphere_transmission[%],Air_Density[kg/sqm],Max_Sol_Cell_Temp[degC],Min_Sol_Cell_Temp[degC],Energy_Spent[Wh],Potential_Energy[Wh],Energy_Remaining[Wh],Harvested_Solar_Energy[Wh],Hour_Angle[deg],Incidence_Angle[deg],Declination_Angle[deg],Zenith_Angle[deg],Azimuth_Angle[deg],Sunvector_x,Sunvector_y,Sunvector_z,Best_Heading[deg],Dist_Trav_East[km],Dist_Travel_North[km]\n")
	
	for n in range(0,len(mission_data_time)):
		write_all.write(str(mission_data_time[n])+","+str(mission_data_real_time_h[n])+","+str(mission_data_altitude[n])+","+str(mission_data_velocity[n])+","+str(3.6*(mission_data_velocity_CAS[n]))+","+str(mission_data_reynolds[n])+","+str(mission_data_mach[n])+","+str(mission_data_CL[n])+","+str(mission_data_alfa[n])+","+str(mission_data_climbrate[n])+\
		","+str(mission_data_DBeta[n])+","+str(mission_data_Efficiency_prop[n])+","+str(mission_data_Efficiency_EPU[n])+","+str(mission_data_Efficiency_MOT_MC[n])+","+str(mission_data_Efficiency_TOT[n])+","+str(mission_data_RPM[n])+","+str(mission_data_RPM_MAX[n])+","+str(mission_data_Pprop[n])+","+str(mission_data_Pshaft[n])+","+str(mission_data_Ptot[n])+","+str(Ptot_plus_payload_system[n])+","+str(mission_data_PBat_ex[n])+","+str(mission_data_PBat_int[n])+\
		","+str(mission_data_Thrust[n])+","+str(mission_data_Q[n])+","+str(mission_data_Mot_Current[n])+","+str(mission_data_Bat_Current[n])+","+str(mission_data_Bat_Voltage[n])+","+str(mission_data_PL_Bat[n])+","+str(mission_data_PL_MC[n])+","+str(mission_data_PL_PROP[n])+","+str(mission_data_PL_MOT_COPPER[n])+\
		","+str(mission_data_PL_MOT_BE_FE[n])+","+str(mission_data_PL_MOT_TOT[n])+","+str(mission_data_PL_MOT_TOT_MEAS[n])+","+str(mission_data_CURR_CONST_MEAS[n])+","+str(mission_data_excess_solar_power[n])+","+str(mission_data_PL_BATT_CHARGE_CHEM[n])+","+str(mission_data_PL_BATT_CHARGE_OHM[n])+","+str(mission_data_batt_charging_power[n])+","+str(mission_data_batt_charging_current[n])+\
		","+str(mission_data_batt_heatup[n])+","+str(mission_data_sol_pow_avg[n])+","+str(mission_data_sol_pow_max[n])+","+str(mission_data_atmo_transmission[n])+","+str(mission_data_density[n])+","+str(mission_data_cell_temp_max[n])+","+str(mission_data_cell_temp_min[n])+\
		","+str(mission_data_Energy_Spent[n])+","+str(mission_data_Potential_Energy[n])+","+str(mission_data_Energy_Remaining[n])+","+str(mission_data_harvested_Sol_Energy[n])+","+str(mission_data_hour_angle[n])+","+str(mission_data_incangle[n])+","+str(mission_data_declination_angle[n])+\
		","+str(mission_data_zenith_angle[n])+","+str(mission_data_azimuth_angle[n])+","+str(mission_data_sunvector_x[n])+","+str(mission_data_sunvector_y[n])+","+str(mission_data_sunvector_z[n])+","+str(mission_data_best_heading[n])+","+str(mission_data_distance_east[n])+","+str(mission_data_distance_north[n])+"\n")
	write_all.close()
	

if writedata == 1:
	write_cells_temp.close()
	write_cells_effi.close()
	write_cells_powers.close()
	write_cells_mppt_cutoff.close()
	write_cells_surf_transmission.close()

if heading_strategy == 1:
	directory_string="Best_Head_Lat="+str(latitude)+"deg_M="+str(int(aircraft_mass))+"kg_Bat_Cap="+str(int(bat_cap/1000.))+"kWh_DOY="+str(int(day_of_year))+"_TO_Time="+str(takeoff_time)+"_Dur="+str(round((mission_data_time[len(mission_data_time)-1]-mission_data_time[0])/3600.,3))+"h_"+str(calc_type)
if heading_strategy == 2:
	directory_string="Arb_Head_Lat="+str(latitude)+"deg_M="+str(int(aircraft_mass))+"kg_Bat_Cap="+str(int(bat_cap/1000.))+"kWh_DOY="+str(int(day_of_year))+"_TO_Time="+str(takeoff_time)+"_Dur="+str(round((mission_data_time[len(mission_data_time)-1]-mission_data_time[0])/3600.,3))+"h_"+str(calc_type)
if heading_strategy < 1 or heading_strategy > 2:
	print("########## HEADING STRATEGY INPUT ERROR #############")

os.system("mkdir %s" % directory_string)
os.system("mv sol_cells_temp.csv sol_cells_powers.csv sol_cells_effi.csv mission.csv plot*png %s" % directory_string)
os.system("cp /home/sven/Python/Solar_Calculator/vectors.py /home/sven/Python/Solar_Calculator/functions.py /home/sven/Python/Solar_Calculator/solarcalc.py input_solar.inp solsyst.def %s" % directory_string)
os.system("rm sol_cells_surf_transmission.csv sol_cells_mppt_cutoff.csv sol_cells_effi_spavl.csv sol_cells_powers_spavl.csv sol_cells_temp_spavl.csv")
	
if batchmode == 2:
	plt.show(block=False)


print("\n###### ACCUMMULATED ALTITUDE ERROR [m]: ",altitude_error_sum) #######\n"

if batchmode == 2:
	_ = input("Press [enter] to close all figures.")
